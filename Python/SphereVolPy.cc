/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "SphereVolPy.h"

using namespace boost::python;

    void exportSphereVol()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<SphereVol, bases<AVolume3D> >(
        "SphereVol",
        "A class defining a spherical L{AVolume3D}.",
        init<>()
      )
      .def(init<const SphereVol &>())
      .def(
        init<Vector3,double>(
          ( arg("centre"), arg("radius") ),
          "Constructs a sphere with the specified centre and radius.\n"
          "@type centre: L{Vector3}\n"
          "@kwarg centre: Coordinates of the centre of the sphere\n"
          "@type radius: double\n"
          "@kwarg radius: Radius of the sphere\n"
        )
      )
      .def(self_ns::str(self))
      ;
    }



