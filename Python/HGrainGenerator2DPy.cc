/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include <iostream>
#include <sstream>
#include "HGrainGenerator2DPy.h"

using namespace boost::python;

    using boost::python::arg;
    void exportHGrainGenerator2D()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<HGrainGenerator2D, bases<AGenerator2D> >(
        "HGrainGenerator2D",
        "A particle packing algorithm for filling 2D spaces with hexagonal grains.",
        init<>()
      )
      .def(init<const HGrainGenerator2D &>())
      .def(
        init<double>(
          ( arg("radius") ),
          "Initialises a particle packer in preparation for filling "
          "L{AVolume2D} with bonded hexagonal grains of particles of a specified radius, positioned on a regular base lattice. "
	  "Bonds within hexagonal grains are assigned bondTag=2. No bonds are generated between hexagonal grains.\n"
          "@type radius: double\n"
          "@kwarg radius: the radius of grains to pack\n"
          "@rtype: void\n"
        )
      )
      .def(
        "generatePacking",
        &HGrainGenerator2D::generatePacking,
        ( arg("volume"), arg("ntable"), arg("groupID")=0, arg("tag") ),
        "Generates an assembly of haxagonal grains to fill the specified volume.\n"
        "@type volume: L{BoxWithLines2D}\n"
        "@kwarg volume: the 2D volume to fill with particles\n"
        "@type ntable: L{MNTable2D}\n"
        "@kwarg ntable: the neighbours table that particles are inserted into\n"
        "@type groupID: int\n"
        "@kwarg groupID: the group ID assigned to particles (default: 0)\n"
        "@type tag: int\n"
        "@kwarg tag: the tag assigned to particles\n"
        "@rtype: void\n" 
      )
      .def(self_ns::str(self))
      ;
    }



