/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "DifferenceVolPy.h"

using namespace boost::python;

    void exportDifferenceVol()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<DifferenceVol, bases<AVolume3D> >(
        "DifferenceVol",
        "A class defining a volume consisting of the difference between two volumes",
        init<>()
      )
      .def(init<const DifferenceVol &>())
      .def(
        init<AVolume3D&,AVolume3D&>(
          ( arg("volume1"), arg("volume2") ),
          "Constructs a volume comprised of the difference of two volumes.\n"
          "The volume to be packed is all points within the first volume that are not also in the second volume.\n"
          "@type volume1: L{AVolume3D}\n"
          "@kwarg volume1: The first volume comprising the difference\n"
          "@type volume2: L{AVolume3D}\n"
          "@kwarg volume2: The second volume comprising the difference\n"
        )
      )
      .def(self_ns::str(self))
      ;
    }



