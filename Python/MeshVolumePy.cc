/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "MeshVolumePy.h"

using namespace boost::python;

void exportMeshVolume()
{
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

  class_<MeshVolume, bases<AVolume3D> >(
	"MeshVolume",
        "A class defining a volume bounded by a triangle mesh.",
        init<>()
      )
      .def(
        init<const TriPatchSet& >(
          ( arg("Mesh")),
          "Constructs a volume from a supplied set of triangles.\n"
          "@type Mesh: L{TriPatchSet}\n"
          "@kwarg Mesh: The set of triangles\n"
        )
      )
      ;
}

