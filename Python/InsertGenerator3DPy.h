/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef GENGEO_INSERTGENERATOR3D_H
#define GENGEO_INSERTGENERATOR3D_H

#include <boost/python.hpp>
#include <boost/python/overloads.hpp>
#include "util/vector3.h"
#include "src/AGenerator3D.h"
#include "src/InsertGenerator3D.h"
#include "MNTable3DPy.h"

void exportAGenerator3D();
void exportInsertGenerator3D();

#endif
