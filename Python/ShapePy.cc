/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "ShapePy.h"

using namespace boost::python;

void exportShape ()
{
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<Shape>(
        "Shape",
        "A shape designed for insertion into a L{ShapeList}.\n",
        //init<>()
	no_init
      )
      .def(
        "makeOrientationRandom",
        &Shape::makeOrientationRandom,
        ( arg("v") ),
        "Sets the shape to be randomly orientated every time\n"
        "it is inserted.\n"
        "@type v: integer\n"
        "@kwarg v: 1 to use a random orientation, 0 for constant orientation\n"
      );
      ;
}
