/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef GENGEO_INSERTGENERATOR2D_H
#define GENGEO_INSERTGENERATOR2D_H

#include <boost/python.hpp>
#include "util/vector3.h"
#include "src/AGenerator2D.h"
#include "src/InsertGenerator2D.h"
#include "BoxWithLines2DPy.h"
#include "MNTable2DPy.h"

void exportAGenerator2D();
void exportInsertGenerator2D();

#endif
