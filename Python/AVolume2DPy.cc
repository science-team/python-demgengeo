/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include <iostream>
#include <sstream>
#include "AVolume2DPy.h"

using namespace boost::python;

    using boost::python::arg;
    void exportAVolume()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<AVolume, boost::noncopyable>("AVolume", "Abstract base class for Volume classes in 2D or 3D.", no_init);
      ;
    }
    void exportAVolume2D()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<AVolume2D, bases<AVolume>, boost::noncopyable >(
         "AVolume2D", 
         "Abstract base class for 2D Volumes.", 
         no_init
      )
      ;
    }



