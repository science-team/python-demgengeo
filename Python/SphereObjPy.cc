/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "SphereObjPy.h"

using namespace boost::python;

void exportSphereObj ()
{
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<SphereObj>(
        "SphereObj",
        "A 3D sphere shape to be inserted in a shape list.",
        init<>()
      )
      .def(
        "setBias",
        &::dot,
        ( arg("i") ),
        "Sets the bias of the sphere shape (how often it should occur)\n"
        "@type i: int\n"
        "@kwarg i: the bias as any integer\n"
      )
      ;
}
