/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include <iostream>
#include <sstream>
#include "HexAggregateInsertGenerator2DPy.h"

using namespace boost::python;

    using boost::python::arg;
    void exportHexAggregateInsertGenerator2D()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<HexAggregateInsertGenerator2D, bases<InsertGenerator2D> >(
        "HexAggregateInsertGenerator2D",
        "A particle packing algorithm for filling L{AVolume2D} spaces with hexagonal aggregates of particles",
        init<>()
      )
      .def(init<const HexAggregateInsertGenerator2D &>())
      .def(
        init<double,double,int,int,double>(
          ( arg("minRadius"), arg("maxRadius"), arg("insertFails"), arg("maxIterations"), arg("tolerance") ),
          "Initialises a particle packer in preparation for filling an "
          "L{AVolume2D} with hexagonal aggregates whose radii are in the specified range.\n"
          "The algorithm consists of two stages: 1) the L{AVolume2D} is packed with particles of a specified "
            "radius range, then 2) each particle is replaced with a Hexagonal grain of particles of equal size\n"
          "@type minRadius: double\n"
          "@kwarg minRadius: the minimum radius of particles to pack\n"
          "@type maxRadius: double\n"
          "@kwarg maxRadius: the maximum radius of particles to pack\n"
          "@type insertFails: int\n"
          "@kwarg insertFails: the number of particle insertion failures before the packer terminates\n"
          "@type maxIterations: int\n"
          "@kwarg maxIterations: the maximum number of iterations of the solver\n"
          "@rtype: void\n"
        )
      )
      .def(self_ns::str(self))
      ;
      class_<HexAggregateInsertGenerator2DRand, bases<HexAggregateInsertGenerator2D> >(
        "HexAggregateInsertGenerator2DRand",
        "A particle packing algorithm for filling 2D spaces with hexagonal aggregates of particles",
        init<>()
      )
      .def(init<const HexAggregateInsertGenerator2DRand &>())
      .def(
	   init<double,double,int,int,double,double>(
	   ( arg("minRadius"), arg("maxRadius"), arg("insertFails"), arg("maxIterations"), arg("tolerance"), arg("removeProb") ),
          "Initialises a particle packer in preparation for filling a\n"
          "2D space with hexagonal aggregates whose radii are in the specified range.\n"
          "The algorithm consists of three stages: \n"
          "1) the L{AVolume2D} is packed with particles of a specified radius range;\n"
          "2) each particle is replaced with a hexagonal grain of particles of equal size;\n"
	  "3) for each hexagonal grain, one of the particles within the grain is removed with a specified probability, C{removeProb}\n"
          "@type minRadius: double\n"
          "@kwarg minRadius: the minimum radius of particles to pack\n"
          "@type maxRadius: double\n"
          "@kwarg maxRadius: the maximum radius of particles to pack\n"
          "@type insertFails: int\n"
          "@kwarg insertFails: the number of particle insertion failures before the packer terminates\n"
          "@type maxIterations: int\n"
          "@kwarg maxIterations: the maximum number of iterations of the solver\n"
          "@type removeProb: double\n"
          "@kwarg removeProb: the probability that one particle is removed from a hexagonal grain during packing\n"
          "@rtype: void\n"
        )
      )
      .def(self_ns::str(self))
      ;
    }
