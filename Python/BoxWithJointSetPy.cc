/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "BoxWithJointSetPy.h"

using namespace boost::python;

    void exportBoxWithJointSet()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<BoxWithJointSet, bases<BoxWithPlanes3D> >(
        "BoxWithJointSet",
        "A class defining a rectangular volume in 3D containing a joint set.",
        init<>()
      )
      .def(init<const BoxWithJointSet &>())
      .def(
        init<Vector3,Vector3>(
          ( arg("minPoint"), arg("maxPoint") ),
          "Constructs a box with the specified corner points.\n"
          "@type minPoint: L{Vector3}\n"
          "@kwarg minPoint: Coordinate of bottom-left-front corner of the box\n"
          "@type maxPoint: L{Vector3}\n"
          "@kwarg maxPoint: Coordinate of upper-right-back corner of the box\n"
        )
      )
      .def(
        "addJoints",
        &BoxWithJointSet::addJoints,
        ( arg("JointSet") ),
        "Adds to the box a L{TriPatchSet} representing joints. Particles will be fitted around the joints.\n"
        "@type JointSet: L{TriPatchSet}\n"
        "@kwarg JointSet: the set of triangular patches to add\n"
        "@rtype: void\n"
      )
      .def(self_ns::str(self))
      ;

    }
