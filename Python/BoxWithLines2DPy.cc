/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "BoxWithLines2DPy.h"
#include "src/BoxWithLines2D.h"

using namespace boost::python;

    void exportBoxWithLines2D()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<BoxWithLines2D, bases<AVolume2D> >(
        "BoxWithLines2D",
        "A class defining a rectangular volume in 2D optionally bounded by lines.",
        init<>()
      )
      .def(init<const BoxWithLines2D &>())
      .def(
        init<Vector3,Vector3>(
          ( arg("minPoint"), arg("maxPoint") ),
          "Constructs a box with the specified corner points.\n"
          "@type minPoint: L{Vector3}\n"
          "@kwarg minPoint: Coordinate of bottom left corner of the box\n"
          "@type maxPoint: L{Vector3}\n"
          "@kwarg maxPoint: Coordinate of upper right corner of the box\n"
        )
      )
      .def(
        "addLine",
        &BoxWithLines2D::addLine,
        ( arg("line") ),
        "Adds a line to the box for fitting particles.\n"
        "@type line: L{Line2D}\n"
        "@kwarg line: the line to add to the box\n"
        "@rtype: void\n"
      )
      .def(self_ns::str(self))
      ;
//      boost::python::implicitly_convertible<BoxWithLines2D, AVolume2D>();
    }



