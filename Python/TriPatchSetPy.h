/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __TRIPATCHSET_PY_H
#define __TRIPATCHSET_PY_H


#include <boost/python.hpp>
#include "src/TriPatchSet.h"

void exportTriPatchSet();

#endif // __TRIPATCHSET_PY_H
