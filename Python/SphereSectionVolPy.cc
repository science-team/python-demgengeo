/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "SphereSectionVolPy.h"

using namespace boost::python;

    void exportSphereSectionVol()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<SphereSectionVol, bases<AVolume3D> >(
        "SphereSectionVol",
        "A class defining a section of a spherical L{AVolume3D}.",
        init<>()
      )
      .def(init<const SphereSectionVol &>())
      .def(
        init<Vector3,double,double,Vector3>(
          ( arg("centre"), arg("radius"), arg("distance"), arg("normal") ),
          "Constructs a section of a sphere with the specified centre and radius\n"
          "cropped by a plane with a given normal vector and distance from the sphere centre.\n"
          "@type centre: L{Vector3}\n"
          "@kwarg centre: Coordinates of the centre of the sphere\n"
          "@type radius: double\n"
          "@kwarg radius: Radius of the sphere\n"
          "@type distance: double\n"
          "@kwarg distance: Distance of section surface from centre of sphere\n"
          "@type normal: L{Vector3}\n"
          "@kwarg normal: Coordinates of the unit normal vector pointing from the section surface to the centre of the sphere\n"
        )
      )
      .def(self_ns::str(self))
      ;
    }



