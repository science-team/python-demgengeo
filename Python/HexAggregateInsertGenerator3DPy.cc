/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include <iostream>
#include <sstream>
#include "HexAggregateInsertGenerator3DPy.h"

using namespace boost::python;

   using boost::python::arg;
   void exportHexAggregateInsertGenerator3D()
   {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<HexAggregateInsertGenerator3D, bases<InsertGenerator3D> >(
        "HexAggregateInsertGenerator3D",
        "A particle packing algorithm for filling L{AVolume3D} volumes with hexahedral aggregates of particles",
        init<>()
      )
      .def(init<const HexAggregateInsertGenerator3D &>())
      .def(
	   init<double,double,int,int,double,bool>(
	   ( arg("minRadius"), arg("maxRadius"), arg("insertFails"), arg("maxIterations"), arg("tolerance"),arg("seed") ),
	  "Initialises a particle packer in preparation for filling an "
          "L{AVolume3D} with hexahedral aggregates whose bounding sphere radii are in the specified range.\n"
          "The algorithm consists of two stages: \n"
          "1) the L{AVolume3D} is packed with particles of a specified radius range;\n"
          "2) each particle is replaced with a hexahedral grain of particles of equal size\n"
          "@type minRadius: double\n"
          "@kwarg minRadius: the minimum radius of particles to pack\n"
          "@type maxRadius: double\n"
          "@kwarg maxRadius: the maximum radius of particles to pack\n"
          "@type insertFails: int\n"
          "@kwarg insertFails: the number of particle insertion failures before the packer terminates\n"
          "@type maxIterations: int\n"
          "@kwarg maxIterations: the maximum number of iterations of the solver\n"
          "@type tolerance: double\n"
          "@kwarg tolerance: the overlap tolerance permitted\n"
          "@type seed: bool\n"
          "@kwarg seed: randomize the random number generator if True\n"
          "@rtype: void\n"
        )
      )
      .def(
        "generatePacking",
        &HexAggregateInsertGenerator3D::generatePacking3,
        ( arg("volume"), arg("ntable"), arg("groupID")=0 ),
        "Generates a particle assembly to fill the specified volume with\n"
        "hexahedral aggregrates whose constituent particles have the specified particle tag\n"
        "@type volume: L{AVolume3D}\n"
        "@kwarg volume: the volume to fill with particles\n"
        "@type ntable: L{MNTable3D}\n"
        "@kwarg ntable: the neighbours table that particles are inserted into\n"
        "@type groupID: int\n"
        "@kwarg groupID: the group ID assigned to particles (default: 0)\n"
        "@rtype: void\n"
      )
      .def(
        "generatePacking",
        &HexAggregateInsertGenerator3D::generatePacking4,
        ( arg("volume"), arg("ntable"), arg("groupID")=0, arg("tag") ),
        "@type tag: int\n"
        "@kwarg tag: the tag assigned to the generated particles\n"
        " (optional (default: -1))\n"
      )
      .def(
        "generatePackingMaxVolume",
        &HexAggregateInsertGenerator3D::generatePackingMaxVolume,
        ( arg("volume"), arg("ntable"), arg("groupID")=0, arg("tag"), arg("maxVolume") ),
	"Fill the specified L{AVolume3D} with particles until the accumulated particle volume exceeds the specified C{maxVolume}.\n"
        "@type maxVolume: float\n"
        "@kwarg maxVolume: the maximum cumulative volume of the inserted particles\n"
      )
      .def(self_ns::str(self))
      ;
    }

