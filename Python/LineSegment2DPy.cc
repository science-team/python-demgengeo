/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "LineSegment2DPy.h"

using namespace boost::python;

    using boost::python::arg;
    void exportLineSegment2D()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<LineSegment2D, bases<Line2D> >(
        "LineSegment2D",
        "A class defining a line segment in 2 dimensions.",
        init<>()
      )
      .def(init<const LineSegment2D &>())
      .def(
        init<Vector3,Vector3>(
          ( arg("startPoint"), arg("endPoint") ),
          "Constructs a line with specified endpoints.\n"
	  "N.B. This is not an L{AVolume2D} so cannot be used with C{InsertGenerators}.\n"
          "@type startPoint: L{Vector3}\n"
          "@kwarg startPoint: the starting point of the line\n"
          "@type endPoint: L{Vector3}\n"
          "@kwarg endPoint: the end point of the line\n"
        )
      )
      .def(self_ns::str(self))
      ;
    }
