/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include "FullCircMNTable3DPy.h"

using namespace std;

using namespace boost::python;

    using boost::python::arg;
    void exportFullCircMNTable3D()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

	class_<FullCircMNTable3D, bases<CircMNTable3D> >(
        "FullCircMNTable3D",
        "A multi-group neighbours table for constructing 3D particle setups with circular boundary conditions in all three directions (X, Y and Z).",
        init<>()
      )
      .def(init<const FullCircMNTable3D &>())
      .def(
        init<Vector3&,Vector3&,double, unsigned int>(
          ( boost::python::arg("minPoint"), boost::python::arg("maxPoint"), boost::python::arg("gridSize"), boost::python::arg("numGroups")=1 ),
          "Constructs a neighbours table with specified bounds, cell size and initial number of particle groups.\n"
          "@type minPoint: L{Vector3}\n"
          "@kwarg minPoint: lower-left-front point of the particle region\n"
          "@type maxPoint: L{Vector3}\n"
          "@kwarg maxPoint: upper-right-back point of the particle region\n"
          "@type gridSize: double\n"
          "@kwarg gridSize: the cell size for neighbour searches\n"
          "@type numGroups: unsigned int\n"
          "@kwarg numGroups: the initial number of groups (default: 1)\n"
        )
      )
      .def(
        "generateBonds",
        &FullCircMNTable3D::generateBonds,
        (boost::python::arg("groupID")=0, boost::python::arg("tolerance"), boost::python::arg("bondID") ),
        "Generates bonds between particle pairs separated by less than the specified tolerance\n"
        "@type groupID: int\n"
        "@kwarg groupID: the group ID of particles to bond together (default: 0)\n"
        "@type tolerance: double\n"
        "@kwarg tolerance: maximum distance separating bonded particles\n"
        "@type bondID: int\n"
        "@kwarg bondID: the bond ID to assign generated bonds\n"
        "@rtype: void\n"
      )
      .def(self_ns::str(self))
      ;
    }



