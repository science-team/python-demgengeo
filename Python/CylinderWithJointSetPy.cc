/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "CylinderWithJointSetPy.h"

using namespace boost::python;

    void exportCylinderWithJointSet()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<CylinderWithJointSet, bases<AVolume3D> >(
        "CylinderWithJointSet",
        "A class defining a cylinder in 3D space containing an optional joint set.",
        init<>()
      )
      .def(init<const CylinderWithJointSet &>())
      .def(
        init<Vector3,Vector3,double,double>(
          ( arg("origin"), arg("axis"), arg("length"), arg("radius") ),
          "Constructs a cylinder with the specified origin, axis, length and radius.\n"
          "@type origin: L{Vector3}\n"
          "@kwarg origin: Coordinate of origin (centre of base) of the cylinder\n"
          "@type axis: L{Vector3}\n"
          "@kwarg axis: Unit vector in the direction of the cylinder axis\n"
          "@type length: double\n"
          "@kwarg length: Length of the cylinder\n"
          "@type radius: double\n"
          "@kwarg radius: Radius of the cylinder\n"
        )
      )
      .def(
        "addJoints",
        &CylinderWithJointSet::addJoints,
        ( arg("JointSet") ),
        "Adds a set of triangluar patches as joints.\n"
        "@type plane: L{Plane}\n"
        "@kwarg plane: the set of patches\n"
        "@rtype: void\n"
      )
      .def(self_ns::str(self))
      ;
    }



