/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef GENGEO_HEXAGGREGATEINSERTGENERATOR2D_H
#define GENGEO_HEXAGGREGATEINSERTGENERATOR2D_H

#include <boost/python.hpp>
#include "src/HexAggregateInsertGenerator2D.h"
#include "src/HexAggregateInsertGenerator2DRand.h"

void exportHexAggregateInsertGenerator2D();

#endif
