/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/python.hpp>
#include "Vector3Py.h"
#include "Line2DPy.h"
#include "LineSegment2DPy.h"
#include "AVolume2DPy.h"
#include "AVolume3DPy.h"
#include "BoxWithLines2DPy.h"
#include "TriWithLines2DPy.h"
#include "PolygonWithLines2DPy.h"
#include "BoxWithLines2DSubVolPy.h"
#include "BoxWithPlanes3DPy.h"
#include "BoxWithJointSetPy.h"
#include "MNTable2DPy.h"
#include "MNTable3DPy.h"
#include "CircMNTable2DPy.h"
#include "CircMNTableXY2DPy.h"
#include "CircMNTable3DPy.h"
#include "FullCircMNTable3DPy.h"
#include "InsertGenerator2DPy.h"
#include "InsertGenerator3DPy.h"
#include "HexAggregateInsertGenerator2DPy.h"
#include "HexAggregateInsertGenerator3DPy.h"
#include "HGrainGenerator2DPy.h"
#include "PlanePy.h"
#include "SpherePy.h"
//#include "SphereObjPy.h"
#include "CylinderVolPy.h"
#include "CylinderWithJointSetPy.h"
#include "SphereVolPy.h"
#include "SphereVolWithJointSetPy.h"
#include "EllipsoidVolPy.h"
#include "ClippedSphereVolPy.h"
#include "SphereSectionVolPy.h"
#include "DogBonePy.h"
#include "TriBoxPy.h"
#include "CircleVolPy.h"
#include "TriPatchSetPy.h"
#include "ShapePy.h"
#include "ShapeListPy.h"
#include "ConvexPolyhedronPy.h"
#include "ConvexPolyWithJointSetPy.h"
#include "UnionVolPy.h"
#include "IntersectionVolPy.h"
#include "DifferenceVolPy.h"
#include "MeshVolumePy.h"
#include "MeshVolWithJointSetPy.h"
#include "ClippedCircleVolPy.h"
#include "LineSet2DPy.h"
#include "MeshVolume2DPy.h"

using namespace boost::python;

BOOST_PYTHON_MODULE(gengeo)
{
  exportVector3();
  exportLine2D();
  exportLineSegment2D();
  exportAVolume();
  exportAVolume2D();
  exportAVolume3D();
  exportBoxWithLines2D();
  exportPolygonWithLines2D();
  exportTriWithLines2D();
  exportBoxWithLines2DSubVol();
  exportMNTable2D();
  exportMNTable3D();
  exportCircMNTable2D();
  exportCircMNTableXY2D();
  exportCircMNTable3D();
  exportFullCircMNTable3D();
  exportAGenerator2D();
  exportInsertGenerator2D();
  exportAGenerator3D();
  exportInsertGenerator3D();
  exportHexAggregateInsertGenerator2D();
  exportHexAggregateInsertGenerator3D();
  exportHGrainGenerator2D();
  exportPlane();
  exportBoxWithPlanes3D();
  exportBoxWithJointSet();
  exportCylinderVol();
  exportCylinderWithJointSet();
  exportSphereVol();
  exportSphereVolWithJointSet();
//  exportSphereObj();
  exportEllipsoidVol();
  exportClippedSphereVol();
  exportSphereSectionVol();
  exportDogBone();
  exportTriBox();
  exportSphere();
  exportCircleVol();
  exportTriPatchSet();
  exportShape();
  exportShapeList();
  exportConvexPolyhedron();
  exportConvexPolyWithJointSet();
  exportUnionVol();
  exportIntersectionVol();
  exportDifferenceVol();
  exportMeshVolume();
  exportMeshVolWithJointSet();
  exportClippedCircleVol();
  exportLineSet2D();
  exportMeshVolume2D();
}
