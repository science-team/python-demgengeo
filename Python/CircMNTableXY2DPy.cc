/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include "CircMNTableXY2DPy.h"

using namespace std;

using namespace boost::python;

    using boost::python::arg;
    void exportCircMNTableXY2D()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

	class_<CircMNTableXY2D, bases<CircMNTable2D,MNTable2D> >(
        "CircMNTableXY2D",
        "A multi-group neighbours table for constructing 2D particle setups with circular boundary conditions in both (X and Y) directions. This is largely for packing algorithm testing",
        init<>()
      )
      .def(init<const CircMNTableXY2D &>())
      .def(
        init<Vector3&,Vector3&,double, unsigned int>(
          ( boost::python::arg("minPoint"), boost::python::arg("maxPoint"), boost::python::arg("gridSize"), boost::python::arg("numGroups")=1 ),
          "Constructs a neighbours table with specified bounds, cell size and initial number of particle groups.\n"
          "@type minPoint: L{Vector3}\n"
          "@kwarg minPoint: lower-left point of the particle region\n"
          "@type maxPoint: L{Vector3}\n"
          "@kwarg maxPoint: upper-right point of the particle region\n"
          "@type gridSize: double\n"
          "@kwarg gridSize: the cell size for neighbour searches\n"
          "@type numGroups: unsigned int\n"
          "@kwarg numGroups: the initial number of groups (default: 1)\n"
        )
      )
      .def(self_ns::str(self))
      ;
    }



