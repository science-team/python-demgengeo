/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "Vector3Py.h"

using namespace boost::python;

void exportVector3 ()
{
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<Vector3>(
        "Vector3",
        "A class defining a location or vector in 3D space",
        init<>()
      )
      .def(init<const Vector3 &>())
      .def(
        init<double,double,double>(
          ( arg("x"), arg("y"), arg("z") ),
          "Constructs a vector with specifed component values.\n"
          "@type x: float\n"
          "@kwarg x: index 0\n"
          "@type y: float\n"
          "@kwarg y: index 1\n"
          "@type z: float\n"
          "@kwarg z: index 2\n"
        )
      )
      .def(self == self)
      .def(self - self)
      .def(self + self)
      .def(self * double(0.0))
      .def(
        "dot",
        &::dot,
        ( arg("v") ),
        "Returns the dot product of this 3-element vector with\n"
        "the specified L{Vector3}.\n"
        "@type v: L{Vector3}\n"
        "@kwarg v: dot product with this\n"
        "@rtype: float\n"
      )
      .def(
        "cross",
        &::cross,
        ( arg("v") ),
        "Returns the cross product of this 3-element vector with\n"
        "the specified L{Vector3}.\n"
        "@type v: L{Vector3}\n"
        "@kwarg v: cross product with this\n"
        "@rtype: L{Vector3}\n"
      )
      .def(
        "norm",
        &Vector3::norm,
        "Returns the magnitude of this 3-element vector.\n"
        "@rtype: float\n"
        "@return: math.sqrt(self.dot(self)).\n"
      )
      .def(
        "unit",
        &Vector3::unit,
        "Returns the unit vector in the direction of this 3-element vector.\n"
        "@rtype: L{Vector3}\n"
        "@return: self/self.norm(self).\n"
      )
      .def(
        "X",
        &Vector3::x,
        "Returns the x-coordinate of the vector.\n"
        "@rtype: double\n"
      )
      .def(
        "Y",
        &Vector3::y,
        "Returns the y-coordinate of the vector.\n"
        "@rtype: double\n"
      )
      .def(
        "Z",
        &Vector3::z,
        "Returns the z-coordinate of the vector.\n"
        "@rtype: double\n"
      )
      .def(self_ns::str(self))
      ;
}
