#ifndef GENGEO_MESHVOLUME2DPY_H
#define GENGEO_MESHVOLUME2DPY_H

#include <boost/python.hpp>
#include "src/MeshVolume2D.h"

void exportMeshVolume2D();

#endif // GENGEO_MESHVOLUME2DPY_H
