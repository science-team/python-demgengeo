/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "ConvexPolyhedronPy.h"

using namespace boost::python;

void exportConvexPolyhedron()
{
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

  class_<ConvexPolyhedron, bases<BoxWithPlanes3D> >(
        "ConvexPolyhedron",
        "A class defining a convex polyhedral L{AVolume3D} bounded by planes.",
        init<>()
      )  
      .def(init<const ConvexPolyhedron &>())
      .def(
        init<Vector3,Vector3>(
          ( arg("minPoint"), arg("maxPoint") ),
          "Specifies the bounding box of a convex polyhedron.\n"
          "@type minPoint: L{Vector3}\n"
          "@kwarg minPoint: Coordinate of bottom-left-front corner of the bounding box\n"
          "@type maxPoint: L{Vector3}\n"
          "@kwarg maxPoint: Coordinate of upper-right-back corner of the bounding box\n"
        )
      )
       .def(self_ns::str(self))
      ;
}
