/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "BoxWithPlanes3DPy.h"

using namespace boost::python;

    void exportBoxWithPlanes3D()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<BoxWithPlanes3D, bases<AVolume3D> >(
        "BoxWithPlanes3D",
        "A class defining a rectangular volume in 3D optionally bounded by planes.",
        init<>()
      )
      .def(init<const BoxWithPlanes3D &>())
      .def(
        init<Vector3,Vector3>(
          ( arg("minPoint"), arg("maxPoint") ),
          "Constructs a box with the specified corner points.\n"
          "@type minPoint: L{Vector3}\n"
          "@kwarg minPoint: Coordinate of bottom-left-front corner of the box\n"
          "@type maxPoint: L{Vector3}\n"
          "@kwarg maxPoint: Coordinate of upper-right-back corner of the box\n"
        )
      )
      .def(
        "addPlane",
        &BoxWithPlanes3D::addPlane,
        ( arg("plane") ),
        "Adds a plane to the box for fitting particles.\n"
        "@type plane: L{Plane}\n"
        "@kwarg plane: the plane to add to the box\n"
        "@rtype: void\n"
      )
      .def(self_ns::str(self))
      ;
//      boost::python::implicitly_convertible<BoxWithPlanes3D, AVolume2D>();
    }



