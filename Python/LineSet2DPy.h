#ifndef LINESET2D_PY_H
#define LINESET2D_PY_H


#include <boost/python.hpp>
#include "src/LineSet.h"

void exportLineSet2D();

#endif // __LINESET2D_PY_H
