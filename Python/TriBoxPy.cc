/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "TriBoxPy.h"

using namespace boost::python;

    void exportTriBox()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<TriBox, bases<AVolume3D> >(
        "TriBox",
        "A class defining a triangular prism L{AVolume3D} bounded by planes.",
        init<>()
      )
      .def(init<const TriBox &>())
      .def(
        init<Vector3,Vector3,bool>(
          ( arg("minPoint"), arg("maxPoint"), arg("inverted") ),
          "Constructs a triangular prism with the specified coordinates.\n"
          "The triangle cross-section is in the X-Y plane and may be upright or C{inverted}.\n"
          "@type minPoint: L{Vector3}\n"
          "@kwarg minPoint: Coordinate of bottom-left-front corner of the axis-aligned bounding box\n"
          "@type maxPoint: L{Vector3}\n"
          "@kwarg maxPoint: Coordinate of upper-right-back corner of the axis-aligned bounding box\n"
          "@type inverted: boolean\n"
          "@kwarg inverted: defines whether tribox is inverted (default is False)\n"
        )
      )
      .def(
        "addPlane",
        &TriBox::addPlane,
        ( arg("plane") ),
        "Adds a plane to the TriBox for fitting particles.\n"
        "@type plane: L{Plane}\n"
        "@kwarg plane: the plane to add to the TriBox\n"
        "@rtype: void"
      )
      .def(self_ns::str(self))
      ;
//      boost::python::implicitly_convertible<TriBox, AVolume2D>();
    }



