#include <boost/version.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include "LineSet2DPy.h"

using namespace std;

using namespace boost::python;

void exportLineSet2D()
{
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

    class_<LineSet>(
      "LineSet",
      "A collection of line segments",
      init<>()
    )
    .def(init<LineSet &>())
    .def("addSegment",
	 &LineSet::addLineSegment,
	 ( boost::python::arg("Point1"),boost::python::arg("Point2"),boost::python::arg("tag")),
	 "Adds a triangle by specifying corner coordinates\n"
         "@type Point1: L{Vector3}\n"
         "@kwarg Point1: location of first point of the line segment\n"
         "@type Point2: L{Vector3}\n"
         "@kwarg Point2: location of second point of the line segment\n"
         "@type tag: int\n"
         "@kwarg tag: the tag to assign to the segment\n"
         )
    .def(
      "isCrossing",
      &LineSet::isCrossing,
      (boost::python::arg("Point1"),boost::python::arg("Point2")),
      "Checks if the line between two specified points crosses a line. "
      "If so, the segment tag is returned; if not, -1.\n"
      "@type Point1: L{Vector3}\n"
      "@kwarg Point1: location of first corner of the triangle\n"
      "@type Point2: L{Vector3}\n"
      "@kwarg Point2: location of second corner of the triangle\n"
      "@rtype: int\n"
    )
    .def(
      "getMinPoint",
      &LineSet::getMinPoint,
      "Returns minimum corner of the line set bounding box\n"
      "@rtype: L{Vector3}\n"
    )
    .def(
      "getMaxPoint",
      &LineSet::getMaxPoint,
      "Returns maximum corner of the line set bounding box\n"
      "@rtype: L{Vector3}\n"
    );
}
