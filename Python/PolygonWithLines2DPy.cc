/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "PolygonWithLines2DPy.h"
#include "src/PolygonWithLines2D.h"

using namespace boost::python;

    void exportPolygonWithLines2D()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<PolygonWithLines2D, bases<AVolume2D> >(
        "PolygonWithLines2D",
        "A class defining a polygonal area in 2D space bounded by lines.",
        init<>()
      )
      .def(init<const PolygonWithLines2D &>())
      .def(
	   init<boost::python::list>(
          ( arg("corners") ),
          "Constructs a polygon from either a list of vertices or a specified centre, radius and number of sides.\n"
          "@type corners: boost::python::list\n"
          "@kwarg corners: list of vertices.  Do not specify C{centre}, C{radius}, C{nsides} or C{smooth_edges} when using this parameter.\n"
	  )
      )
      .def(
        init<Vector3,double,int,bool>(
          ( arg("centre"), arg("radius"), arg("nsides"), arg("smooth_edges") ),
          "@type centre: L{Vector3}\n"
          "@kwarg centre: Coordinates of the centre of the polygon.  Do not specify C{corners} when using this parameter.\n"
          "@type radius: double\n"
          "@kwarg radius: Radius of circle enclosing the polygon.  Do not specify C{corners} when using this parameter.\n"
          "@type nsides: int\n"
          "@kwarg nsides: Number of sides for the polygon (maximum of 50).  Do not specify C{corners} when using this parameter.\n"
          "@type smooth_edges: bool\n"
          "@kwarg smooth_edges: Adds bounding lines to edges to achieve smooth edges.  Do not specify C{corners} when using this parameter.\n"
        )
      )
      .def(
        "addLine",
        &PolygonWithLines2D::addLine,
        ( arg("line") ),
        "Adds a line to the polygon for fitting particles.\n"
        "@type line: L{Line2D}\n"
        "@kwarg line: the line to add to the polygon\n"
        "@rtype: void\n"
      )
      .def(self_ns::str(self))
      ;
//      boost::python::implicitly_convertible<PolygonWithLines2D, AVolume2D>();
    }



