/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "Line2DPy.h"

using namespace boost::python;

    using boost::python::arg;
    void exportLine2D()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<Line2D>(
        "Line2D",
        "A class defining a line in 2D space.",
        init<>()
      )
      .def(init<const Line2D &>())
      .def(
        init<Vector3,Vector3>(
          ( arg("startPoint"), arg("endPoint") ),
          "Constructs a line in 2D space with specified endpoints.\n"
          "N.B. This is not an L{AVolume2D} so cannot be used with C{InsertGenerators}.\n"
          "@type startPoint: L{Vector3}\n"
          "@kwarg startPoint: location of starting point of the line\n"
          "@type endPoint: L{Vector3}\n"
          "@kwarg endPoint: location of end point of the line\n"
        )
      )
      .def(
        "intersect",
        &Line2D::intersect,
        ( arg("line") ),
        "Returns the point of intersection of this line with\n"
        "the specified line.\n"
        "@type line: L{Line2D}\n"
        "@kwarg line: the intersecting line\n" 
        "@rtype: L{Vector3}\n"
      )
      .def(
        "parallel",
        &Line2D::parallel,
        ( arg("distance") ),
        "Returns a line parallel to this one, separated by\n"
        "the specified distance.\n"
        "@type distance: double\n"
        "@kwarg distance: the distance to the parallel line to construct\n" 
        "@rtype: L{Line2D}\n"
      )
      .def(
        "getOrig",
        &Line2D::getOrig,
        "Returns the origin point of this line.\n"
        "@rtype: L{Vector3}\n"
      )
      .def(
        "getNormal",
        &Line2D::getNormal,
        "Returns the normal vector to this line.\n"
        "@rtype: L{Vector3}\n"
      )
      .def(self_ns::str(self))
      ;
    }
