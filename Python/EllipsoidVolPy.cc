/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "EllipsoidVolPy.h"

using namespace boost::python;

    void exportEllipsoidVol()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<EllipsoidVol, bases<AVolume3D> >(
        "EllipsoidVol",
        "A class defining an ellipsoid. Major axes are axis-aligned.",
        init<>()
      )
      .def(init<const EllipsoidVol &>())
      .def(
        init<Vector3,double,double,double>(
          ( arg("centre"), arg("Lx"), arg("Ly"), arg("Lz") ),
          "Constructs an ellipsoid with the specified centre and axis-aligned lengths. WARNING: The surface particles are not fitted to the ellipsoid surface, resulting in blob-like particle clusters.\n"
          "@type centre: L{Vector3}\n"
          "@kwarg centre: Coordinates of the centre of the ellipsoid\n"
          "@type Lx: double\n"
          "@kwarg Lx: Length of ellipsoid major axis in x-direction\n"
          "@type Ly: double\n"
          "@kwarg Ly: Length of ellipsoid major axis in y-direction\n"
          "@type Lz: double\n"
          "@kwarg Lz: Length of ellipsoid major axis in z-direction\n"
        )
      )
      .def(self_ns::str(self))
      ;
    }



