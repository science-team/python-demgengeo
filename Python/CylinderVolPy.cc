/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "CylinderVolPy.h"

using namespace boost::python;

    void exportCylinderVol()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<CylinderVol, bases<AVolume3D> >(
        "CylinderVol",
        "A class defining a cylindrical L{AVolume3D}.",
        init<>()
      )
      .def(init<const CylinderVol &>())
      .def(
        init<Vector3,Vector3,double,double>(
          ( arg("origin"), arg("axis"), arg("length"), arg("radius") ),
          "Constructs a cylinder with the specified origin, axis, length and radius.\n"
          "@type origin: L{Vector3}\n"
          "@kwarg origin: Coordinate of origin (centre of base) of the cylinder\n"
          "@type axis: L{Vector3}\n"
          "@kwarg axis: Unit vector in the direction of the cylinder axis\n"
          "@type length: double\n"
          "@kwarg length: Length of the cylinder\n"
          "@type radius: double\n"
          "@kwarg radius: Radius of the cylinder\n"
        )
      )
      .def(self_ns::str(self))
      ;
    }



