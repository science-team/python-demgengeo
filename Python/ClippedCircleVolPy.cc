/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "ClippedCircleVolPy.h"

using namespace boost::python;

    void exportClippedCircleVol()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<ClippedCircleVol, bases<AVolume2D> >(
        "ClippedCircleVol",
        "A class defining a circle in 2D space clipped by lines.",
        init<>()
      )
      .def(init<const ClippedCircleVol &>())
      .def(
        init<Vector3,double>(
          ( arg("centre"), arg("radius") ),
          "Constructs a circle with the specified centre and radius.\n"
          "@type centre: L{Vector3}\n"
          "@kwarg centre: Coordinates of the centre of the circle\n"
          "@type radius: double\n"
          "@kwarg radius: Radius of the circle\n"
        )
      )
      .def(
        "addLine",
        &ClippedCircleVol::addLine,
        ( arg("line"), arg("fit") ),
        "Adds a line to the area for clipping the circle.\n"
        "@type line: L{Line2D}\n"
        "@kwarg line: the line to add to the area\n"
        "@rtype: void\n"
      )
      .def(self_ns::str(self))
      ;
    }
