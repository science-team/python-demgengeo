/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef GENGEO_MNTABLE2D_H
#define GENGEO_MNTABLE2D_H

#include <boost/python.hpp>
#include "util/vector3.h"
#include "geometry/Line2D.h"
#include "src/MNTable2D.h"

void exportMNTable2D();

#endif
