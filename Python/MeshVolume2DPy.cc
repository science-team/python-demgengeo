#include <boost/version.hpp>
#include "MeshVolume2DPy.h"

using namespace boost::python;

void exportMeshVolume2D()
{
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

  class_<MeshVolume2D, bases<AVolume2D> >(
	"MeshVolume2D",
        "A class defining a volume bounded by a triangle mesh.",
        init<>()
      )
      .def(
        init<const LineSet& >(
          ( arg("Mesh")),
          "Constructs a 2D volume from a supplied set of line segments.\n"
          "@type Mesh: L{LineSet}\n"
          "@kwarg Mesh: The set of line segments\n"
        )
      )
      ;
}
