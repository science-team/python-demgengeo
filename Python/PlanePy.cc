/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "PlanePy.h"

using namespace boost::python;

    using boost::python::arg;
    void exportPlane()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<Plane>(
        "Plane",
        "A class defining a plane in 3D space.",
        init<>()
      )
      .def(init<const Plane &>())
      .def(
        init<Vector3,Vector3>(
          ( arg("origin"), arg("normal") ),
          "Constructs a plane with specifed origin and normal.\n"
          "N.B. This is not an L{AVolume3D} so cannot be used by C{InsertGenerators}.\n"
          "@type origin: L{Vector3}\n"
          "@kwarg origin: a point within the plane\n"
          "@type normal: L{Vector3}\n"
          "@kwarg normal: the normal of the plane (will be normalized)\n"
        )
      )
      .def(
        "getOrig",
        &Plane::getOrig,
        "Returns the origin point of this plane.\n"
        "@rtype: L{Vector3}\n"
      )
      .def(
        "getNormal",
        &Plane::getNormal,
        "Returns the normal vector to this plane.\n"
        "@rtype: L{Vector3}\n"
      )
      .def(self_ns::str(self))
      ;
    }
