/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "SpherePy.h"

using namespace boost::python;

    using boost::python::arg;
    void exportSphere()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

//      class_<Sphere, bases<AGeometricObject> >(
      class_<Sphere>(
        "Sphere",
        "A class defining a sphere.", 
        init<>()
      )
      .def(init<const Sphere &>())
      .def(
        init<Vector3,double>(
          ( arg("centre"), arg("radius") ),
          "Constructs a sphere with the specified centre and radius.\n"
	  "N.B. This is not a L{AVolume3D} so cannot be used by C{InsertGenerators}.\n"
          "@type centre: L{Vector3}\n"
          "@kwarg centre: the centre of the sphere\n"
          "@type radius: double\n"
          "@kwarg radius: the radius of the sphere\n"
        )
      )
      .def(
        "Centre",
        &Sphere::Center,
        "Returns the centre of the sphere.\n"
        "@rtype: L{Vector3}\n"
      )
      .def(
        "Radius",
        &Sphere::Radius,
        "Returns the radius of the sphere.\n"
        "@rtype: double\n"
      )
      .def(
        "Id",
        &Sphere::Id,
        "Returns the particle ID of the sphere.\n"
        "@rtype: int\n"
      )
      .def(
        "Tag",
        &Sphere::Tag,
        "Returns the particle tag of the sphere.\n"
        "@rtype: int\n"
      )
      .def(
        "setTag",
        &Sphere::setTag,
	( arg("Tag")),
        "Sets the particle tag of the sphere.\n"
        "@rtype: int\n"
      )
      .def(
        "setId",
        &Sphere::setId,
	( arg("Id")),
        "Sets the ID of the sphere.\n"
        "@rtype: int\n"
      )
      .def(self_ns::str(self))
      .def(self_ns::str(self))
      ;
    }
