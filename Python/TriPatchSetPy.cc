/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include "TriPatchSetPy.h"

using namespace std;

using namespace boost::python;

void exportTriPatchSet()
{
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

    class_<TriPatchSet>(
      "TriPatchSet",
      "A collection of triangular patches defining a joint set",
      init<>()
    )
    .def(init<TriPatchSet &>())
    .def("addTriangle",
	 &TriPatchSet::addTriangle,
	 ( boost::python::arg("Point1"),boost::python::arg("Point2"),boost::python::arg("Point2"),boost::python::arg("tag")),
	 "Adds a triangle by specifying corner coordinates\n"
         "@type Point1: L{Vector3}\n"
         "@kwarg Point1: location of first corner of the triangle\n"
         "@type Point2: L{Vector3}\n"
         "@kwarg Point2: location of second corner of the triangle\n"
         "@type Point3: L{Vector3}\n"
         "@kwarg Point3: location of third corner of the triangle\n"
         "@type tag: int\n"
         "@kwarg tag: the tag to assign to the triangle\n"
         )
    .def(
      "isCrossing",
      &TriPatchSet::isCrossing,
      (boost::python::arg("Point1"),boost::python::arg("Point2")),
      "Checks if the line between two specified points crosses a triangle. "
      "If so, the triangle tag is returned; if not, -1.\n"
      "@type Point1: L{Vector3}\n"
      "@kwarg Point1: location of first corner of the triangle\n"
      "@type Point2: L{Vector3}\n"
      "@kwarg Point2: location of second corner of the triangle\n"
      "@rtype: int\n"
    )
    .def(
      "getMinPoint",
      &TriPatchSet::getMinPoint,
      "Returns minimum corner of the joint set bounding box\n"
      "@rtype: L{Vector3}\n"
    )
    .def(
      "getMaxPoint",
      &TriPatchSet::getMaxPoint,
      "Returns maximum corner of the joint set bounding box\n"
      "@rtype: L{Vector3}\n"
    );
}
