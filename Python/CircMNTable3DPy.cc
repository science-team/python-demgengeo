/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include "CircMNTable3DPy.h"

using namespace std;

using namespace boost::python;

    using boost::python::arg;
    void exportCircMNTable3D()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<CircMNTable3D, bases<MNTable3D> >(
        "CircMNTable3D",
        "A multi-group neighbours table for constructing 3D particle setups with circular boundary conditions in the X-direction.",
        init<>()
      )
      .def(init<const CircMNTable3D &>())
      .def(
        init<Vector3&,Vector3&,double, unsigned int>(
          ( boost::python::arg("minPoint"), boost::python::arg("maxPoint"), boost::python::arg("gridSize"), boost::python::arg("numGroups") ),
          "Constructs a neighbours table with specified bounds, cell size and initial number of particle groups.\n"
          "@type minPoint: L{Vector3}\n"
          "@kwarg minPoint: lower-left-front point of the particle region\n"
          "@type maxPoint: L{Vector3}\n"
          "@kwarg maxPoint: upper-right-back point of the particle region\n"
          "@type gridSize: double\n"
          "@kwarg gridSize: the cell size for neighbour searches\n"
          "@type numGroups: unsigned int\n"
          "@kwarg numGroups: the initial number of groups\n"
        )
      )
      .def(
        "tagParticlesAlongPlane",
        &CircMNTable3D::tagParticlesAlongPlane,
        ( boost::python::arg("plane"), boost::python::arg("distance"), boost::python::arg("tag"), boost::python::arg("groupID")=0 ),
        "Assigns the specified tag to all particles from group C{groupID}\n"
        "that lie within the specified distance of the given plane.\n"
        "@type plane: L{Plane}\n"
        "@kwarg plane: the plane along which to tag particles\n"
        "@type distance: double\n"
        "@kwarg distance: the maximum distance between tagged particles and the plane\n"
        "@type tag: int\n"
        "@kwarg tag: the tag to assign particles\n"
        "@type groupID: unsigned int\n"
        "@kwarg groupID: the group ID of particles to tag (default: 0)\n"
        "@rtype: void\n"
      )
      .def(
        "generateBonds",
        &CircMNTable3D::generateBonds,
        (boost::python::arg("groupID")=0, boost::python::arg("tolerance"), boost::python::arg("bondID") ),
        "Generates bonds between particle pairs separated by less than the specified tolerance\n"
        "@type groupID: int\n"
        "@kwarg groupID: the group ID of particles to bond together (default: 0)\n"
        "@type tolerance: double\n"
        "@kwarg tolerance: maximum distance separating bonded particles\n"
        "@type bondID: int\n"
        "@kwarg bondID: the bond ID to assign generated bonds\n"
        "@rtype: void\n"
      )
      .def(
        "getSumVolume",
        &CircMNTable3D::getSumVolume,
        ( boost::python::arg("groupID")=0 ),
        "Returns the sum of the particle volumes in the specified group.\n"
        "@type groupID: int\n"
        "@kwarg groupID: the group ID of particles whose volumes are summed (default: 0).\n"
        "@rtype: double\n"
      )
      .def(
        "write",
        &CircMNTable3D::write,
        ( boost::python::arg("fileName"), boost::python::arg("outputStyle") ),
        "Writes the particle assembly and bonding information to the specified\n"
        "file using the specified output style (0: debug; 1: geo; 2: vtk)\n"
        "@type fileName: string\n"
        "@kwarg fileName: the name of the file to write\n"
        "@type outputStyle: int\n"
        "@kwarg outputStyle: output style (0: debug; 1: geo; 2: vtk)\n"
        "@rtype: void\n"
      )
      .def(
        "generateClusterBonds",
        &CircMNTable3D::generateClusterBonds,
        (boost::python::arg("groupID")=0, boost::python::arg("tolerance"), boost::python::arg("bondTag1"), boost::python::arg("bondTag2")),
	"Generates bonds with tag C{bondTag1} within clusters and C{bondTag2} between clusters\n"
      )
      .def(self_ns::str(self))
      ;
    }



