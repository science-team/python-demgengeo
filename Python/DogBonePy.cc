/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "DogBonePy.h"

using namespace boost::python;

    void exportDogBone()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<DogBone, bases<CylinderVol> >(
        "DogBone",
        "A class defining a dogbone volume.",
        init<>()
      )
      .def(init<const DogBone &>())
      .def(
        init<Vector3,Vector3,double,double,double,double>(
          ( arg("origin"), arg("axis"), arg("length"), arg("radius"), arg("l2"), arg("r2") ),
          "Constructs a dogbone volume with the specified dimensions.\n"
          "@type origin: L{Vector3}\n"
          "@kwarg origin: Coordinate of origin (centre of base) of the dogbone\n"
          "@type axis: L{Vector3}\n"
          "@kwarg axis: Unit vector in the direction of the dogbone axis\n"
          "@type length: double\n"
          "@kwarg length: Length of the dogbone\n"
          "@type radius: double\n"
          "@kwarg radius: Radius of the dogbone\n"
          "@type l2: double\n"
          "@kwarg l2: L2 of the dogbone\n"
          "@type r2: double\n"
          "@kwarg r2: R2 of the dogbone\n"
        )
      )
      .def(self_ns::str(self))
      ;
    }



