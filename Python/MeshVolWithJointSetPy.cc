/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "MeshVolWithJointSetPy.h"

using namespace boost::python;

void exportMeshVolWithJointSet()
{
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

  class_<MeshVolWithJointSet, bases<MeshVolume> >(
	"MeshVolWithJointSet",
        "A class defining a volume bounded by a triangle mesh containing joints.",
        init<>()
      )
      .def(
        init<const TriPatchSet& >(
          ( arg("Mesh")),
          "Constructs a volume from a supplied set of triangles.\n"
          "@type Mesh: L{TriPatchSet}\n"
          "@kwarg Mesh: The set of triangles\n"
        )
      )
      .def(
        "addJoints",
        &MeshVolWithJointSet::addJoints,
        ( arg("JointSet") ),
        "Adds a set of triangluar patches as joints.\n"
        "@type plane: L{Plane}\n"
        "@kwarg plane: the set of patches\n"
        "@rtype: void\n"
      )
      ;
}

