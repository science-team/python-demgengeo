/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "ShapeListPy.h"

using namespace boost::python;

void exportShapeList ()
{
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<ShapeList>(
        "ShapeList",
        "A list of 3D shapes to be inserted into a packing by replacing spheres with scaled versions of the specified shape.\n"
	"Shapes may be read from a shape database file and the probability with which a given shape is inserted into a packing may be specified.\n",
        init<>()
      )
      .def(
        "addHexShape",
        &ShapeList::addHexShape,
        ( arg("bias") ),
        ( arg("random") ),
        "Adds a hexagonal aggregate to the L{ShapeList}\n"
        "@type bias: int\n"
        "@kwarg bias: How often the shape should be added\n"
        "@type random: int\n"
        "@kwarg random: 1 to randomly orientate this shape, 0 to have all constant\n"
      )
      .def(
        "addGenericShape",
        &ShapeList::addGenericShape,
        ( arg("db"), 
        arg("name"),
        arg("bias"),
        arg("random"),
        arg("particleTag"),
        arg("bondTag") ),
        "Adds a shape from a database file into the list\n"
        "@type db: string\n"
        "@kwarg db: The filename of the database file\n"
        "@type name: string\n"
        "@kwarg name: The name of the shape to be added, as per database file\n"
        "@type bias: int\n"
        "@kwarg bias: The bias of the new shape; in other words, how often it will be added\n"
        "@type random: int\n"
        "@kwarg random: Whether the shape should be orientated randomly at each insertion:\n"
        "0 for not random, 1 for always random\n"
        "@type particleTag: int\n"
        "@kwarg particleTag: the ID of each particle inside each inserted shape\n"
        "@type bondTag: int\n"
        "@kwarg bondTag: the ID of each bond internal to each shape\n"
      )
      ;
}
