/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "BoxWithLines2DSubVolPy.h"
#include "src/BoxWithLines2DSubVol.h"

using namespace boost::python;

    void exportBoxWithLines2DSubVol()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<BoxWithLines2DSubVol, bases<BoxWithLines2D> >(
        "BoxWithLines2DSubVol",
        "A class defining a rectangular sub-volume in 2D bounded by lines.",
        init<>()
      )
      .def(init<const BoxWithLines2DSubVol &>())
      .def(
        init<Vector3,Vector3,double,double>(
          ( arg("minPoint"), arg("maxPoint"), arg("svdim_x"), arg("svdim_y") ),
          "Constructs a box with the specified corner points.\n"
          "@type minPoint: L{Vector3}\n"
          "@kwarg minPoint: Coordinate of bottom left corner of the box\n"
          "@type maxPoint: L{Vector3}\n"
          "@kwarg maxPoint: Coordinate of upper right corner of the box\n"
          "@type svdim_x: double\n"
          "@kwarg svdim_x: length of subvolumes in x-direction\n"
          "@type svdim_y: double\n"
          "@kwarg svdim_y: length of subvolumes in y-direction\n"
        )
      )
      .def(self_ns::str(self))
      ;
//      boost::python::implicitly_convertible<BoxWithLines2DSubVol, AVolume2D>();
    }



