/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include <boost/version.hpp>
#include "TriWithLines2DPy.h"
#include "src/TriWithLines2D.h"

using namespace boost::python;

    void exportTriWithLines2D()
    {
      // Disable autogeneration of C++ signatures (Boost 1.34.0 and higher)
      // for Epydoc which stumbles over indentation in the automatically generated strings.
      boost::python::docstring_options no_autogen(true,false);

      class_<TriWithLines2D, bases<AVolume2D> >(
        "TriWithLines2D",
        "A class defining a triangular L{AVolume2D} bounded by lines.",
        init<>()
      )
      .def(init<const TriWithLines2D &>())
      .def(
        init<Vector3,Vector3,Vector3>(
          ( arg("vertex0"), arg("vertex1"), arg("vertex2") ),
          "Constructs a triangle with the specified corner points.\n"
          "@type vertex0: L{Vector3}\n"
          "@kwarg vertex0: Coordinate of first corner of the triangle\n"
          "@type vertex1: L{Vector3}\n"
          "@kwarg vertex1: Coordinate of second corner of the triangle\n"
          "@type vertex2: L{Vector3}\n"
          "@kwarg vertex2: Coordinate of third corner of the triangle\n"
        )
      )
      .def(
        "addLine",
        &TriWithLines2D::addLine,
        ( arg("line") ),
        "Adds a line to the box for fitting particles.\n"
        "@type line: L{Line2D}\n"
        "@kwarg line: the line to add to the box\n"
        "@rtype: void\n"
      )
      .def(self_ns::str(self))
      ;
//      boost::python::implicitly_convertible<TriWithLines2D, AVolume2D>();
    }



