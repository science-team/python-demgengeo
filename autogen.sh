#!/bin/sh
#
# Script for generating the autoconf configure script
#
libtoolize --ltdl --force --copy --automake # version >= 1.5.2
aclocal -I m4                               # version >= 1.11 for Python 3, 1.8.2 for Python 2.6-2.7
autoheader                                  # version >= 2.59
automake -a -c                              # version >= 1.11 for Python 3, 1.8.2 for Python 2.6-2.7
autoconf                                    # version >= 2.59
