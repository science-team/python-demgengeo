/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __AGEOMETRICOBJECT_H
#define __AGEOMETRICOBJECT_H

#include "util/vector3.h"

class AGeometricObject
{
 protected:
 public:
  virtual ~AGeometricObject(){};

  virtual double getDist(const Vector3&) const=0;
};

#endif// __AGEOMETRICOBJECT_H  
