/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __TRIANGLE_3D
#define __TRIANGLE_3D

// project includes
#include "util/vector3.h"
#include "AGeometricObject.h"

/*!
  class for a triagle in 3D.
*/
class Triangle3D : public AGeometricObject
{
 private:
  Vector3 m_p1,m_p2,m_p3;
  int m_tag;

 public:
  Triangle3D(const Vector3&,const Vector3&,const Vector3&,int);

  virtual double getDist(const Vector3&) const;
  bool crosses(const Vector3&, const Vector3&) const;
  int getTag() const {return m_tag;};
  Vector3 getMinPoint() const;
  Vector3 getMaxPoint() const;
};

#endif // __TRIANGLE_3D
