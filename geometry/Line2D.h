/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __LINE2D_H
#define __LINE2D_H

// --- Project includes ---
#include "util/vector3.h"
#include "AGeometricObject.h"

// --- IO includes ---
#include <iostream>

using std::ostream;

/*!
  \brief class for a line in 2D
*/ 
class Line2D : public AGeometricObject
{
 protected:
  Vector3 m_p1,m_p2;
  Vector3 m_normal;

 public:
  Line2D();
  Line2D(const Vector3&,const Vector3&);

  Vector3 intersect(const Line2D&) const;
  Line2D parallel(double);
  virtual double getDist(const Vector3&) const;
  Vector3 getOrig() const {return m_p1;};
  Vector3 getNormal() const {return m_normal;};
  
  friend ostream& operator<< (ostream&, const Line2D&);
};

#endif // __LINE2D_H 
