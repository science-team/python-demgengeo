/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "LineSegment2D.h"

// -- system includes --
#include <cmath>

using std::fabs;

/*!
  default constructor -> zero all data
*/
LineSegment2D::LineSegment2D() : Line2D()
{}

/*!
  Construct a line segment from 2 points. The order of the points determines
  the direction of the normal.

  \param p1 1st point
  \param p2 2nd point

  \warning doesn't check p1!=p2
*/
LineSegment2D::LineSegment2D(const Vector3& p1,const Vector3& p2) : Line2D(p1,p2)
{}


/*!
  Construct a line segment from 2 points. The order of the points determines
  the direction of the normal.

  \param p1 1st point
  \param p2 2nd point
  \param tag the tag 

  \warning doesn't check p1!=p2
*/
LineSegment2D::LineSegment2D(const Vector3& p1,const Vector3& p2, int tag) : Line2D(p1,p2)
{
	m_tag=tag;
}


/*!
  Get the distance of a point from the line segment

  \param p the point
*/
double LineSegment2D::getDist(const Vector3& p) const
{
  double res;

  double du=(p-m_p1)*((m_p2-m_p1).unit());
  if((0<=du) && (du <=(m_p2-m_p1).norm())){// nearest point inside segment
    res=fabs((p-m_p1)*m_normal); 
  } else { // nearest point  outside -> get distance to closest endpoint
    double d1=(p-m_p1).norm();
    double d2=(p-m_p2).norm();
    res = (d1<d2) ? d1 : d2;
  }

  return res;
}

/*!
  get minimum corner of the bounding box
*/
Vector3 LineSegment2D::getMinPoint() const
{
  double xmin=(m_p1.x() < m_p2.x()) ? m_p1.x() : m_p2.x();
  double ymin=(m_p1.y() < m_p2.y()) ? m_p1.y() : m_p2.y();

  return Vector3(xmin,ymin,0.0);
}

/*!
  get maximum corner of the bounding box
*/
Vector3 LineSegment2D::getMaxPoint() const
{
  double xmax=(m_p1.x() > m_p2.x()) ? m_p1.x() : m_p2.x();
  double ymax=(m_p1.y() > m_p2.y()) ? m_p1.y() : m_p2.y();

  return Vector3(xmax,ymax,0.0);
}

/*!
	check if line between 2 points intersects line segment 	 
*/ 
bool LineSegment2D::crosses(const Vector3& p1, const Vector3& p2) const
{
  bool res=false;

  Vector3 intersection=this->intersect(Line2D(p1,p2));
  Vector3 d1=intersection-m_p1; // difference between intersection and starting point
  Vector3 d2=intersection-p1; // difference between intersection and starting point of crossing line

  double f1=dot(d1,m_p2-m_p1)/(m_p2-m_p1).norm2(); // normalized distance along line
  double f2=dot(d2,p2-p1)/(p2-p1).norm2(); // normalized distance along crossing line

  res=(f1>0.0) && (f1<1.0) && (f2>0.0) && (f2<1.0);
  	
  return res;
}

/*!
  output line segment
*/
ostream& operator<< (ostream& ost, const LineSegment2D& L)
{
  ost << L.m_p1 << "-" << L.m_p2;

  return ost;
}
