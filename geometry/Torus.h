/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __TORUS_H
#define __TORUS_H

//-- project includes --
#include "util/vector3.h"

// --- project includes ---
#include "AGeometricObject.h"

class Torus  : public AGeometricObject
{
 private:
  Vector3 m_c;
  Vector3 m_axis;
  double m_r1,m_r2;
  bool m_inside;

 public:
  Torus(const Vector3&,const Vector3&,double,double,bool);
  Torus(){};
  ~Torus(){};

  virtual double getDist(const Vector3&) const;
  double getInnerRadius() const {return m_r1;};
  double getOuterRadius() const {return m_r2;};
  Vector3 getBasePoint() const {return m_c;};
  Vector3 getAxis() const {return m_axis;};
};

#endif //__TORUS_H
