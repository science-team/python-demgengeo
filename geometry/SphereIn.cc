/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "SphereIn.h"

SphereIn::SphereIn():Sphere()
{}

SphereIn::SphereIn(const Vector3& V,double r): Sphere(V,r)
{}

/*!
  Get distance between a point and a cylindical surface. 
  The function returns the _absolute_ distance, i.e. the result 
  is always positive.

  \param P the point
*/
double SphereIn::getDist(const Vector3& P) const
{
  return fabs(m_rad-(P-m_center).norm());
}

/*!
  Get distance between a point and a cylindical surface. 
  The function returns the directed distance assuming an 
  inward facing surface normal, i.e. the result is positive 
  if the point is inside the cylinder and negative if it is 
  outside.

  \param P the point
*/
double SphereIn::getDirDist(const Vector3& P) const
{
  return m_rad-(P-m_center).norm();
}
