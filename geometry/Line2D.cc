/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "Line2D.h"

//-- system includes --
#include <cmath>

using std::fabs;

/*!
  default constructor -> zero all data
*/
Line2D::Line2D()
{}

/*!
  Construct a line from 2 points. The order of the points determines
  the direction of the normal.

  \param p1 1st point
  \param p2 2nd point

  \warning doesn't check p1!=p2
*/
Line2D::Line2D(const Vector3& p1,const Vector3& p2)
{
  m_p1=p1;
  m_p2=p2;
  Vector3 r=(m_p2-m_p1).unit();
  m_normal=(Vector3(r.Y(),-1.0*r.X(),0.0)).unit();
}
  
/*!
  Calculate the intersection with another line.

  \param L the other line
*/
Vector3 Line2D::intersect(const Line2D& L) const
{
  Vector3 r0=m_p2-m_p1;
  Vector3 r1=L.m_p2-L.m_p1;
  Vector3 d=L.m_p1-m_p1;
  double c1=r0.Y()*r1.X()-r0.X()*r1.Y();
  double c2=d.Y()*r0.X()-d.X()*r0.Y();

  return L.m_p1+(c2/c1)*r1;
}

/*!
  Construct a line parallel to the current line at a given distance. The direction
  of the normal determines on which side the parallel is constructed.

  \param d the distance
*/
Line2D Line2D::parallel(double d)
{
  Vector3 p1=m_p1+d*m_normal;
  Vector3 p2=m_p2+d*m_normal;

  return Line2D(p1,p2);
}


/*!
  Get the distance of a point from the line

  \param p the point
*/
double Line2D::getDist(const Vector3& p) const
{
  return fabs((p-m_p1)*m_normal);
}

ostream& operator<< (ostream& ost, const Line2D& L)
{
  ost << L.m_p1 << " to " << L.m_p2;

  return ost;
}
