/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __SPHERE_IN_H
#define __SPHERE_IN_H

//--- project includes ---
#include "util/vector3.h"

//--- project includes ---
#include "Sphere.h"

class SphereIn : public Sphere
{
 public:
  SphereIn();
  SphereIn(const Vector3&,double);

  virtual double getDist(const Vector3&) const;
  virtual double getDirDist(const Vector3&) const;
};



#endif // __SPHERE_IN_H
