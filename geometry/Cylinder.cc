/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "Cylinder.h"

Cylinder::Cylinder()
{
}

Cylinder::Cylinder(const Vector3& c,const Vector3& axis,double r)
{
  m_c=c;
  m_axis=axis;
  m_r=r;
}

/*!
  Get distance between a point and a cylindical surface. 
  The function returns the _absolute_ distance, i.e. the result 
  is always positive.

  \param P the point
*/
double Cylinder::getDist(const Vector3& P) const
{
  Vector3 v1=P-m_c;
  double d=m_axis*v1;
  return fabs(m_r-(v1-d*m_axis).norm());
}

/*!
  Get distance between a point and a cylindical surface. 
  The function returns the directed distance assuming an 
  inward facing surface normal, i.e. the result is positive 
  if the point is inside the cylinder and negative if it is 
  outside.

  \param P the point
*/
double Cylinder::getDirDist(const Vector3& P) const
{
  Vector3 v1=P-m_c;
  double d=m_axis*v1;
  return m_r-(v1-d*m_axis).norm();
}
