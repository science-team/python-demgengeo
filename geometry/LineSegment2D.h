/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __LINESEGMENT2D_H
#define __LINESEGMENT2D_H

// --- Project includes ---
#include "Line2D.h"

/*!
  \brief class for a line in 2D
*/ 
class LineSegment2D : public Line2D
{
 protected:
  int m_tag;
		
 public:
  LineSegment2D();
  LineSegment2D(const Vector3&,const Vector3&);
  LineSegment2D(const Vector3&,const Vector3&,int);
  virtual ~LineSegment2D(){};

  virtual double getDist(const Vector3&) const;
  int getTag() const {return m_tag;};
  Vector3 getMinPoint() const;
  Vector3 getMaxPoint() const;
  bool crosses(const Vector3&, const Vector3&) const;
		
  friend ostream& operator<< (ostream&, const LineSegment2D&);
};

#endif // __LINESEGMENT2D_H 
