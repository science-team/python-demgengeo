/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __SPHERE_H
#define __SPHERE_H

//-- project includes --
#include "util/vector3.h"

// --- IO includes ---
#include <iostream>

using std::ostream;

// --- project includes ---
#include "AGeometricObject.h"

/*! 
  \class Sphere
  \brief Simple sphere implementation. Works both 2D and 3D 
*/ 
class Sphere : public AGeometricObject
{
 protected:
  Vector3 m_center;
  double m_rad;
  int m_id;
  int m_tag;
  bool m_valid;

  static double NearZero;
  static int s_output_style;
  static unsigned int s_current_id;

 public:
  // constructors
  Sphere();
  Sphere(const Vector3&,double);
  Sphere(const Sphere&);

  // access ops and the like
  virtual double getDist(const Vector3&) const;
  inline Vector3 Center() const {return m_center;};
  inline double Radius() const {return m_rad;};
  inline int Id() const {return m_id;};
  inline int Tag() const {return m_tag;};
  void setTag(int t){m_tag=t;};
  void setId(int);

  // move it
  void shift(const Vector3& s){m_center+=s;};

  // output
  static void SetOutputStyle(int);
  friend ostream& operator << (ostream&,const Sphere&);
} ;

#endif // __SPHERE_H
