/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __PLANE_H
#define __PLANE_H

// --- Project includes ---
#include "util/vector3.h"
#include "AGeometricObject.h"

// --- IO includes ---
#include <iostream>

using std::ostream;

/*!
  \brief class for a plane in 3D
*/
class Plane : public AGeometricObject
{
 private:
  Vector3 m_p,m_normal;

 public:
  Plane();
  Plane(const Vector3&,const Vector3&);

  virtual double getDist(const Vector3&) const;
  Vector3 getOrig() const {return m_p;};
  Vector3 getNormal() const {return m_normal;};
  
  friend ostream& operator<< (ostream&, const Plane&);
};

#endif // __PLANE_H
