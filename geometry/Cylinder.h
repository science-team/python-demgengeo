/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __CYLINDER_H
#define __CYLINDER_H

//-- project includes --
#include "util/vector3.h"

// --- project includes ---
#include "AGeometricObject.h"

class Cylinder  : public AGeometricObject
{
 private:
  Vector3 m_c;
  Vector3 m_axis;
  double m_r;

 public:
  Cylinder();
  Cylinder(const Vector3&,const Vector3&,double);
  ~Cylinder(){};

  virtual double getDirDist(const Vector3&) const;
  virtual double getDist(const Vector3&) const;
  double getRadius() const {return m_r;};
  Vector3 getBasePoint() const {return m_c;};
  Vector3 getAxis() const {return m_axis;};

};

#endif // __CYLINDER_H
