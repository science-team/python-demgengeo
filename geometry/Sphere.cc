/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "Sphere.h"

// --- System includes ---
#include <cmath>

using std::sqrt;

double Sphere::NearZero=1e-8;
int Sphere::s_output_style=0;
unsigned int Sphere::s_current_id=0;

/*!
  construct "invalid" Sphere
*/
Sphere::Sphere()
{
  m_valid=false;
  m_id=0;
  m_tag=0;
}

/*!
  construct valid Sphere

  \param center the center of the circle
  \param rad the radius
*/
Sphere::Sphere(const Vector3& center,double rad)
{
  m_center=center;
  m_rad=rad;
  m_valid=true;
  m_id=Sphere::s_current_id;
  Sphere::s_current_id++;
  m_tag=0;
}

/*!
  copy constructor

  \param S the original
*/
Sphere::Sphere(const Sphere& S)
{
  m_center=S.m_center;
  m_rad=S.m_rad;
  m_valid=S.m_valid;
  m_id=S.m_id;
  m_tag=S.m_tag; 
}

/*!
  get distance between given point and the surface of the Sphere

  \param P the point
*/
double  Sphere::getDist(const Vector3& P) const
{
  return (P-m_center).norm()-m_rad;
}


/*!
  Set id of sphere. If higher than current_id, increase current_id
  accordingly

  \param i the new id of the sphere
*/
void Sphere::setId(int i)
{
  m_id=i;
  if(i>s_current_id) {
    s_current_id=i;
  }
}

/*!
  Set output style

  \param style the output style: 0=Debug, 1=.geo
*/
void Sphere::SetOutputStyle(int style)
{
  Sphere::s_output_style=style;
}

ostream& operator << (ostream& ost, const Sphere& S)
{
  if(Sphere::s_output_style==0){
    if(S.m_valid){
      ost << S.m_center << " | " << S.m_rad << " |  " << S.m_id; 
    } else {
      ost << "invalid Sphere";
    }
  } else if (Sphere::s_output_style==1){
    if(S.m_valid){
      ost << S.m_center << " "  << S.m_rad << " " << S.m_id << " " << S.m_tag;
    } 
  }
  
  return ost;
}
