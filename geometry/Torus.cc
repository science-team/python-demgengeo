/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "Torus.h"

// --- system includes ---
#include <cmath>

using std::sqrt;

/*!
  \param c base point
  \param axis axis through the middle of the torus
  \param r1 outer radius
  \param r2 inner (tube) radius
  \param inside calc distance inside/outside
*/
Torus::Torus(const Vector3& c,const Vector3& axis,double r1,double r2,bool inside)
{
  m_c=c;
  m_axis=axis.unit();;
  m_r1=r1;
  m_r2=r2;
  m_inside=inside;
}

double Torus::getDist(const Vector3& P) const
{
  Vector3 p0=(P-m_c);
  double h=m_axis*p0;
  double e=(p0-h*m_axis).norm();
  double res=sqrt((m_r1-e)*(m_r1-e)+h*h)-m_r2;

  if(m_inside) res=-1.0*res;

  return res;
}
