/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "Plane.h"

// --- system includes ---
#include <cmath>

using std::fabs;

Plane::Plane()
{
  m_p=Vector3(0.0,0.0,0.0);
  m_normal=Vector3(1.0,0.0,0.0);
}

/*!
  construct plane from origin and normal

  \param orig a point within the plane
  \param normal the normal of the plane (will be normalized)
*/
Plane::Plane(const Vector3& orig,const Vector3& normal)
{
  m_p=orig;
  m_normal=normal.unit();
}

/*!
  Get the distance of a point from the line

  \param p the point
*/
double Plane::getDist(const Vector3& p) const
{
  return fabs((p-m_p)*m_normal);
}

ostream& operator<< (ostream& ost, const Plane& P)
{
  ost << P.m_p << "-" << P.m_normal;
  return ost;
}
