/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "vector3.h"
#include "vector3.hh"

const Vector3 Vector3::ZERO = Vector3(0.0, 0.0, 0.0);

bool Vector3::operator<(const Vector3& rhs) const
{
  bool res;

  if(data[0]!=rhs.data[0]) {
    res=data[0]<rhs.data[0];
  } else if(data[1]!=rhs.data[1]){
    res=data[1]<rhs.data[1];
  } else if(data[2]!=rhs.data[2]){
    res=data[2]<rhs.data[2];
  } else {
    res=false;
  }

  return res;
}
