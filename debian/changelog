python-demgengeo (1.4-7) unstable; urgency=medium

  * Team upload
  * Fix FTBFS due to missing gettimeofday() declaration (Closes: #1092360)

 -- Alexandre Detiste <tchet@debian.org>  Sat, 11 Jan 2025 15:14:15 +0100

python-demgengeo (1.4-6) unstable; urgency=medium

  * Team upload
  * Add dep on python3-setuptools (Closes: #1080858)
  * Use dh-sequence-python3

 -- Alexandre Detiste <tchet@debian.org>  Sat, 09 Nov 2024 23:46:07 +0100

python-demgengeo (1.4-5) unstable; urgency=medium

  * Team upload
  [ zhangdandan <zhangdandan@loongson.cn> ]
  * Add support for loongarch64 (Closes: #1059276)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 13 Aug 2024 11:57:30 +0200

python-demgengeo (1.4-4.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/control: Switch to libltdl-dev (Closes: #1008886)

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 06 Apr 2022 10:22:59 +0200

python-demgengeo (1.4-4) unstable; urgency=medium

  * Team upload.
  * Fix build with Python3.10 and Python3.11

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 17 Jan 2022 13:56:57 +0100

python-demgengeo (1.4-3) unstable; urgency=medium

  * Team upload.
  * Fix build on hppa, alpha, ia64, m68k and riscv64 with autoconf patch
    (Closes: #829143)
    - thanks John David Anglin and Michael Cree for the patches!

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 07 Jan 2020 08:29:43 +0100

python-demgengeo (1.4-2) unstable; urgency=medium

  * Team upload.
  * Reupload as source-only to allow testing migration

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sat, 04 Jan 2020 10:42:35 +0100

python-demgengeo (1.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream version.
  * Update to debhelper-compat (= 12).
  * Drop Python 2 module package (Closes: #937697).
  * Build Python 3 module package.
  * Rename module package to be policy compliant.
  * Update Vcs-* fields.
  * Fix copyright-format/1.0 URL.
  * Add Rules-Requires-Root: no.
  * Use autopkgtest provided tmp directory.
  * Update to Standards-Version 4.4.1.
  * Change priority from extra to optional.

 -- Stuart Prescott <stuart@debian.org>  Thu, 26 Dec 2019 17:49:34 +1100

python-demgengeo (1.2-1.2) unstable; urgency=medium

  * Replace previous NMU with a new source-only upload.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Thu, 08 Aug 2019 02:27:56 +0000

python-demgengeo (1.2-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Eliminate dependency on epydoc, which will be removed (closes: #881556)
    - Update debian/control to remove Build-Depends: python-epydoc
    - Update debian/rules to remove --enable-docs in override_dh_auto_configure
    - Remove debian/python-demgengeo.docs and debian/python-demgengeo.doc-base

 -- Kenneth J. Pronovici <pronovic@debian.org>  Fri, 02 Aug 2019 17:10:23 +0000

python-demgengeo (1.2-1) unstable; urgency=medium

  * Upload into unstable

 -- Anton Gladky <gladk@debian.org>  Sat, 18 Jul 2015 08:20:39 +0300

python-demgengeo (1.2-1~exp1) experimental; urgency=medium

  * [5e4a914] Imported Upstream version 1.2
  * [bcb72c0] Modify git-orig script.
  * [eed48bf] Clean in patches.
  * [301c7c4] Fix doc path.
  * [bd48e8f] Fix rpath-issue.

 -- Anton Gladky <gladk@debian.org>  Sat, 11 Jul 2015 12:37:34 +0200

python-demgengeo (1.0-4) unstable; urgency=medium

  * [5282dfa] Enable gcc5-patch. Typo in previous upload.

 -- Anton Gladky <gladk@debian.org>  Fri, 10 Jul 2015 22:04:01 +0200

python-demgengeo (1.0-3) unstable; urgency=medium

  [ Anton Gladky ]
  * [ed0d287] Apply cme fix dpkg-control.
  * [8411898] Fix d/copyright.

  [ Matthias Klose ]
  * [593e1d5] Call cpp with -P. (Closes: #778075)

 -- Anton Gladky <gladk@debian.org>  Fri, 10 Jul 2015 21:00:42 +0200

python-demgengeo (1.0-2) unstable; urgency=medium

  * [25149fb] Add autopkgtest.
  * [3c880b8] Replace cerr output by cout.

 -- Anton Gladky <gladk@debian.org>  Wed, 14 May 2014 20:37:59 +0200

python-demgengeo (1.0-1) unstable; urgency=medium

  * [332d285] Imported Upstream version 1.0
  * [d15d08f] Ignore quilt dir

 -- Anton Gladky <gladk@debian.org>  Wed, 12 Feb 2014 20:40:13 +0100

python-demgengeo (0.99~bzr124-1) unstable; urgency=medium

  * [830010a] Imported Upstream version 0.99~bzr124
  * [95b1c79] Use wrap-and-sort.
  * [c66b7cd] Update dates in copyright.
  * [385f106] Set Standards-Version: 3.9.5. No changes.
  * [61aeefd] Set canonical VCS-fields.

 -- Anton Gladky <gladk@debian.org>  Sun, 26 Jan 2014 13:45:27 +0100

python-demgengeo (0.99~bzr117-1) unstable; urgency=low

  * [deb483f] Imported Upstream version 0.99~bzr117
  * [05b72d9] Add script to get an upstream tarball from bzr.
  * [ac09ea7] Remove license of debian-files from copyright file
  * [e336ee8] Use compat level 9.
  * [2215171] Use --parallel option for building.
  * [1a0a967] Minor update in .docs file.
  * [a3cf6de] Bumped Standards-Version 3.9.4 (no changes).

 -- Anton Gladky <gladk@debian.org>  Wed, 08 May 2013 22:03:08 +0200

python-demgengeo (0.99~bzr106-1) unstable; urgency=low

  * Initian Debian packaging. (Closes: #649001)

 -- Anton Gladky <gladky.anton@gmail.com>  Fri, 18 Nov 2011 21:47:18 +0100
