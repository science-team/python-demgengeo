#!/bin/bash

# The script creates a tar.xz tarball from git-repository of LAMMPS-project
# ./get_orig_src.sh commitID   -   creates a tarball of specified commit
# ./get_orig_src.sh   - creates a tarball of the latest version
# Packages, that needs to be installed to use the script:
# atool, bzr

bzr checkout lp:esys-particle/gengeo 
cd gengeo

BZR_REV=$(bzr revno)
VER_DEB=1.3~bzr$BZR_REV
FOLDER_NAME=python-demgengeo-$VER_DEB
TARBALL_NAME=python-demgengeo_$VER_DEB.orig.tar.xz


echo $VER_DEB
echo $FOLDER_NAME
echo $TARBALL_NAME

cd ..
mv gengeo $FOLDER_NAME
rm -rf $FOLDER_NAME/.bzr
apack $TARBALL_NAME $FOLDER_NAME
rm -rf $FOLDER_NAME
