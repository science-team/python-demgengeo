#############################################################
##                                                         ##
## Copyright (c) 2007-2017 by The University of Queensland ##
## Centre for Geoscience Computing                         ##
## http://earth.uq.edu.au/centre-geoscience-computing      ##
##                                                         ##
## Primary Business: Brisbane, Queensland, Australia       ##
## Licensed under the Open Software License version 3.0    ##
## http://www.apache.org/licenses/LICENSE-2.0              ##
##                                                         ##
#############################################################

from __future__ import division, print_function
from gengeo     import *
#An example python script to generate a bonded rectangular prism

# Define region extremities:
maxRadius = 1.0
size = 10.0
minPoint = Vector3(0.0,0.0,0.0)
maxPoint = Vector3(size,2.0*size,size)

# Define the volume to be filled with spheres:
# 	(e.g. a box bounded by planes)
box = BoxWithPlanes3D (
   minPoint = minPoint,
   maxPoint = maxPoint
)

box.addPlane(
   Plane(
      origin = minPoint, 
      normal = Vector3(1.0,0.0,0.0)
   )
)
#or the compact form:
box.addPlane(Plane(minPoint, Vector3(0.0,1.0,0.0)))
box.addPlane(Plane(minPoint, Vector3(0.0,0.0,1.0)))
box.addPlane(Plane(maxPoint, Vector3(-1.0,0.0,0.0)))
box.addPlane(Plane(maxPoint, Vector3(0.0,-1.0,0.0)))
box.addPlane(Plane(maxPoint, Vector3(0.0,0.0,-1.0)))

# Create a multi-group neighbour table to contain the particles:
mntable = MNTable3D (
   minPoint = minPoint,
   maxPoint = maxPoint,
   gridSize = 2.5*maxRadius
)

# Fill the volume with particles:
packer = InsertGenerator3D (
   minRadius = 0.2,
   maxRadius = maxRadius,
   insertFails = 1000,
   maxIterations = 1000,
   tolerance = 1.0e-6
)

# Instruct packer to generate the particle packing
packer.generatePacking(
   volume = box, 
   ntable = mntable
)

# create bonds between neighbouring particles:
mntable.generateBonds(
   tolerance = 1.0e-5,
   bondID = 0
)

# print the porosity:
volume = 2.0*size*size*size
porosity = (volume - mntable.getSumVolume())/volume
print("Porosity:  ", porosity)

# write the geometry to a VTK file
mntable.write(
   fileName = "temp/geo_example2.vtu",
   outputStyle = 2
)

# write the geometry to an ESyS-Particle geo file
mntable.write(
   fileName = "temp/geo_example2.geo",
   outputStyle = 1
)
