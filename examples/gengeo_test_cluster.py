#############################################################
##                                                         ##
## Copyright (c) 2007-2017 by The University of Queensland ##
## Centre for Geoscience Computing                         ##
## http://earth.uq.edu.au/centre-geoscience-computing      ##
##                                                         ##
## Primary Business: Brisbane, Queensland, Australia       ##
## Licensed under the Open Software License version 3.0    ##
## http://www.apache.org/licenses/LICENSE-2.0              ##
##                                                         ##
#############################################################

#from __future__ import print_function
from __future__ import division
from gengeo     import Sphere, Vector3, Line2D, BoxWithLines2D, MNTable2D, InsertGenerator2D
from random     import random

# An example python script to generate a bonded rectangle of particles
# and cluster the particles inside the rectangle 

# Define region extremities:
xsize=20.0
ysize=20.0
minPoint = Vector3(0.0,0.0,0.0)
maxPoint = Vector3(xsize,ysize,0.0)

# Define the geometrical constraints for packing
# 	(e.g. lines bordering a rectangular region in 2D)
top_line = Line2D (
   startPoint = Vector3(xsize,0.0,0.0),
   endPoint = minPoint
)

bottom_line = Line2D (
   startPoint = maxPoint,
   endPoint = Vector3(0.0,ysize,0.0)
)

left_line = Line2D (
   startPoint = Vector3(xsize,0.0,0.0),
   endPoint = maxPoint
)

right_line = Line2D (
   startPoint = minPoint,
   endPoint = Vector3(0.0,ysize,0.0)
)

# Define the Volume to be filled with spheres:
box = BoxWithLines2D (
   minPoint = minPoint,
   maxPoint = maxPoint
)

box.addLine(top_line)
box.addLine(bottom_line)
box.addLine(left_line)
box.addLine(right_line)

# Create a multi-group neighbour table to contain the particles:
mntable = MNTable2D (
   minPoint = minPoint,
   maxPoint = maxPoint,
   gridSize = 2.5,
   numGroups = 2
)

# Fill the volume with particles:
packer = InsertGenerator2D (
   minRadius = 0.1,
   maxRadius = 1.0,
   insertFails = 1000,
   maxIterations = 1000,
   tolerance = 1.0e-6,
   seed=0
)

packer.generatePacking( volume = box, ntable = mntable, groupID = 0, tag = 0)

# insert  zero-radius particles in group 2 as a cluster seeds
# use randomly disturbed square grid
nx=5
ny=5
for i in range(nx):
    for j in range(ny):
        xpos=(float(i)+random())*xsize/float(nx)
        ypos=(float(j)+random())*ysize/float(ny)
        cseed=Sphere(Vector3(xpos,ypos,0.0),0.0)
#        cseed.setTag(i*ny+j)
        cseed.setTag(int(random()*nx*ny))
        mntable.insert(
           sphere=cseed,
           groupID=1
        )
        #print("xpos: ",xpos)

mntable.tagParticlesToClosest(
   groupID1=0,
   groupID2=1
)

# create cluster bonds between neighbouring particles
# bonds between particles with the same tag will have bondTag1
# bonds between particles with different tags will have bondTag2
mntable.generateClusterBonds(
   groupID = 0,
   tolerance = 1.0e-5,
   bondTag1 = 0,
   bondTag2 = 1,
)


# write a geometry file
mntable.write(
   fileName = "temp/cluster.geo",
   outputStyle = 1
)

# write a vtk file
mntable.write(
   fileName = "temp/cluster.vtu",
   outputStyle = 2
)
