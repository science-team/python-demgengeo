#############################################################
##                                                         ##
## Copyright (c) 2007-2017 by The University of Queensland ##
## Centre for Geoscience Computing                         ##
## http://earth.uq.edu.au/centre-geoscience-computing      ##
##                                                         ##
## Primary Business: Brisbane, Queensland, Australia       ##
## Licensed under the Open Software License version 3.0    ##
## http://www.apache.org/licenses/LICENSE-2.0              ##
##                                                         ##
#############################################################

from gengeo import Vector3, Line2D, BoxWithLines2D, MNTable2D, InsertGenerator2D, CircleVol

# An example python script to generate a 2D rectangle of particles
# containing a circular cluster of bonded particles

# Define region extremities:
minPoint = Vector3(0.0,0.0,0.0)
maxPoint = Vector3(10.0,10.0,0.0)

# Define the volume to be filled with spheres:
box = BoxWithLines2D (
   minPoint = minPoint,
   maxPoint = maxPoint
)

# Define the geometrical constraints for packing
top_line = Line2D (
   startPoint = Vector3(10.0,0.0,0.0),
   endPoint = minPoint
)

bottom_line = Line2D (
   startPoint = maxPoint,
   endPoint = Vector3(0.0,10.0,0.0)
)

left_line = Line2D (
   startPoint = Vector3(10.0,0.0,0.0),
   endPoint = maxPoint
)

right_line = Line2D (
   startPoint = minPoint,
   endPoint = Vector3(0.0,10.0,0.0)
)


box.addLine(top_line)
box.addLine(bottom_line)
box.addLine(left_line)
box.addLine(right_line)

# Create a multi-group neighbour table to contain the particles:
mntable = MNTable2D (
   minPoint = minPoint,		# specific regional extremities
   maxPoint = maxPoint,
   gridSize = 2.5		# neighbour search algorithm grid size
)

# Fill the volume with particles:
packer = InsertGenerator2D (
   minRadius = 0.1,		# minimum radius of spheres to insert
   maxRadius = 0.4,		# maximum radius of spheres to insert
   insertFails = 5000,		# maximum number of insertion failures
   maxIterations = 1000,	# max. iterations for Newton-Raphson solver
   tolerance = 1.0e-6		# max. allowable overlap of spheres
)

packer.generatePacking( volume = box, ntable = mntable, tag = 0)

# define circular area
center = Vector3(5.0,5.0,0.0)
radius = 2.5

circ_area=CircleVol(center,radius)

# tag particles in circular area
mntable.tagParticlesInVolume(circ_area,2,0)


# create bonds between neighbouring particles with tag 2:
mntable.generateBondsWithMask(
   tolerance = 1.0e-5,	# max. gap between "touching" particles
   bondID = 0,		# bond ID of created bonds
   tag = 2,
   mask = 2
)

# write the geometry to a file in raw format
mntable.write(
   fileName = "temp/geo_example11.txt",
   outputStyle = 0
)

# write the geometry to an ESyS-Particle geometry file
mntable.write(
   fileName = "temp/geo_example11.geo",
   outputStyle = 1
)

# write the geometry as a VTK file
mntable.write(
   fileName = "temp/geo_example11.vtu",
   outputStyle = 2
)
