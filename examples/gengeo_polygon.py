#############################################################
##                                                         ##
## Copyright (c) 2007-2017 by The University of Queensland ##
## Centre for Geoscience Computing                         ##
## http://earth.uq.edu.au/centre-geoscience-computing      ##
##                                                         ##
## Primary Business: Brisbane, Queensland, Australia       ##
## Licensed under the Open Software License version 3.0    ##
## http://www.apache.org/licenses/LICENSE-2.0              ##
##                                                         ##
#############################################################

from __future__ import division
from gengeo     import *
from random     import random

#An example python script to generate a bonded rectangle of particles with a discrete fracture network included

width = 50.0
height = width
# Define region extremities:
minPoint = Vector3(0.0,0.0,0.0)
maxPoint = Vector3(width,height,0.0)

# Define the geometrical constraints for packing
# 	(e.g. lines bordering a rectangular region in 2D)
# QUESTION: Is there a particular order for defining endpoints of lines?
top_line = Line2D (
   startPoint = Vector3(width,0.0,0.0),
   endPoint = minPoint
)

bottom_line = Line2D (
#   startPoint = maxPoint,
   startPoint = Vector3(width,0.0,0.0),
   endPoint = Vector3(width/2.0,height,0.0)
)

left_line = Line2D (
   startPoint = Vector3(width/2.0,height,0.0),
   endPoint = minPoint
)

right_line = Line2D (
   startPoint = minPoint,
   endPoint = Vector3(0.0,height,0.0)
)

# Define the Volume to be filled with spheres:
#	(e.g. a BoxWithLines2D)
box = PolygonWithLines2D (
   centre = Vector3(25.0,25.0,0.0),
   radius = 25.0,
   nsides = 5,
   smooth_edges = True
)

#box.addLine(top_line)
#box.addLine(bottom_line)
#box.addLine(left_line)
#box.addLine(right_line)


# Create a multi-group neighbour table to contain the particles:
mntable = MNTable2D (
   minPoint = minPoint,
   maxPoint = maxPoint,
   gridSize = 2.5
)

# Fill the volume with particles:
packer = InsertGenerator2D (
   minRadius = 0.1,
   maxRadius = 1.0,
   insertFails = 1000,
   maxIterations = 1000,
   tolerance = 1.0e-6
)

packer.generatePacking( volume = box, ntable = mntable, tag = 0)

# create bonds between neighbouring particles:
mntable.generateBonds(
   tolerance = 1.0e-5,
   bondID = 0
)

#Add a discrete fracture network
n_faults = 10
breakLines = []
for f in range(n_faults):

   x0 = random()*0.8*width + 0.1*width
   y0 = random()*0.8*height + 0.1*height
   x1 = random()*0.8*width + 0.1*width
   y1 = random()*0.8*height + 0.1*height

   brkLine = LineSegment2D (
      startPoint = Vector3(x0,y0,0.0),
      endPoint = Vector3(x1,y1,0.0)
   )
   breakLines.append(brkLine)

# write a geometry file
mntable.write(
   fileName = "temp/geo_polygon.vtu",
   outputStyle = 2
)
