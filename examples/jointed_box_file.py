#############################################################
##                                                         ##
## Copyright (c) 2007-2017 by The University of Queensland ##
## Centre for Geoscience Computing                         ##
## http://earth.uq.edu.au/centre-geoscience-computing      ##
##                                                         ##
## Primary Business: Brisbane, Queensland, Australia       ##
## Licensed under the Open Software License version 3.0    ##
## http://www.apache.org/licenses/LICENSE-2.0              ##
##                                                         ##
#############################################################

from __future__ import print_function
from gengeo     import *
from read_jointset import *
import sys

# An example python script to generate a bonded rectangular prism,
# import joint sets and tag bonds crossing joints
# takes file name of joint set as an argument

# particle properties
minRadius = 0.2
maxRadius = 1.0

#set up joint set
joints=TriPatchSet()

# read joints from file
readJointFileTagSet(joints,sys.argv[1])

# output bounding box of joint set
print("Bounding Box of Joint Set: " , joints.getMinPoint(), joints.getMaxPoint())

# Define region extremities using the bounding box of the joint set
minPoint = joints.getMinPoint()-Vector3(1.0,1.0,1.0)
maxPoint = joints.getMaxPoint()-Vector3(1.0,1.0,1.0)


# Define the volume to be filled with spheres:

box = BoxWithPlanes3D (
   minPoint = minPoint,
   maxPoint = maxPoint
)

# boundary planes
box.addPlane(Plane(minPoint,Vector3(1.0,0.0,0.0)))
box.addPlane(Plane(minPoint, Vector3(0.0,1.0,0.0)))
box.addPlane(Plane(minPoint, Vector3(0.0,0.0,1.0)))
box.addPlane(Plane(maxPoint, Vector3(-1.0,0.0,0.0)))
box.addPlane(Plane(maxPoint, Vector3(0.0,-1.0,0.0)))
box.addPlane(Plane(maxPoint, Vector3(0.0,0.0,-1.0)))

# Create a multi-group neighbour table to contain the particles:
mntable = MNTable3D (
   minPoint = minPoint,
   maxPoint = maxPoint,
   gridSize = 2.5*maxRadius
)


# Fill the volume with particles:
packer = InsertGenerator3D (
   minRadius = minRadius,
   maxRadius = maxRadius,
   insertFails = 1000,
   maxIterations = 1000,
   tolerance = 1.0e-6,
   seed=42
)

packer.generatePacking(
   volume = box, 
   ntable = mntable,
   tag = 2
)


#bonding
tol=1.0e-5
basetag=1 # tag for the "normal" bonds
mntable.generateBondsWithJointSet(joints,0,tol,basetag)

# write a geometry and a VTK file
mntable.write("temp/box_"+str(maxRadius)+".geo",1)
mntable.write("temp/box_"+str(maxRadius)+".vtu",2)
