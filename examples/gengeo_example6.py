#############################################################
##                                                         ##
## Copyright (c) 2007-2017 by The University of Queensland ##
## Centre for Geoscience Computing                         ##
## http://earth.uq.edu.au/centre-geoscience-computing      ##
##                                                         ##
## Primary Business: Brisbane, Queensland, Australia       ##
## Licensed under the Open Software License version 3.0    ##
## http://www.apache.org/licenses/LICENSE-2.0              ##
##                                                         ##
#############################################################

from __future__ import division
from gengeo     import *
#An example python script to generate a triangular prism of bonded particles

size=5.0
# Define region extremities:
minPoint = Vector3(-1.0*size,-1.0*size,-1.0*size)
maxPoint = Vector3(size,size,size)

# Define the volume to be filled with spheres:
tribox = TriBox (
   minPoint = minPoint,
   maxPoint = maxPoint,
   inverted = False
)
tribox.addPlane(Plane(minPoint,Vector3(0.0,1.0,0.0)))
tribox.addPlane(Plane(minPoint,Vector3(0.0,0.0,1.0)))
tribox.addPlane(Plane(maxPoint,Vector3(0.0,0.0,-1.0)))

delta = maxPoint-minPoint
dx = delta.X()
dy = delta.Y()
tribox.addPlane(Plane(minPoint, Vector3(dy,-0.5*dx,0.0).unit()))
tribox.addPlane(Plane(maxPoint-Vector3(dx/2.0,0.0,0.0), Vector3(-1.0*dy,-0.5*dx,0.0).unit()))

# Create a multi-group neighbour table to contain the particles:
mntable = MNTable3D (
   minPoint = minPoint,
   maxPoint = maxPoint,
   gridSize = 2.5
)

# Fill the volume with particles:
packer = InsertGenerator3D (
   minRadius = 0.2,
   maxRadius = 1.0,
   insertFails = 1000,
   maxIterations = 1000,
   tolerance = 1.0e-6
)

# Generate the packing
packer.generatePacking(
   volume = tribox, 
   ntable = mntable
)

# generate bonds between neighbouring particles
mntable.generateBonds(
   tolerance = 1.0e-5,
   bondID = 0
)

# write a geometry file in VTK format
mntable.write(
   fileName = "temp/geo_example6.vtu",
   outputStyle = 2
)

# write a geometry file in ESyS-Particle geo format
mntable.write(
   fileName = "temp/geo_example6.geo",
   outputStyle = 1
)
