#############################################################
##                                                         ##
## Copyright (c) 2007-2017 by The University of Queensland ##
## Centre for Geoscience Computing                         ##
## http://earth.uq.edu.au/centre-geoscience-computing      ##
##                                                         ##
## Primary Business: Brisbane, Queensland, Australia       ##
## Licensed under the Open Software License version 3.0    ##
## http://www.apache.org/licenses/LICENSE-2.0              ##
##                                                         ##
#############################################################

from __future__ import division, print_function
from gengeo     import *
#An example python script to generate an aggregate of hexagonal grains
# with circular boundary conditions and tagging of particles along lines

# Define region extremities:
size = 5.0
minPoint = Vector3(0.0,0.0,0.0)
maxPoint = Vector3(size,size,0.0)

# Define the geometrical constraints for packing
top_line = Line2D (
   startPoint = Vector3(size,0.0,0.0),
   endPoint = minPoint
)

bottom_line = Line2D (
   startPoint = maxPoint,
   endPoint = Vector3(0.0,size,0.0)
)

box = BoxWithLines2DSubVol (
   minPoint = minPoint-Vector3(-0.5,0.0,0.0),
   maxPoint = maxPoint+Vector3(0.5,0.0,0.0),
   svdim_x = 2.5,
   svdim_y = 2.5
)
box.addLine(top_line)
box.addLine(bottom_line)

# Create a multi-group neighbour table to contain the particles:
mntable = CircMNTable2D (
   minPoint = minPoint,
   maxPoint = maxPoint,
   gridSize = 2.5
)

# Fill the volume with particles:
packer = HexAggregateInsertGenerator2D (
   minRadius = 0.1,
   maxRadius = 1.0,
   insertFails = 1000,
   maxIterations = 1000,
   tolerance = 1.0e-6
)

# Generate the packing
packer.generatePacking(
   volume = box, 
   ntable = mntable,
   tag = 0
)

# tag particles along the top_line and bottom_line
mntable.tagParticlesAlongLineWithMask(
   line = top_line,
   distance = 0.5,
   tag = 4,
   mask = 4
)

#and the compact form:
mntable.tagParticlesAlongLineWithMask(bottom_line, 0.5, 8, 8, 0)

#print the porosity of the particle packing:
volume = size*size
porosity = (volume - mntable.getSumVolume())/volume
print("Porosity:  ",porosity)

# write a geometry file in VTK format
mntable.write(
   fileName = "temp/geo_example7.vtu",
   outputStyle = 2
)

# write a geometry file in ESyS-Particle geo format
mntable.write(
   fileName = "temp/geo_example7.geo",
   outputStyle = 1
)
