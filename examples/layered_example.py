#############################################################
##                                                         ##
## Copyright (c) 2007-2017 by The University of Queensland ##
## Centre for Geoscience Computing                         ##
## http://earth.uq.edu.au/centre-geoscience-computing      ##
##                                                         ##
## Primary Business: Brisbane, Queensland, Australia       ##
## Licensed under the Open Software License version 3.0    ##
## http://www.apache.org/licenses/LICENSE-2.0              ##
##                                                         ##
#############################################################

from __future__ import division
from gengeo     import *

size = 50.0
ntry = 1000
rmin_sand = 0.5
rmin_clay = 0.2
rmax_clay = 0.5

sand_layer_thickness = 0.425*size
clay_layer_thickness = 0.15*size
center_box_dim = 0.05*size

mntable = CircMNTable2D (Vector3(0,0,0), Vector3(size,size,0),2.5,1)

sand_packer = HexAggregateInsertGenerator2D (rmin_sand, 1.0, ntry, 1000, 1.0e-6)

clay_packer = InsertGenerator2D(rmin_clay, rmax_clay, ntry, 1000, 1.0e-6)

clay_layer_min=sand_layer_thickness
clay_layer_max=clay_layer_min+clay_layer_thickness
clay_box_min = clay_layer_min - rmax_clay
clay_box_max = clay_layer_max + rmax_clay

ClayBox = BoxWithLines2D(Vector3(clay_layer_min,0.0,0.0),Vector3(clay_layer_max,size,0.0))

ClayBox.addLine(Line2D(Vector3(clay_layer_min,0.0,0.0),Vector3(clay_layer_max,0.0,0.0)))
ClayBox.addLine(Line2D(Vector3(clay_layer_max,size,0.0),Vector3(clay_layer_min,size,0.0)))
ClayBox.addLine(Line2D(Vector3(clay_layer_max,size,0.0),Vector3(clay_layer_max,0.0,0.0)))
ClayBox.addLine(Line2D(Vector3(clay_layer_min,size,0.0),Vector3(clay_layer_max,size,0.0)))

clay_packer.generatePacking(ClayBox,mntable,0,2)

cbox_layer_minx=sand_layer_thickness-center_box_dim
cbox_layer_maxx=sand_layer_thickness+clay_layer_thickness+center_box_dim
cbox_layer_miny=0.5*size-center_box_dim
cbox_layer_maxy=0.5*size+center_box_dim

CenterBox = BoxWithLines2D(Vector3(cbox_layer_minx,cbox_layer_miny,0.0), Vector3(cbox_layer_maxx,cbox_layer_maxy,0.0))

CenterBox.addLine(Line2D(Vector3(cbox_layer_minx,cbox_layer_miny,0.0), Vector3(cbox_layer_maxx,cbox_layer_miny,0.0)))

CenterBox.addLine(Line2D(Vector3(cbox_layer_maxx,cbox_layer_maxy,0.0), Vector3(cbox_layer_minx,cbox_layer_maxy,0.0)))

CenterBox.addLine(Line2D(Vector3(cbox_layer_maxx,cbox_layer_maxy,0.0), Vector3(cbox_layer_maxx,cbox_layer_miny,0.0)))

CenterBox.addLine(Line2D(Vector3(cbox_layer_minx,cbox_layer_maxy,0.0), Vector3(cbox_layer_maxx,cbox_layer_maxy,0.0)))

clay_packer.generatePacking(CenterBox,mntable,0,2)

sand_layer_min1=0
sand_layer_max1=sand_layer_thickness
sand_box_min1=sand_layer_min1-rmin_sand
sand_box_max1=sand_layer_max1+rmin_sand

SandBox1 = BoxWithLines2D(Vector3(sand_box_min1,0.0,0.0),Vector3(sand_box_max1,size,0.0))

SandBox1.addLine(Line2D(Vector3(sand_layer_min1,size,0.0),Vector3(sand_layer_min1,0.0,0.0)))

SandBox1.addLine(Line2D(Vector3(sand_layer_min1,0.0,0.0),Vector3(sand_layer_max1,0.0,0.0)))

SandBox1.addLine(Line2D(Vector3(sand_layer_max1,size,0.0),Vector3(sand_layer_max1,0.0,0.0)))

SandBox1.addLine(Line2D(Vector3(sand_layer_min1,size,0.0),Vector3(sand_layer_max1,size,0.0)))

SandBox1.addLine(Line2D(Vector3(0.0,size/2.0,0.0),Vector3(size,size/2.0,0.0)))

sand_packer.generatePacking(SandBox1,mntable,0,1)

sand_layer_min2=sand_layer_thickness+clay_layer_thickness
sand_layer_max2=size
sand_box_min2=sand_layer_min2-rmin_sand
sand_box_max2=sand_layer_max2+rmin_sand

SandBox2 = BoxWithLines2D(Vector3(sand_box_min2,0.0,0.0),Vector3(sand_box_max2,size,0.0))

SandBox2.addLine(Line2D(Vector3(sand_layer_min2,size,0.0),Vector3(sand_layer_min2,0.0,0.0)))
SandBox2.addLine(Line2D(Vector3(sand_layer_min2,0.0,0.0),Vector3(sand_layer_max2,0.0,0.0)))
SandBox2.addLine(Line2D(Vector3(sand_layer_max2,size,0.0),Vector3(sand_layer_max2,0.0,0.0)))
SandBox2.addLine(Line2D(Vector3(sand_layer_min2,size,0.0),Vector3(sand_layer_max2,size,0.0)))

SandBox2.addLine(Line2D(Vector3(0.0,size/2.0,0.0),Vector3(size,size/2.0,0.0)))

sand_packer.generatePacking(SandBox2,mntable,0,1)

mntable.generateBondsWithMask(0,1.0e-6,0,1,1)

top_line = Line2D(Vector3(0.0,size,0.0),Vector3(size,size,0.0))
bottom_line = Line2D(Vector3(0.0,0.0,0.0),Vector3(size,0.0,0.0))

mntable.tagParticlesAlongLineWithMask(top_line,0.5,4,4,0)
mntable.tagParticlesAlongLineWithMask(bottom_line,0.5,8,8,0)

mntable.write("temp/geo_layered.vtu",2)
