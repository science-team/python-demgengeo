#############################################################
##                                                         ##
## Copyright (c) 2007-2017 by The University of Queensland ##
## Centre for Geoscience Computing                         ##
## http://earth.uq.edu.au/centre-geoscience-computing      ##
##                                                         ##
## Primary Business: Brisbane, Queensland, Australia       ##
## Licensed under the Open Software License version 3.0    ##
## http://www.apache.org/licenses/LICENSE-2.0              ##
##                                                         ##
#############################################################

from gengeo import *
#An example python script to generate a union/intersection of two volumes 

# Define region extremities:
maxRadius = 1.0
size = 20.0
minPoint = Vector3(0.0,0.0,0.0)
maxPoint = Vector3(size,size,size)

minPt = Vector3(0,0,0)
maxPt = Vector3(size,0.5*size,size)
# Define the first volume to be filled with spheres:
box = BoxWithPlanes3D (
   minPoint = minPt,
   maxPoint = maxPt
)

box.addPlane(
   Plane(
      origin = minPt, 
      normal = Vector3(1.0,0.0,0.0)
   )
)
#or the compact form:
box.addPlane(Plane(minPt, Vector3(0.0,1.0,0.0)))
box.addPlane(Plane(minPt, Vector3(0.0,0.0,1.0)))
box.addPlane(Plane(maxPt, Vector3(-1.0,0.0,0.0)))
box.addPlane(Plane(maxPt, Vector3(0.0,-1.0,0.0)))
box.addPlane(Plane(maxPt, Vector3(0.0,0.0,-1.0)))

sphere = SphereVol (
   centre = Vector3(0.5*size,0.5*size,0.5*size),
   radius = 0.5*size
)

sphere1 = SphereVol (
   centre = Vector3(0.5*size,0.75*size,0.5*size),
   radius = 0.5*size
)

sphere2 = SphereVol (
   centre = Vector3(0.5*size,0.25*size,0.5*size),
   radius = 0.5*size
)

cylinder = CylinderVol (
   origin = Vector3 (0.5*size,0,0.5*size),
   axis = Vector3 (0,1,0),
   length = 0.5*size,
   radius = 0.5*size
)

cylinder2 = CylinderVol (
   origin = Vector3 (0.5*size,0,0.5*size),
   axis = Vector3 (0,1,0),
   length = 0.5*size,
   radius = 0.4*size
)

cylinder3 = CylinderVol (
   origin = Vector3 (0.5*size,0.25*size,0.5*size),
   axis = Vector3 (0,1,0),
   length = 0.5*size,
   radius = 0.3*size
)

unionVol = UnionVol (
   volume1 = sphere,
   volume2 = cylinder
)

intersectionVol = IntersectionVol (
   volume1 = sphere1,
   volume2 = sphere2
)

differenceVol = DifferenceVol (
   volume1 = cylinder,
   volume2 = cylinder2
)

differenceVol2 = DifferenceVol (
   volume1 = cylinder,
   volume2 = sphere1
)

differenceVol3 = DifferenceVol (
   volume1 = cylinder,
   volume2 = cylinder3
)

# Create a multi-group neighbour table to contain the particles:
mntable = MNTable3D (
   minPoint = minPoint,
   maxPoint = maxPoint,
   gridSize = 2.5*maxRadius
)

# Fill the volume with particles:
packer = InsertGenerator3D (
   minRadius = 0.2,
   maxRadius = maxRadius,
   insertFails = 1000,
   maxIterations = 1000,
   tolerance = 1.0e-6
)

packer.generatePacking(
#   volume = unionVol, 
#   volume = intersectionVol, 
#   volume = differenceVol, 
   volume = differenceVol2, 
#   volume = differenceVol3, 
#   volume = cylinder, 
   ntable = mntable
)

# create bonds between neighbouring particles:
mntable.generateBonds(
   tolerance = 1.0e-5,
   bondID = 0
)

mntable.tagParticlesInVolume (
   volume = differenceVol,
   tag = 12345
)

# write a geometry file
mntable.write(
   fileName = "temp/geo_example10.vtu",
   outputStyle = 2	
)

mntable.write(
   fileName = "temp/geo_example10.raw",
   outputStyle = 0
)

mntable.write(
   fileName = "temp/geo_example10.geo",
   outputStyle = 1	
)
