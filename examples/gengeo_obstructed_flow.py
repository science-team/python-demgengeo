#############################################################
##                                                         ##
## Copyright (c) 2007-2017 by The University of Queensland ##
## Centre for Geoscience Computing                         ##
## http://earth.uq.edu.au/centre-geoscience-computing      ##
##                                                         ##
## Primary Business: Brisbane, Queensland, Australia       ##
## Licensed under the Open Software License version 3.0    ##
## http://www.apache.org/licenses/LICENSE-2.0              ##
##                                                         ##
#############################################################

from gengeo import *

# Define region extremities:
minPoint = Vector3(0.0,25.0,0.0)
maxPoint = Vector3(30.0,40.0,0.0)

top_line = Line2D (
   startPoint = Vector3(30.0,25.0,0.0),
   endPoint = minPoint
)

bottom_line = Line2D (
   startPoint = maxPoint,
   endPoint = Vector3(0.0,40.0,0.0)
)

left_line = Line2D (
   startPoint = Vector3(30.0,25.0,0.0),
   endPoint = maxPoint
)

right_line = Line2D (
   startPoint = minPoint,
   endPoint = Vector3(0.0,40.0,0.0)
)

box = BoxWithLines2D (
   minPoint = minPoint,
   maxPoint = maxPoint
)

box.addLine(top_line)
box.addLine(bottom_line)
box.addLine(left_line)
box.addLine(right_line)

mntable = MNTable2D (
   minPoint = Vector3(0.0,0.0,0.0),
   maxPoint = Vector3(30.0,40.0,0.0),
   gridSize = 1.25
)

packer = InsertGenerator2D (
   minRadius = 0.4,
   maxRadius = 0.6,
   insertFails = 5000,
   maxIterations = 10000,
   tolerance = 1.0e-6
)

packer.generatePacking( volume = box, ntable = mntable, tag = 0)

#tri = PolygonWithLines2D (
#   centre = Vector3(15.0,15.0,0.0),
#   radius = 10.0,
#   nsides = 3,
#   smooth_edges = True
#)

left_tri = TriWithLines2D (
   vertex0 = Vector3(0.0,0.0,0.0),
   vertex1 = Vector3(10.0,0.0,0.0),
   vertex2 = Vector3(0.0,17.0,0.0)
)

right_tri = TriWithLines2D (
   vertex0 = Vector3(20.0,0.0,0.0),
   vertex1 = Vector3(30.0,0.0,0.0),
   vertex2 = Vector3(30.0,17.0,0.0)
)

obstacle = BoxWithLines2D (
   minPoint = Vector3(10.0,15.0,0.0),
   maxPoint = Vector3(20.0,23.0,0.0)
)

t_line = Line2D (
   startPoint = Vector3(10.0,23.0,0.0),
   endPoint = Vector3(20.0,23.0,0.0)
)
obstacle.addLine(t_line)

b_line = Line2D (
   startPoint = Vector3(10.0,15.0,0.0),
   endPoint = Vector3(20.0,15.0,0.0)
)
obstacle.addLine(b_line)

l_line = Line2D (
   startPoint = Vector3(10.0,15.0,0.0),
   endPoint = Vector3(10.0,23.0,0.0)
)
obstacle.addLine(l_line)

r_line = Line2D (
   startPoint = Vector3(20.0,15.0,0.0),
   endPoint = Vector3(20.0,23.0,0.0)
)
obstacle.addLine(r_line)

packer2 = InsertGenerator2D (
   minRadius = 0.1,
   maxRadius = 1.0,
   insertFails = 5000,
   maxIterations = 10000,
   tolerance = 1.0e-6
)

packer2.generatePacking( volume = left_tri, ntable = mntable, tag = 999)
packer2.generatePacking( volume = right_tri, ntable = mntable, tag = 999)
packer2.generatePacking( volume = obstacle, ntable = mntable, tag = 999)

# write a geometry file
mntable.write(
   fileName = "temp/obstructed_flow.vtu",
   outputStyle = 2
)

mntable.write(
   fileName = "temp/obstructed_flow.geo",
   outputStyle = 1
)

mntable.write(
   fileName = "temp/obstructed_flow.raw",
   outputStyle = 0
)
