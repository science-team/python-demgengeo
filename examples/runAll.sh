#!/bin/bash
mkdir temp
python gengeo_example1.py >& temp/ex1_output.txt
python gengeo_example2.py >& temp/ex2_output.txt
python gengeo_example3.py >& temp/ex3_output.txt
python gengeo_example4.py >& temp/ex4_output.txt
python gengeo_example5.py >& temp/ex5_output.txt
python gengeo_example6.py >& temp/ex6_output.txt
python gengeo_example7.py >& temp/ex7_output.txt
python gengeo_example8.py >& temp/ex8_output.txt
python gengeo_example9.py >& temp/ex9_output.txt
python gengeo_example10.py >& temp/ex10_output.txt
python gengeo_example11.py >& temp/ex11_output.txt
python gengeo_exampleShapeList.py >& temp/ex12_output.txt
python gengeo_obstructed_flow.py >& temp/ex13_output.txt
python gengeo_polygon.py >& temp/ex14_output.txt
python gengeo_crack.py >& temp/ex15_output.txt
python gengeo_DFN.py >& temp/ex16_output.txt
python gengeo_test_cluster.py >& temp/ex17_output.txt
python gouge_example.py >& temp/ex18_output.txt
python hlayer_example.py >& temp/ex19_output.txt
python layered_example.py >& temp/ex20_output.txt
