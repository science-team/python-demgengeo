#############################################################
##                                                         ##
## Copyright (c) 2007-2017 by The University of Queensland ##
## Centre for Geoscience Computing                         ##
## http://earth.uq.edu.au/centre-geoscience-computing      ##
##                                                         ##
## Primary Business: Brisbane, Queensland, Australia       ##
## Licensed under the Open Software License version 3.0    ##
## http://www.apache.org/licenses/LICENSE-2.0              ##
##                                                         ##
#############################################################

from gengeo import *
#An example python script to generate a dogbone of unbonded particles

# Define region extremities:
l2 = 10.0
r2 = 2.5
origin = Vector3(0.0,0.0,0.0)
axis = Vector3(0.0,1.0,0.0)
size = 5.0
minPoint = Vector3(-1.0*size,0.0,-1.0*size)
maxPoint = Vector3(size,3.0*size,size)

# Define the volume to be filled with spheres:
dogbone = DogBone (
   origin = origin,
   axis = axis,
   length = 3.0*size,
   radius = size,
   l2 = l2,		
   r2 = r2
)

# Create a multi-group neighbour table to contain the particles:
mntable = MNTable3D (
   minPoint = minPoint,
   maxPoint = maxPoint,
   gridSize = 2.2
)

# Fill the volume with particles:
packer = InsertGenerator3D (
   minRadius = 0.2,
   maxRadius = 1.0,
   insertFails = 1000,
   maxIterations = 1000,
   tolerance = 1.0e-6
)

# Generate the packing
packer.generatePacking(
   volume = dogbone, 
   ntable = mntable
)

# write a geometry file in VTK format
mntable.write(
   fileName = "temp/geo_example4.vtu",
   outputStyle = 2		
)

# write a geometry file in ESyS-Particle geo format
mntable.write(
   fileName = "temp/geo_example4.geo",
   outputStyle = 1		
)
