#############################################################
##                                                         ##
## Copyright (c) 2007-2017 by The University of Queensland ##
## Centre for Geoscience Computing                         ##
## http://earth.uq.edu.au/centre-geoscience-computing      ##
##                                                         ##
## Primary Business: Brisbane, Queensland, Australia       ##
## Licensed under the Open Software License version 3.0    ##
## http://www.apache.org/licenses/LICENSE-2.0              ##
##                                                         ##
#############################################################

from gengeo import *
from math import sqrt
#An example python script to generate a regular aggregate of hexagonal grains
# with bonds broken along lines 

# Define region extremities:
size = 150.0
xsize = size
ysize = size
minPoint = Vector3(0.0,0.0,0.0)
maxPoint = Vector3(xsize,ysize,0.0)

# Define the geometrical constraints for packing
top_line = Line2D (
   startPoint = minPoint,
   endPoint = Vector3(size,0.0,0.0)
)

bottom_line = Line2D (
   startPoint = Vector3(0.0,40.,0.0),
   endPoint = Vector3(40.,40.,0.0)
)

#or compact form:
left_line = Line2D(Vector3(40.,40.,0.0),Vector3(40.,0.0,0.0))
right_line = Line2D(minPoint, Vector3(0.0,ysize,0.0))

box = BoxWithLines2D (
   minPoint = minPoint,
   maxPoint = maxPoint
)
box.addLine(top_line)
box.addLine(bottom_line)
box.addLine(left_line)
box.addLine(right_line)

# Create a multi-group neighbour table to contain the particles:
mntable = MNTable2D (
   minPoint = minPoint,
   maxPoint = maxPoint,
   gridSize = 1.25
)

# Fill the volume with particles:
packer = HGrainGenerator2D (
   radius = 0.5
)

# Generate the packing
packer.generatePacking(
   volume = box, 
   ntable = mntable,
   tag = 0
)

# bond the particles
#mntable.generateBonds(0,1.0e-5,3)

# tag particles along the boundary lines (e.g. top_line, left_line etc.)
#	using the compact form:
mntable.tagParticlesAlongLineWithMask(bottom_line, 5.0, 4, -1, 0)
mntable.tagParticlesAlongLineWithMask(top_line, 5.0, 8, -1, 0)
mntable.tagParticlesAlongLineWithMask(left_line, 8.0, 16, -1, 0)
mntable.tagParticlesAlongLineWithMask(right_line, 8.0, 32,-1, 0)

# write a geometry file in VTK format
mntable.write(
   fileName = "temp/geo_example8.vtu",
   outputStyle = 2
)

# write a geometry file in ESyS-Particle geo format
mntable.write(
   fileName = "temp/geo_example8.geo",
   outputStyle = 1
)
