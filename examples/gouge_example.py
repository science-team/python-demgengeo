#############################################################
##                                                         ##
## Copyright (c) 2007-2017 by The University of Queensland ##
## Centre for Geoscience Computing                         ##
## http://earth.uq.edu.au/centre-geoscience-computing      ##
##                                                         ##
## Primary Business: Brisbane, Queensland, Australia       ##
## Licensed under the Open Software License version 3.0    ##
## http://www.apache.org/licenses/LICENSE-2.0              ##
##                                                         ##
#############################################################

from gengeo import *
from granular_gouge import *

xsize = 20.0
ysize = 20.0
ysize_bdry = 4.0
zsize = 20.0
numRidges = 5
heightRidges = 2.5
numInsertFails = 1000
minRadius = 0.2
minGrainRadius = 2.0
maxGrainRadius = 4.0

minPoint = Vector3(0,0,0)
maxPoint = Vector3(xsize,ysize,zsize)
minGPoint = Vector3(0,ysize_bdry,0)
maxGPoint = Vector3(xsize,ysize-ysize_bdry,zsize)

mntable = CircMNTable3D (
   minPoint = minPoint,
   maxPoint = maxPoint,
   gridSize = 2.5,
   numGroups = 3
)

packer = InsertGenerator3D (
   minRadius = minRadius,
   maxRadius = 1.0,
   insertFails = numInsertFails,
   maxIterations = 1000,
   tolerance = 1.0e-6
)

generate_upper_tri_rough_block(mntable, packer, xsize, ysize_bdry, zsize, ysize-ysize_bdry, numRidges, heightRidges)

generate_lower_tri_rough_block(mntable, packer, xsize, ysize_bdry, zsize, 0.0, numRidges, heightRidges)

generate_granular_gouge(mntable, packer, minGPoint, maxGPoint, minGrainRadius, maxGrainRadius, numInsertFails)

mntable.generateBonds(
   tolerance = 1.0e-5,
   bondID = 0
)

mntable.write(
   fileName = "temp/geo_gouge.vtu",
   outputStyle = 2   
)
