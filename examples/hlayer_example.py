#############################################################
##                                                         ##
## Copyright (c) 2007-2017 by The University of Queensland ##
## Centre for Geoscience Computing                         ##
## http://earth.uq.edu.au/centre-geoscience-computing      ##
##                                                         ##
## Primary Business: Brisbane, Queensland, Australia       ##
## Licensed under the Open Software License version 3.0    ##
## http://www.apache.org/licenses/LICENSE-2.0              ##
##                                                         ##
#############################################################

from gengeo import *

xsize=50.0
ysize=50.0
ysize_cl=10.0
ntry=1000
rmin_nb=0.2
rmin_b=0.2
pbond=0.5

y_bdry_1=0.5*(ysize-ysize_cl)
y_bdry_2=ysize-y_bdry_1

T = MNTable2D(Vector3(0.0,0.0,0.0),Vector3(xsize,ysize,0.0),2.5,1)
GB = InsertGenerator2D(rmin_b,1.0,ntry,1000,1e-6)
GNB = InsertGenerator2D(rmin_nb,1.0,ntry,1000,1e-6)

CenterBox = BoxWithLines2D (Vector3(0.0,y_bdry_1,0.0),Vector3(xsize,y_bdry_2,0.0))

CenterBox.addLine(Line2D(Vector3(0.0,y_bdry_1,0.0),Vector3(0.0,y_bdry_2,0.0)))
CenterBox.addLine(Line2D(Vector3(xsize,y_bdry_1,0.0),Vector3(xsize,y_bdry_2,0.0)))
CenterBox.addLine(Line2D(Vector3(0.0,y_bdry_1,0.0),Vector3(xsize,y_bdry_1,0.0)))
CenterBox.addLine(Line2D(Vector3(0.0,y_bdry_2,0.0),Vector3(xsize,y_bdry_2,0.0)))

GB.generatePacking(CenterBox,T,0,1)
T.generateRandomBonds(0,1e-5,pbond,0,1,0)

BottomBox = BoxWithLines2D (Vector3(0.0,0.0,0.0),Vector3(xsize,y_bdry_1+0.5,0.0))

BottomBox.addLine(Line2D(Vector3(0.0,0.0,0.0),Vector3(0.0,y_bdry_1,0.0)))
BottomBox.addLine(Line2D(Vector3(xsize,0.0,0.0),Vector3(xsize,y_bdry_1,0.0)))
BottomBox.addLine(Line2D(Vector3(0.0,0.0,0.0),Vector3(xsize,0.0,0.0)))

GNB.generatePacking(BottomBox,T,0,2)

TopBox = BoxWithLines2D (Vector3(0.0,y_bdry_2-0.5,0.0),Vector3(xsize,ysize,0.0))

TopBox.addLine(Line2D(Vector3(0.0,y_bdry_2,0.0),Vector3(0.0,ysize,0.0)))
TopBox.addLine(Line2D(Vector3(xsize,y_bdry_2,0.0),Vector3(xsize,ysize,0.0)))
TopBox.addLine(Line2D(Vector3(0.0,ysize,0.0),Vector3(xsize,ysize,0.0)))

GNB.generatePacking(TopBox,T,0,3)

T.write("temp/geo_hlayer.vtu",2)
