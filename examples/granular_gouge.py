#############################################################
##                                                         ##
## Copyright (c) 2007-2017 by The University of Queensland ##
## Centre for Geoscience Computing                         ##
## http://earth.uq.edu.au/centre-geoscience-computing      ##
##                                                         ##
## Primary Business: Brisbane, Queensland, Australia       ##
## Licensed under the Open Software License version 3.0    ##
## http://www.apache.org/licenses/LICENSE-2.0              ##
##                                                         ##
#############################################################

from __future__ import division
from gengeo     import *

def generate_upper_tri_rough_block(NT, G, xt, yt, zt, y0, nr, yr):
   min = Vector3(0.0,y0,0.0)
   min_2 = Vector3(0.0,y0+yr,0.0)
   max = Vector3(xt,y0+yt,zt)
   xr = xt/(1.0*nr)

   top_plane = Plane (max, Vector3(0,-1,0))
   bottom_plane_1 = Plane(min, Vector3(0,1,0))
   bottom_plane_2 = Plane(min_2, Vector3(0,1,0))
   front_plane = Plane(min, Vector3(0,0,1))
   back_plane = Plane(max, Vector3(0,0,-1))

   diff = Vector3(1,0,0)

   Box = BoxWithPlanes3D (min_2-diff,max+diff)

   Box.addPlane(top_plane)
   Box.addPlane(bottom_plane_2)
   Box.addPlane(front_plane)
   Box.addPlane(back_plane)

   G.generatePacking(Box,NT,0)

   for i in range(0,nr):
      x1 = (1.0*i)*xr
      x2 = (1.0*i+1.0)*xr

      lmin = Vector3(x1,y0,0.0)
      lmax = Vector3(x2,y0+yr,zt)

      left_plane = Plane (lmin+Vector3(xr/2.0,0.0,0.0), (Vector3(yr,-0.5*xr,0.0)).unit())
      right_plane = Plane (lmax, Vector3(-1.0*yr,-0.5*xr,0.0).unit())

      RBox = TriBox (lmin,lmax,True)
      RBox.addPlane(front_plane)
      RBox.addPlane(back_plane)
      RBox.addPlane(left_plane)
      RBox.addPlane(right_plane)

      G.generatePacking(RBox, NT, 0)

#   NT.tagParticlesAlongPlane(top_plane,0.5,3,0)
   NT.tagParticlesAlongPlane(top_plane,yt,3,0)

def generate_lower_tri_rough_block(NT, G, xt, yt, zt, y0, nr, yr):
   min = Vector3(0.0,y0,0.0)
   max_2 = Vector3(xt,y0+(yt-yr),zt)
   max = Vector3(xt,y0+yt,zt)
   xr = xt/(nr)

   top_plane = Plane (max, Vector3(0,-1,0))
   top_plane_2 = Plane(max_2, Vector3(0,-1,0))
   bottom_plane = Plane(min, Vector3(0,1,0))
   front_plane = Plane(min, Vector3(0,0,1))
   back_plane = Plane(max, Vector3(0,0,-1))

   diff = Vector3(1,0,0)

   Box = BoxWithPlanes3D (min-diff,max_2+diff)

   Box.addPlane(top_plane_2)
   Box.addPlane(bottom_plane)
   Box.addPlane(front_plane)
   Box.addPlane(back_plane)

   G.generatePacking(Box,NT,0)

   for i in range(0,nr):
      x1 = (1.0*i)*xr
      x2 = (1.0*i+1.0)*xr

      lmin = Vector3(x1,y0+(yt-yr),0.0)
      lmax = Vector3(x2,y0+yt,zt)

      left_plane = Plane (lmin, Vector3(yr,-0.5*xr,0).unit())
      right_plane = Plane (lmax-Vector3(xr/2.0,0.0,0.0), Vector3(-1.0*yr,-0.5*xr,0.0).unit())

      RBox = TriBox (lmin,lmax,False)
      RBox.addPlane(front_plane)
      RBox.addPlane(back_plane)
      RBox.addPlane(left_plane)
      RBox.addPlane(right_plane)

      G.generatePacking(RBox, NT, 0)

#   NT.tagParticlesAlongPlane(bottom_plane,0.5,4,0)
   NT.tagParticlesAlongPlane(bottom_plane,yt,4,0)

def generate_granular_gouge(NT, G, Pmin, Pmax, grmin, grmax, gntry):
   T_aux = MNTable3D (Pmin, Pmax, 2.1*grmax, 1)
   G_aux = InsertGenerator3D (grmin, grmax, gntry, 1000, 1.0e-5)
   V_aux = BoxWithPlanes3D (Pmin, Pmax)

   G_aux.generatePacking(V_aux, T_aux, 0)

   sphere_list = T_aux.getSphereListFromGroup(0)

   ngr = len(sphere_list)
   NT.GrowNGroups(1+ngr)
   ggid = 1
   for ss in sphere_list:
      S = SphereVol(ss.Centre(), ss.Radius())
      G.generatePacking(S,NT,ggid)
      NT.generateBonds(ggid,1.0e-5,1)
      ggid += 1
