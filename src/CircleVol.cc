////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "CircleVol.h"

//--- 
#include <cmath>
#include <cstdlib>

using std::cos;
using std::sin; 

// --- STL includes ---
#include <utility>

using std::make_pair;

CircleVol::CircleVol()
{}

CircleVol::CircleVol(const Vector3& c,double r)
{
  m_sph=SphereIn(c,r);
}

pair<Vector3,Vector3> CircleVol::getBoundingBox()
{
  Vector3 r=Vector3(m_sph.Radius(),m_sph.Radius(),0.0);
  return make_pair(m_sph.Center()-r,m_sph.Center()+r);
}

Vector3 CircleVol::getAPoint(int) const
{
  double r=m_sph.Radius()*((double)(rand())/(double)(RAND_MAX));
  double rho=2.0*M_PI*((double)(rand())/(double)(RAND_MAX));

  return m_sph.Center()+r*Vector3(cos(rho),sin(rho),0.0);
}

Line2D CircleVol::getClosestPlane(const Vector3& V)
{
  Line2D res;

  return res;
}

const map<double, const Line2D*> CircleVol::getClosestPlanes(const Vector3& p,int nmax) const
{
  map<double,const Line2D*> res;

  return res;
}

const map<double,const AGeometricObject*> CircleVol::getClosestObjects(const Vector3& P,int) const
{
  map<double,const AGeometricObject*> res;

  res.insert(make_pair(m_sph.getDist(P),&m_sph));

  return res;  
}

bool CircleVol::isIn(const Vector3& P) const
{
  double dist=(m_sph.Center()-P).norm();
  return (dist<m_sph.Radius()); 
}

bool CircleVol::isIn(const Sphere& S)
{
  double dist=(m_sph.Center()-S.Center()).norm();
  return (dist+S.Radius()<m_sph.Radius()); 
}

ostream& operator << (ostream& ost,const CircleVol& T)
{
   return ost;
}
