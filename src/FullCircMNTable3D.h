/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __FULLCIRCMNTABLE3D_H
#define __FULLCIRCMNTABLE3D_H

// --- Project includes ---
#include "CircMNTable3D.h"

/*!
  \class FullCircMNTable3D
  \brief circular 3D Multi-group Neighbour table

  Neighbour table supporting multiple tagged groups of particles and circular boundary
  conditions in x, y and z 
*/
class FullCircMNTable3D : public CircMNTable3D
{
 protected:
  Vector3 m_shift_y,m_shift_z;

  virtual void set_y_circ();
  virtual void set_z_circ();
  virtual int getIndex(const Vector3&) const;

 public:
  FullCircMNTable3D();
  FullCircMNTable3D(const Vector3&,const Vector3&,double,unsigned int);
  virtual ~FullCircMNTable3D();
  
  virtual bool insert(const Sphere&,unsigned int);
  virtual bool insertChecked(const Sphere&,unsigned int,double tol=s_small_value);
  virtual bool checkInsertable(const Sphere&,unsigned int);
  virtual void generateBonds(int,double,int);  

  // output 
  //  friend ostream& operator << (ostream&,const FullCircMNTable3D&);
};

#endif // __FULLCIRCMNTABLE3D_H
