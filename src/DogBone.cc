/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "DogBone.h"

// --- STL includes ---
#include <utility>

using std::make_pair;

DogBone::DogBone()
{}

/*!
  \param c base point
  \param axis central axis
  \param l total length
  \param r total radius
  \param l2 length of cylindrical end pieces
  \param r2 center radius
*/
DogBone::DogBone(const Vector3& c,const Vector3& axis,double l,double r,double l2,double r2) : CylinderVol(c,axis,l,r)
{
  // calc torus center
  Vector3 tcenter=c+0.5*l*axis;

  // torus inner radius
  double h=0.5*l-l2;
  double k=(r-r2);
  double ri=0.5*(k+(h*h)/k);
  
  // torus outer radius
  double ro=ri+r2;

  std::cout << "torus: " << tcenter << " - " << ro << " , " << ri << std::endl;
  m_tor=Torus(tcenter,axis,ro,ri,false);
}

const map<double,const AGeometricObject*> DogBone::getClosestObjects(const Vector3& P,int n) const
{
  map<double,const AGeometricObject*> res=CylinderVol::getClosestObjects(P,n);

  res.insert(make_pair(m_tor.getDist(P),&m_tor));
  
  return res;
}

bool DogBone::isIn(const Vector3& P) const
{
  bool res=CylinderVol::isIn(P);

  bool in_torus=(m_tor.getDist(P)>0);

  return res && in_torus;
}

bool DogBone::isIn(const Sphere& S)
{
  bool res=CylinderVol::isIn(S);

  bool in_torus=(m_tor.getDist(S.Center())>S.Radius());

  return res && in_torus;
}
