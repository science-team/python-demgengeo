/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __CIRCMNTABLEXY2D_H
#define __CIRCMNTABLEXY2D_H

// --- Project includes ---
#include "CircMNTable2D.h"

/*!
  \class CircMNTableXY2D
  \brief circular 2D Multi-group Neighbour table

  Neighbour table supporting multiple tagged groups of particles and circular boundary
  conditions in both x- and y- direction
*/
class CircMNTableXY2D : public CircMNTable2D
{
 protected:
  Vector3 m_shift_vec_y;

  virtual void set_y_circ();
  virtual int getIndex(const Vector3&) const;

 public:
  CircMNTableXY2D();
  CircMNTableXY2D(const Vector3&,const Vector3&,double,unsigned int);
  virtual ~CircMNTableXY2D();
  
  virtual bool insert(const Sphere&,unsigned int);
  virtual bool insertChecked(const Sphere&,unsigned int,double tol=s_small_value);
  virtual bool checkInsertable(const Sphere&,unsigned int);
  virtual void generateBonds(int,double,int);  

  // output 
  friend ostream& operator << (ostream&,const CircMNTableXY2D&);
};

#endif // __CIRCMNTABLEXY2D_H
