/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef  __CLIPPED_SPHERE_VOL_H
#define  __CLIPPED_SPHERE_VOL_H

// --- Project includes ---
#include "SphereVol.h"
#include "geometry/Plane.h"

// --- STL includes ---
#include <map>
#include <utility>

using std::map;
using std::pair;

class ClippedSphereVol :  public SphereVol
{
 protected:
  vector<pair<Plane,bool> > m_planes;

 public:
  ClippedSphereVol();
  ClippedSphereVol(const Vector3&,double);
  virtual ~ClippedSphereVol(){};

  void addPlane(const Plane&,bool);
  virtual Vector3 getAPoint(int) const;  
  virtual const map<double,const AGeometricObject*> getClosestObjects(const Vector3&,int) const;
  virtual bool isIn(const Vector3&) const;
  virtual bool isIn(const Sphere&);

  //friend ostream& operator << (ostream&,const ClippedSphereVol&);
};

#endif // __CLIPPED_SPHERE_VOL_H
