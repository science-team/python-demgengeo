/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __AVOLUME_H
#define __AVOLUME_H

// --- Project includes ---
#include "util/vector3.h"
#include "geometry/Sphere.h"

// --- STL includes ---
#include <utility>
#include <vector>

using std::pair;
using std::vector;

/*!
  \class AVolume

  Abstract base class for a 2D volume for particle packing
*/
class AVolume
{
 private:

 public:
  virtual ~AVolume(){};
  virtual pair<Vector3,Vector3> getBoundingBox() = 0;
  virtual Vector3 getAPoint(int) const = 0;
  //  virtual bool hasPlane() const = 0;
//  virtual bool isIn(const Vector3&) = 0;
  virtual bool isIn(const Vector3&) const = 0;
  virtual bool isIn(const Sphere&) =0;
  virtual int getNumberSubVolumes()const{return 1;};
};

#endif // __AVOLUME_H
