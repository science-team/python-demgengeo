/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __TRIPATCHSET_H
#define __TRIPATCHSET_H

// project includes
#include "JointSet.h"
#include "geometry/Triangle3D.h"
#include "util/vector3.h"

// STL includes
#include <vector>
#include <utility>

using std::vector;
using std::pair;
using std::make_pair;

/*!
  class to store joints as a collection of triangular patches and check if a bond crosses one of them
*/
class TriPatchSet : public JointSet
{
 private:
  vector<Triangle3D> m_triangles;
  Vector3 m_pmin;
  Vector3 m_pmax;
  bool m_bbx_set;

 public:
  TriPatchSet();
  virtual ~TriPatchSet(){};


  virtual int isCrossing(const Vector3&, const Vector3&) const;
  void addTriangle(const Vector3&, const Vector3&, const Vector3&,int);
  Vector3 getMinPoint() const {return m_pmin;};
  Vector3 getMaxPoint() const {return m_pmax;};
  vector<Triangle3D>::const_iterator triangles_begin() const {return m_triangles.begin();};
  vector<Triangle3D>::const_iterator triangles_end() const {return m_triangles.end();};
  pair<Vector3,Vector3> getBoundingBox() {return make_pair(m_pmin,m_pmax);};
};

#endif // __TRIPATCHSET_H
