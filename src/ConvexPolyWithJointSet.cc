/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "ConvexPolyWithJointSet.h"

ConvexPolyWithJointSet::ConvexPolyWithJointSet()
{}

/*!
  Construct polyhedron - without planes its just a box 

  \param pmin minimum point of bounding box
  \param pmax maximum point of bounding box
*/
ConvexPolyWithJointSet::ConvexPolyWithJointSet(const Vector3& pmin,const Vector3& pmax) 
  : ConvexPolyhedron(pmin,pmax)
{}

ConvexPolyWithJointSet::~ConvexPolyWithJointSet()
{}

/*!
  get point inside the box. The Argument is ignored
*/
Vector3 ConvexPolyWithJointSet::getAPoint(int)const
{
  double px,py,pz;
  Vector3 res;
  
  //  std::cout << m_pmin << " - " << m_pmax << std::endl;
  do{
    px=m_random(m_pmin.x(),m_pmax.x());
    py=m_random(m_pmin.y(),m_pmax.y());
    pz=m_random(m_pmin.z(),m_pmax.z());
    res=Vector3(px,py,pz);
  } while (!isIn(res));
  return res;
}

/*!
  check if point is inside

  \param p the point
*/
bool ConvexPolyWithJointSet::isIn(const Vector3& p) const
{
  bool res;
  // std::cout << p << std::endl;
  // check inside bounding box
  res= ((p.x()>m_pmin.x()) && (p.x()<m_pmax.x()) && 
	(p.y()>m_pmin.y()) && (p.y()<m_pmax.y()) &&
	(p.z()>m_pmin.z()) && (p.z()<m_pmax.z()));

  // std::cout << "inside bbx : " << res << std::endl;
  // if inside, check against planes
  if(res){
    vector<Plane>::const_iterator iter=m_planes.begin();
  
    while((iter!=m_planes.end()) && (res)){
      Vector3 normal=iter->getNormal();
      Vector3 pdist=p-(iter->getOrig());
      res=(normal*pdist>0);
      iter++;
    }
  }
  return res;
}

/*!
  check if a Sphere is inside and doesn't intersect any planes

  \param S the Sphere
*/
bool ConvexPolyWithJointSet::isIn(const Sphere& S)
{
  bool res;

  double r=S.Radius();
  Vector3 p=S.Center();

  //  std::cout << "rad: " << r << "  pos: " << p << std::endl;
  // inside bbx
  res=(p.X()>m_pmin.X()+r) && (p.X()<m_pmax.X()-r) && 
    (p.Y()>m_pmin.Y()+r) && (p.Y()<m_pmax.Y()-r) &&
    (p.Z()>m_pmin.Z()+r) && (p.Z()<m_pmax.Z()-r);
  
  // if inside, check against planes
  if(res){
    vector<Plane>::const_iterator iter=m_planes.begin();
  
    while((iter!=m_planes.end()) && (res)){
      Vector3 normal=iter->getNormal();
      Vector3 pdist=p-(iter->getOrig());
      res=(normal*pdist>r);
      iter++;
    }
  }
  
  if(res){
    // check intersection with joints
    vector<Triangle3D>::iterator iter=m_joints.begin();
    double dist=2*r;
    while((iter!=m_joints.end()) && res){
      dist=iter->getDist(p);
      res=(dist>r);
      iter++;
    }
  }

  return res;
}

void ConvexPolyWithJointSet::addJoints(const TriPatchSet& t)
{
  for(vector<Triangle3D>::const_iterator iter=t.triangles_begin();
      iter!=t.triangles_end();
      iter++){
    m_joints.push_back(*iter);
  }
}
