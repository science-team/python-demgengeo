/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __CIRCMNTABLE3D_H
#define __CIRCMNTABLE3D_H

// --- Project includes ---
#include "MNTable3D.h"

/*!
  \class CircMNTable3D
  \brief circular Multi-group Neighbour table

  Neighbour table supporting multiple tagged groups of particles and circular boundary
  conditions (x only at the moment)
*/
class CircMNTable3D : public MNTable3D
{
 protected:
  Vector3 m_shift_x;

  virtual void set_x_circ();
  virtual int getIndex(const Vector3&) const;
  int getXIndex(const Vector3&) const;
  int getYIndex(const Vector3&) const;
  int getZIndex(const Vector3&) const;
  int getFullIndex(const Vector3&) const;


 public:
  CircMNTable3D();
  CircMNTable3D(const Vector3&,const Vector3&,double,unsigned int);
  ~CircMNTable3D();
  
  virtual bool insert(const Sphere&,unsigned int);
  virtual bool checkInsertable(const Sphere&,unsigned int);
  virtual bool insertChecked(const Sphere&,unsigned int,double=s_small_value);
  virtual void generateBonds(int,double,int);  
  virtual void generateClusterBonds(int,double,int,int);
};

#endif // __CIRCMNTABLE3D_H
