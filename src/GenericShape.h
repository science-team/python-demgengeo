/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef GENERICSHAPE_H
#define GENERICSHAPE_H

#include <iostream>
#include <boost/regex.hpp>
#include "util/vector3.h"
#include "Shape.h"
#include "string"

using std::string;
using boost::regex;
using boost::regex_match;

class GenericShape : public Shape {
  public:
    GenericShape(string, string);
    void insert(Vector3 pos, double radius, MNTable3D* ntable, int, int);
    int bias();
    void setBias(int);

  protected:
    vector<Vector3> origins;
    vector<double> radii;
    vector<vector<int> > bonds;
};


#endif /* GENERICSHAPE_H */
