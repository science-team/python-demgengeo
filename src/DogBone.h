/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __DOGBONE_H
#define __DOGBONE_H

// --- Project includes ---
#include "CylinderVol.h"
#include "geometry/Cylinder.h"
#include "geometry/Torus.h"
#include "geometry/Plane.h"

// --- STL includes ---
#include <vector>
#include <map>

using std::vector;
using std::map;

class DogBone : public CylinderVol
{
 protected:
  Torus m_tor;

 public:
  DogBone();
  DogBone(const Vector3&,const Vector3&,double,double,double,double);
  virtual ~DogBone(){};

  virtual const map<double,const AGeometricObject*> getClosestObjects(const Vector3&,int) const;
  virtual bool isIn(const Vector3&) const;
  virtual bool isIn(const Sphere&);  

  
};

#endif // __DOGBONE_H
