/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __MNTABLE3D_H
#define __MNTABLE3D_H

// --- STL includes ---
#include <boost/python.hpp>
#include <vector>
#include <map>
#include <string>
#include <set>

using std::vector;
using std::map;
using std::multimap;
using std::string;
using std::set;


// --- IO includes ---
#include <iostream>

using std::ostream;

// --- Project includes ---
#include "MNTCell.h"
#include "AVolume3D.h"
#include "AVolume.h"
#include "geometry/Sphere.h"
#include "util/vector3.h"
#include "geometry/Plane.h"
#include "JointSet.h"
#include "TriPatchSet.h"

// --- STL includes ---
#include <vector>
#include <map>
#include <utility>
#include <string>

using std::vector;
using std::map;
using std::pair;
using std::string;

/*!
  \class MNTable3D
  \brief Multi-group Neighbour table

  Neighbour table supporting multiple tagged groups of particles
*/
class MNTable3D
{
 public: // types
  typedef pair<int,int> bond;

 protected:
  MNTCell* m_data;
  map<int,set<bond> > m_bonds; 
  Vector3 m_origin; //!< origin
  Vector3 m_min_pt, m_max_pt; // original min/max point
  double m_celldim; //!< cell size
  int m_nx,m_ny,m_nz; //!< number of cells in x-, y- and z-direction
  unsigned int m_ngroups;
  static int s_output_style; 
  static double s_small_value;
  int m_x_periodic,m_y_periodic,m_z_periodic;
  // bounding box handling
  bool m_bbx_tracking; //!< if true, bbx is updated for each insert operation 
  bool m_has_tight_bbx; //!< true if a tight bounding box has been calculated
  bool m_write_tight_bbx; //!< if true, tight bbx is written instead of min/max
  Vector3 m_min_tbbx, m_max_tbbx; //!< corner points of tight bounding box

  virtual int getIndex(const Vector3&) const;
  inline int idx(int i,int j, int k) const{return i*m_ny*m_nz+j*m_nz+k;};
  void WriteAsVtkXml(ostream&) const;
  virtual void set_x_circ();
  virtual void set_y_circ();
  virtual void set_z_circ();
  int m_write_prec; //!< precision (nr. of significant digits) for file output
  
  // blocked output handling
  bool m_is_writing_blocks;
  string m_outfilename;
  string m_temp_bondfilename;
  std::ios::pos_type m_np_write_pos;
  unsigned int m_block_particles_written;
  unsigned int m_block_bonds_written;

 public:
  MNTable3D();
  MNTable3D(const Vector3&,const Vector3&,double,unsigned int);
  virtual ~MNTable3D();

  bool insertFromRawFile(const string&,double,double);
  virtual bool insert(const Sphere&,unsigned int);
  virtual bool insertChecked(const Sphere&,unsigned int,double=s_small_value);
  virtual bool checkInsertable(const Sphere&,unsigned int);
  void GrowNGroups(unsigned int);
  const multimap<double,const Sphere*> getSpheresClosestTo(const Vector3&,unsigned int) const;
  const multimap<double,const Sphere*> getSpheresFromGroupNear(const Vector3&,double,int) const;
  const vector<const Sphere*> getAllSpheresFromGroup(int) const;
  const Sphere* getClosestSphereFromGroup(const Sphere&,int) const;
  int getTagOfClosestSphereFromGroup(const Sphere&,int) const;
  const Sphere* getClosestSphereFromGroup(const Sphere&,int,double,double,double) const;
  int getTagOfClosestSphereFromGroup(const Sphere&,int,double,double,double) const;
  void tagParticlesAlongPlane(const Plane&,double,int,unsigned int);
  void tagParticlesAlongPlaneWithMask(const Plane&,double,int,int,unsigned int);
  void tagParticlesInSphere(const Sphere&,int,unsigned int);
  void tagParticlesInGroupNT(int tag,int gid){tagParticlesInGroup(tag,gid,-1);};
  void tagParticlesInGroup(int,int,int);
  void tagParticlesToClosest(int,int);
  void tagParticlesToClosestAnisotropic(int,int,double,double,double);
  void removeParticlesWithTag(int,unsigned int);
  void removeParticlesWithTagMask(unsigned int,int,int);
  void removeParticlesInVolume(AVolume3D*, int, bool);
  void removeParticlesInGroup(unsigned int);
  void tagParticlesAlongJoints(const  TriPatchSet&,double,int, int, unsigned int);
  void tagParticlesInVolume(const AVolume&,int,unsigned int);
  void renumberParticlesContinuous();

  // bbx handling
  virtual void calculateTightBoundingBox();
  void setBoundingBoxTracking(bool);
  
  boost::python::list getSphereListFromGroup(int) const;
  boost::python::list getSphereListDist(const Vector3&, double, int) const;
  virtual void generateBonds(int,double,int);
  virtual void generateClusterBonds(int,double,int,int);
  virtual void generateBondsWithJointSet(const TriPatchSet&,int,double,int);
  virtual void generateBondsTagged(int,double,int,int,int);
  virtual void generateRandomBonds(int,double,double,int,int,int);
  virtual void removeBonds();

  void insertBond(int,int,int);

  // output 
  double getSumVolume(int);
  friend ostream& operator << (ostream&,const MNTable3D&);
  static void SetOutputStyle(int);
  void SetOutputPrecision(int);
  void SetWriteTightBoundingBox(bool);
  void write(char *filename, int outputStyle);
  
  // block writing 
  void initBlockWriting(const string&);
  void writeBlock(const Vector3&,const Vector3&);
  void writeBondsBlocked();
  void finishBlockWriting();
};
#endif // __MNTABLE_H
