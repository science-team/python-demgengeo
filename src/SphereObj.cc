/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "SphereObj.h"

    void SphereObj::insert(Vector3 pos, double radius, MNTable3D* table,int tag,int id) {
      
      // First list all (only 1 in this case) sphere locations relative to the
      // middle of the "empty space"
      Vector3 sphere_origin(0,0,0);

      // Rotate each one through all axis using the rotate function as below
      //
      // The below will do nothing, but is an example of what to do with more
      // complex shapes with multiple spheres.
      if ( this->useRandomOrientation() ) {
        this->setRandomOrientation();
        sphere_origin = this->rotatePoint(sphere_origin);
      }

      // Translate all spheres to the given position
      sphere_origin = sphere_origin + pos;
      
      // Expand radius from unit circle to actual given radius for each circle
      // For example a circle that has radius 1/4 of unit circle should  get
      // radius 0.25f*radius.
      double r = radius*1.0f;

      // Finally insert all spheres and bonds where relevant.
      Sphere Sc(sphere_origin, r);
      if ( table->checkInsertable(Sc,id) ) {
        Sc.setTag(tag);
        table->insert(Sc,id);
      }
    }
    void SphereObj::setBias(int factor) {
      this->bias_factor = factor;
    }
    int SphereObj::bias() {
      return this->bias_factor;
    }
