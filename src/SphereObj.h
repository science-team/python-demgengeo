/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "util/vector3.h"
#include "MNTable3D.h"
#include "Shape.h"

class SphereObj : public Shape {
  public:
    void setBias(int);
    void insert(Vector3 pos, double radius, MNTable3D* ntable, int,int);
    int bias();
  protected:
    int bias_factor;
};
