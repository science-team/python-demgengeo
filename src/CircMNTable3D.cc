/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "CircMNTable3D.h"

// --- System includes ---
#include <cmath>

using std::floor;

CircMNTable3D::CircMNTable3D()
{}

/*!
  Construct CircMNTable3D. Just calls MNTable3D constructor.

  \param MinPt minimum point (z component ignored)
  \param MaxPt maximum point (z component ignored)
  \param cd cell dimension
  \param ngroups initial number of particle groups
*/
CircMNTable3D::CircMNTable3D(const Vector3& MinPt,const Vector3& MaxPt,double cd,unsigned int ngroups):
  MNTable3D(MinPt, MaxPt, cd, ngroups)
{
  // check if grid spacing fits size in circular direction:
  double nx=(MaxPt-MinPt).X()/m_celldim;
  // error message if not
  if(nx!=floor(nx)){
    std::cout << "WARNING! grid spacing " << m_celldim << " doesn't fit periodic x-dimension " << (MaxPt-MinPt).X() << std::endl;
  }
  m_shift_x=Vector3((m_max_pt-m_min_pt).X(),0.0,0.0);
  set_x_circ();
}

/*!
  Destruct CircMNTable3D. Just calls MNTable3D destructor.
*/
CircMNTable3D::~CircMNTable3D()
{}

/*!
  set circularity of x-dimension to 1
*/
void CircMNTable3D::set_x_circ()
{
  m_x_periodic=1;
} 

int CircMNTable3D::getXIndex(const Vector3& Pos) const
{
  return int(floor((Pos.x()-m_origin.x())/m_celldim));
}

int CircMNTable3D::getYIndex(const Vector3& Pos) const
{
  return int(floor((Pos.y()-m_origin.y())/m_celldim));
}

int CircMNTable3D::getZIndex(const Vector3& Pos) const
{
  return int(floor((Pos.z()-m_origin.z())/m_celldim));
}

int CircMNTable3D::getFullIndex(const Vector3& Pos) const
{
  int ix=int(floor((Pos.x()-m_origin.x())/m_celldim));
  int iy=int(floor((Pos.y()-m_origin.y())/m_celldim));
  int iz=int(floor((Pos.z()-m_origin.z())/m_celldim));

  return idx(ix,iy,iz);
}

/*!
  get the cell index for a given position

  \param Pos the position
  \return the cell index if Pos is inside the table, -1 otherwise
*/
int CircMNTable3D::getIndex(const Vector3& Pos) const
{
  int ret;

  int ix=int(floor((Pos.x()-m_origin.x())/m_celldim));
  int iy=int(floor((Pos.y()-m_origin.y())/m_celldim));
  int iz=int(floor((Pos.z()-m_origin.z())/m_celldim));

  // check if pos is in table excl. padding
  if((ix>=0) && (ix<=m_nx-1) && (iy>0) && (iy<m_ny-1) && (iz>0) && (iz<m_nz-1)){
    ret=idx(ix,iy,iz);
  } else {
    ret=-1;
  }
  
  return ret;
}

/*!
  Insert sphere. Insert clone into other side of Table.

  \param S the Sphere
  \param gid the group id
*/
bool CircMNTable3D::insert(const Sphere& S,unsigned int gid)
{
  bool res;

  int id=getIndex(S.Center());
  int idx=getXIndex(S.Center());
  
  if((id!=-1) && (idx>0) && (idx<m_nx-1) && (gid<m_ngroups)){ // valid index
    // insert sphere
    m_data[id].insert(S,gid);
    res=true;
    int xidx=getXIndex(S.Center());
    // insert clone
    if (xidx==1){
      Sphere SClone=S;
      SClone.shift(m_shift_x);
      int clone_id=getFullIndex(SClone.Center());
      m_data[clone_id].insert(SClone,gid);
    } else if  (xidx==m_nx-2){
      Sphere SClone=S;
      SClone.shift(-1.0*m_shift_x);
      int clone_id=getFullIndex(SClone.Center());
      m_data[clone_id].insert(SClone,gid);
    } 
  } else {
    res=false;
  }

  return res;
}
/*!
  check if sphere is insertable

  \param S the Sphere
  \param gid the group id
*/
bool CircMNTable3D::checkInsertable(const Sphere& S,unsigned int gid)
{
  bool orig_insertable=false;
  bool clone_insertable=false;
  bool needs_cloning=false;	
  int id=getIndex(S.Center());
  int idx=getXIndex(S.Center());
  Sphere SClone=S;

  if((id!=-1) && (idx>0) && (idx<m_nx-1) && (gid<m_ngroups)){ // original position is inside "core" of the table
    // check if original is insertable
    multimap<double,const Sphere*> close_spheres=getSpheresFromGroupNear(S.Center(),S.Radius()-s_small_value,gid);
    orig_insertable=(close_spheres.size()==0);
    // calculate clone location
    int xidx=getXIndex(S.Center());
    // check if clone is insertable
    if (xidx==1){ // original in x-min slice of the table -> shift clone to +x  
      needs_cloning=true;
      SClone.shift(m_shift_x);
      multimap<double,const Sphere*> close_spheres=getSpheresFromGroupNear(SClone.Center(),SClone.Radius()-s_small_value,gid);
      clone_insertable=(close_spheres.size()==0);
    } else if  (xidx==m_nx-2){ // original in x-max slice of the table -> shift clone to -x  
      needs_cloning=true;
      SClone.shift(-1.0*m_shift_x);
      multimap<double,const Sphere*> close_spheres=getSpheresFromGroupNear(SClone.Center(),SClone.Radius()-s_small_value,gid);
      clone_insertable=(close_spheres.size()==0);
    } // original not in end slices -> no cloning
  } 
  
  return (orig_insertable && (clone_insertable || (!needs_cloning))); // original is insertable and clone is either insertable or not needed
}

/*!
  Insert sphere if it doesn't collide with other spheres. Insert clone into other side of Table.

  \param S the Sphere
  \param gid the group id
*/
bool CircMNTable3D::insertChecked(const Sphere& S,unsigned int gid,double tol)
{
  bool res;
  bool orig_insertable=false;
  bool clone_insertable=false;
  bool needs_cloning=false;	
  int id=getIndex(S.Center());
  int idx=getXIndex(S.Center());
  Sphere SClone=S;

  tol+=s_small_value;
  if((id!=-1) && (idx>0) && (idx<m_nx-1) && (gid<m_ngroups)){ // original position is inside "core" of the table
    // check if original is insertable
    multimap<double,const Sphere*> close_spheres=getSpheresFromGroupNear(S.Center(),S.Radius()-tol,gid);
    orig_insertable=(close_spheres.size()==0); // insertable if there are no spheres too close
    // calculate clone location
    int xidx=getXIndex(S.Center());
    // check if clone is insertable
    if (xidx==1){ // original in x-min slice of the table -> shift clone to +x  
      needs_cloning=true;
      SClone.shift(m_shift_x);
      multimap<double,const Sphere*> close_spheres=getSpheresFromGroupNear(SClone.Center(),SClone.Radius()-tol,gid);
      clone_insertable=(close_spheres.size()==0);
    } else if  (xidx==m_nx-2){ // original in x-max slice of the table -> shift clone to -x  
      needs_cloning=true;
      SClone.shift(-1.0*m_shift_x);
      multimap<double,const Sphere*> close_spheres=getSpheresFromGroupNear(SClone.Center(),SClone.Radius()-tol,gid);
      clone_insertable=(close_spheres.size()==0);
    } // original not in end slices -> no cloning
  } 

  if(orig_insertable && (clone_insertable || (!needs_cloning))){ // if origin is insertable and clone is either insertable or not needed
	m_data[id].insert(S,gid); // insert original
	if (needs_cloning) { // if needed, insert clone
		int clone_id=getFullIndex(SClone.Center());
		m_data[clone_id].insert(SClone,gid);
	}
	res=true;
  } else {
    res=false;
  }
  
  return res;
}


/*!
  generate bonds between particles of a group

  \param gid the group ID
  \param tol max. difference between bond length and equilibrium dist.
  \param btag bond tag
*/
void CircMNTable3D::generateBonds(int gid,double tol,int btag)
{
  std::cout << "MNTable3D::generateBonds( " << gid << " , " << tol << " , " << btag << " )" << std::endl;  // loop over all cells 
  for(int i=0;i<m_nx-1;i++){
    for(int j=1;j<m_ny-1;j++){
      for(int k=1;k<m_nz-1;k++){
	int id=idx(i,j,k);
	// loop over "upper" neighbors of each cell
	for(int ii=-1;ii<=1;ii++){
	  for(int jj=-1;jj<=1;jj++){
	    for(int kk=-1;kk<=1;kk++){
	      int id2=idx(i+ii,j+jj,k+kk);
	      vector<pair<int,int> > bonds;
	      if(((ii+jj+kk)==0)){ // intra-cell, not for boundary 
		bonds=m_data[id].getBonds(gid,tol);
	      } else if(id2>id){ // inter-cell
		bonds=m_data[id].getBonds(gid,tol,m_data[id2]);
	      } 
	      for(vector<pair<int,int> >::iterator iter=bonds.begin();
		  iter!=bonds.end();
		  iter++){	    
		m_bonds[btag].insert(*iter);
	      }
	    }
	  }
	}
      }
    }
  }
}

/*!
  generate bonds between particles of a group

  \param gid the group ID
  \param tol max. difference between bond length and equilibrium dist.
  \param btag1 tag for bonds within clusters (same particle tag)
  \param btag2 tag for bonds betweem clusters (different particle tag)
*/
void CircMNTable3D::generateClusterBonds(int gid,double tol,int btag1, int btag2)
{
  for(int i=0;i<m_nx-1;i++){
    for(int j=1;j<m_ny-1;j++){
      for(int k=1;k<m_nz-1;k++){
	int id=idx(i,j,k);
	// loop over "upper" neighbors of each cell
	for(int ii=-1;ii<=1;ii++){
	  for(int jj=-1;jj<=1;jj++){
	    for(int kk=-1;kk<=1;kk++){
	      int id2=idx(i+ii,j+jj,k+kk);
	      vector<pair<int,int> > same_bonds;
	      vector<pair<int,int> > diff_bonds;
	      if((id==id2) && (id!=-1)){ // intra-cell, not for boundary
		same_bonds=m_data[id].getBondsSame(gid,tol);
		diff_bonds=m_data[id].getBondsDiff(gid,tol);
	      } else if((id2>id) && (id!=-1) && (id2!=-1) ){ // inter-cell
		same_bonds=m_data[id].getBondsSame(gid,tol,m_data[id2]);
		diff_bonds=m_data[id].getBondsDiff(gid,tol,m_data[id2]);
	      } 
	      // insert intra-cluster bonds
	      for(vector<pair<int,int> >::iterator iter=same_bonds.begin();
		  iter!=same_bonds.end();
		  iter++){	    
		if(m_bonds[btag1].find(*iter)==m_bonds[btag1].end()){
		  m_bonds[btag1].insert(*iter);
		}
	      }
	      // insert inter-cluster bonds
	      for(vector<pair<int,int> >::iterator iter=diff_bonds.begin();
		  iter!=diff_bonds.end();
		  iter++){	    
		if(m_bonds[btag2].find(*iter)==m_bonds[btag2].end()){
		  m_bonds[btag2].insert(*iter);
		}
	      } 
	    }
	  }
	}
      }
    }
  }
}
