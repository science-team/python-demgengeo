/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "Model2D.h"

// --- IO includes ---
#include <fstream>
#include <iostream>

using std::ofstream;
using std::endl;

/*!
  constructor

  \param Pmin minimum point
  \param Pmax maximum point
  \param cdim cell dimension for neigbour table
*/
Model2D::Model2D(const Vector3& Pmin,const Vector3& Pmax,double cdim)
{
  m_ntable=new MNTable2D(Pmin,Pmax,cdim,1);
  m_ngroups=1;
  m_max_gid=-1;
  m_min_pt=Pmin;
  m_max_pt=Pmax;
}

/*!
  destructor
*/
Model2D::~Model2D()
{
  if(m_ntable!=NULL) delete m_ntable;
}


/*!
  Add particle group

  \param PGPtr pointer to particle group
  \return id of the particle group
*/
int Model2D::AddParticleGroup(AParticleGroup* PGPtr)
{
  m_max_gid++;
  if(m_max_gid>=m_ngroups){
    m_ngroups=m_ngroups*2;
    m_ntable->GrowNGroups(m_ngroups);
  }
  PGPtr->Init(m_ntable,m_max_gid);
  m_pgroups.push_back(PGPtr);

  return m_max_gid;
}

/*!
  Write Geonmetry out to file

  \param filename the filename
*/
void Model2D::WriteAsGeoFile(const string& filename)
{
  ofstream outfile(filename.c_str());

  outfile << "LSMGeometry 1.2" << endl;
  outfile << "BoundingBox" << m_min_pt << " " << m_max_pt << endl;
  outfile << "PeriodicBoundaries 0 0 0" << endl;
  outfile << "Dimension 2D" << endl;
  outfile << *m_ntable;

  outfile.close();
}
