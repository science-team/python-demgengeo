/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __AGENERATOR3D_H
#define __AGENERATOR3D_H

// --- Project includes ---
#include "AVolume3D.h"
#include "MNTable3D.h"
#include "ShapeList.h"
/*!
  \class AGenerator3D

  Abstract base class for a particle packing generator in 3D 
*/
class AGenerator3D
{
 public:
  virtual ~AGenerator3D(){};
  virtual void generatePacking (AVolume3D*,MNTable3D*,int,int,ShapeList*) = 0;
};

#endif // __AGENERATOR3D_H
