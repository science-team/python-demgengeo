/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __BOXWITHLINES2DSUBVOL_H
#define __BOXWITHLINES2DSUBVOL_H

// --- Project includes ---
#include "BoxWithLines2D.h"

// --- STL includes ---
#include <vector>

using std::vector;

/*!
  \class BoxWithLines2DSubVol

  A class for the generation of random particles inside a box. An arbitrary number 
  of lines can be added to which the particles are fitted.  The class supplies a number of 
  subvolumes for the getAPoint function 
*/
class BoxWithLines2DSubVol : public BoxWithLines2D
{
 private:
  double m_svdim_x,m_svdim_y;
  int m_nsv_x,m_nsv_y;

 public:
  BoxWithLines2DSubVol();
  BoxWithLines2DSubVol(const Vector3&,const Vector3&,double,double);
  virtual ~BoxWithLines2DSubVol(){};

  virtual Vector3 getAPoint(int) const;
  virtual int getNumberSubVolumes() const;
};
#endif // __BOXWITHLINES2DSUBVOL_H
