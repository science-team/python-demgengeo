/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __HEXAGGREGATEINSERTGENERATOR2DRAND_H
#define __HEXAGGREGATEINSERTGENERATOR2DRAND_H

// --- Project includes ---
#include "HexAggregateInsertGenerator2D.h"

/*!
  \class InsertGenerator2D

  Packing generator using Place et al. insertion based algorithm. 
*/
class HexAggregateInsertGenerator2DRand : public HexAggregateInsertGenerator2D
{
 protected:
  double m_remove_prob;

  virtual void seedParticles(AVolume2D* ,MNTable2D* ,int,int);

 public:
  HexAggregateInsertGenerator2DRand();
  HexAggregateInsertGenerator2DRand(double,double,int,int,double,double);
  virtual ~HexAggregateInsertGenerator2DRand(){};

  virtual void fillIn(AVolume2D* ,MNTable2D* ,int,int);
};

#endif // __HEXAGGREGATEINSERTGENERATOR2DRAND_H
