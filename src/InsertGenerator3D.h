/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __INSERTGENERATOR3D_H
#define __INSERTGENERATOR3D_H

// --- Project includes ---
#include "AGenerator3D.h"

/*!
  \class InsertGenerator3D

  Packing generator using Place et al. insertion based algorithm in 3D. 
*/
class InsertGenerator3D : public AGenerator3D
{
 protected:
  double m_rmin;
  double m_rmax;
  double m_max_tries;
  int m_max_iter;
  double m_prec;
  double m_next_tag;
  bool m_old_seeding;

  virtual void seedParticles(AVolume3D* ,MNTable3D* ,int,int);
  virtual void seedParticles(AVolume3D* ,MNTable3D* ,int,int,ShapeList*);

 public:
  InsertGenerator3D();
  InsertGenerator3D(double,double,int,int,double);
  InsertGenerator3D(double,double,int,int,double,bool);
  virtual ~InsertGenerator3D();

  void setOldSeeding(bool);
  virtual void fillIn(AVolume3D* ,MNTable3D* ,int,int);
  virtual void fillIn(AVolume3D* ,MNTable3D* ,int,int,ShapeList*);
  virtual void fillIn(AVolume3D* ,MNTable3D* ,int,int,double);
  virtual void generatePacking3 (AVolume3D*,MNTable3D*,int);
  virtual void generatePacking4 (AVolume3D*,MNTable3D*,int,int);
  virtual void generatePackingMaxVolume(AVolume3D*,MNTable3D*,int,int,double);
  virtual void generatePacking (AVolume3D*,MNTable3D*,int,int,ShapeList*);
  void setNextTag(int);

  friend ostream& operator << (ostream&,const InsertGenerator3D&);
};

#endif // __INSERTGENERATOR2D_H
