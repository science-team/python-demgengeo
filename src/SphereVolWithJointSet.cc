/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "SphereVolWithJointSet.h"

//--- 
#include <cmath>
#include <cstdlib>

using std::cos;
using std::sin; 

// --- STL includes ---
#include <utility>

using std::make_pair;

SphereVolWithJointSet::SphereVolWithJointSet()
{}

SphereVolWithJointSet::SphereVolWithJointSet(const Vector3& c,double r)
{
  m_sph=SphereIn(c,r);
}

const map<double,const AGeometricObject*> SphereVolWithJointSet::getClosestObjects(const Vector3& P,int) const
{
  map<double,const AGeometricObject*> res;

  res.insert(make_pair(m_sph.getDist(P),&m_sph));

  for(vector<Triangle3D>::const_iterator iter=m_joints.begin();
      iter!=m_joints.end();
      iter++){
    double ndist=iter->getDist(P);
    res.insert(make_pair(ndist,&(*iter)));
  }

  return res;  
}

bool SphereVolWithJointSet::isIn(const Sphere& S)
{
  double r=S.Radius();
  Vector3 p=S.Center();

  bool res=(m_sph.getDirDist(S.Center())>S.Radius()); 

  if(res){
    // check intersection with joints
    vector<Triangle3D>::iterator iter=m_joints.begin();
    double dist=2*r;
    while((iter!=m_joints.end()) && res){
      dist=iter->getDist(p);
      res=(dist>r);
      iter++;
    }
  }
  
  return res;
}

void SphereVolWithJointSet::addJoints(const TriPatchSet& t)
{
  for(vector<Triangle3D>::const_iterator iter=t.triangles_begin();
      iter!=t.triangles_end();
      iter++){
    m_joints.push_back(*iter);
  }
}

ostream& operator << (ostream& ost,const SphereVolWithJointSet& T)
{
   return ost;
}
