/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __AVOLUME2D_H
#define __AVOLUME2D_H

// --- Project includes ---
#include "AVolume.h"
#include "geometry/Line2D.h"

// --- STL includes ---
#include <map>

using std::map;

class AVolume2D : public AVolume
{
 public:
  virtual ~AVolume2D(){};

  virtual Line2D getClosestPlane(const Vector3&) = 0;
  virtual const map<double,const Line2D*> getClosestPlanes(const Vector3&,int) const = 0;
  virtual const map<double,const AGeometricObject*> getClosestObjects(const Vector3&,int) const;
};

#endif // __AVOLUME2D_H
