/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __SPHERE_SECTION_VOL_H
#define  __SPHERE_SECTION_VOL_H

// --- Project includes ---
#include "AVolume3D.h"
#include "geometry/SphereIn.h"

// --- STL includes ---
#include <map>

using std::map;

class SphereSectionVol :  public AVolume3D
{
 protected:
  SphereIn m_sph;
  double m_section_dist;
  Vector3 m_section_normal; 

 public:
  SphereSectionVol();
  SphereSectionVol(const Vector3&,const double, const double, const Vector3&);
  virtual ~SphereSectionVol(){};

  virtual pair<Vector3,Vector3> getBoundingBox();
  virtual Vector3 getAPoint(int) const;  
  virtual const map<double,const AGeometricObject*> getClosestObjects(const Vector3&,int) const;
  virtual bool isIn(const Vector3&) const;
  virtual bool isIn(const Sphere&);
  virtual bool isFullyOutside(const Sphere&);

  friend ostream& operator << (ostream&,const SphereSectionVol&);
};

#endif // __SPHERE_SECTION_VOL_H
