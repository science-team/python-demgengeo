/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef  __CLIPPED_CIRCLE_VOL_H
#define  __CLIPPED_CIRCLE_VOL_H

// --- Project includes ---
#include "CircleVol.h"
#include "geometry/Line2D.h"

// --- STL includes ---
#include <map>
#include <utility>

using std::map;
using std::pair;

/*!
  A clipped circle volume. 
  
  The volume is bounded by a circle and by the lines added to the 
  volume using the addLine function. The in/out decision does depend
  on the line normals, i.e. particles on the side of a line where the
  normal is positive are considered inside, those on the negative side 
  outside.
*/
class ClippedCircleVol :  public CircleVol
{
 protected:
  vector<pair<Line2D,bool> > m_lines;

 public:
  ClippedCircleVol();
  ClippedCircleVol(const Vector3&,double);
  virtual ~ClippedCircleVol(){};

  void addLine(const Line2D&,bool);
  virtual Vector3 getAPoint(int) const;  
  virtual const map<double,const AGeometricObject*> getClosestObjects(const Vector3&,int) const;
  virtual bool isIn(const Vector3&) const;
  virtual bool isIn(const Sphere&);
};

#endif //  __CLIPPED_CIRCLE_VOL_H
