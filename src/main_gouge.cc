/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "geometry/Sphere.h"
#include "util/vector3.h"
#include "CircMNTable3D.h"
#include "InsertGenerator3D.h"
#include "granular_gouge.h"

// --- IO includes ---
#include <iostream>

using std::cout;
using std::cout;
using std::endl;

// -- System includes --
#include <cstdlib>
#include <sys/time.h>
using std::atoi;
using std::atof;


int main(int argc, char** argv)
{
  int ret=0;

  double xsize=atof(argv[1]);
  double ysize=atof(argv[2]);
  double ysize_bdry=atof(argv[3]);
  double zsize=atof(argv[4]);
  int nrid=atoi(argv[5]);
  double rid=atoi(argv[6]);
  int ntry=atoi(argv[7]);
  double rmin=atof(argv[8]);
  double grmin=atof(argv[9]);
  double grmax=atof(argv[10]);
  int outputstyle=atoi(argv[11]);


  // seed RNG
  struct timeval tv;
  gettimeofday(&tv,NULL);
  int random_seed=tv.tv_usec;
  srand(random_seed);

  Vector3 min=Vector3(0.0,0.0,0.0); 
  Vector3 max=Vector3(xsize,ysize,zsize);
  Vector3 gmin=Vector3(0.0,ysize_bdry,0.0);
  Vector3 gmax=Vector3(xsize,ysize-ysize_bdry,zsize);

  CircMNTable3D T(min,max,2.5,3);
  CircMNTable3D::SetOutputStyle(outputstyle);
  InsertGenerator3D G(rmin,1.0,ntry,1000,1e-6);

  generate_upper_tri_rough_block(&T,&G,xsize,ysize_bdry,zsize,ysize-ysize_bdry,nrid,rid);
  generate_lower_tri_rough_block(&T,&G,xsize,ysize_bdry,zsize,0.0,nrid,rid);
  generate_granular_gouge(&T,&G,gmin,gmax,grmin,grmax,ntry);
  T.generateBonds(0,1e-5,0);
  
  cout << T << endl;
  
  return ret;
}
