/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __MESH_VOLUME_2D_H
#define __MESH_VOLUME_2D_H

// --- Project includes ---
#include "AVolume2D.h"
#include "LineSet.h"
#include "util/vector3.h"

// --- STL includes ---

/*!
  \class MeshVolume

  A class for the generation of random particles inside a volume bounded by a triangle mesh.
  The correctness of the supplied mesh, i.e. that it actually defines a closed surface,
  is not checked in the class. Instead the caller is responsible to supply a correct mesh.
  No convexity or other topological requirements apply to the volume. 
  Facet orientations is not relevant.
  Works also if the mesh encloses multiple volumes. 
*/
class MeshVolume2D : public AVolume2D
{
 protected:
  LineSet m_mesh; 
  Vector3 m_MinPoint;
  Vector3 m_MaxPoint;
  Vector3 m_DistPoint;

  double m_random(double,double)const;

 public:
  MeshVolume2D();
  MeshVolume2D(const LineSet&);
  virtual ~MeshVolume2D(){};

  virtual pair<Vector3,Vector3> getBoundingBox();
  virtual Vector3 getAPoint(int) const;
  virtual const map<double,const AGeometricObject*> getClosestObjects(const Vector3&,int) const;
  virtual const map<double,const Line2D*> getClosestPlanes(const Vector3&,int) const;
  virtual Line2D getClosestPlane(const Vector3&);
  virtual bool isIn(const Vector3&) const;
  virtual bool isIn(const Sphere&);
  virtual bool isFullyOutside(const Sphere&);
};

#endif // __MESH_VOLUME_2D_H
