/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef SHAPELIST_H
#define SHAPELIST_H


#include "Shape.h"
#include "GenericShape.h"
#include "util/vector3.h"
#include "MNTable3D.h"

using std::string;

class ShapeList {
  protected:
    std::vector<Shape*> shapeList;
  
  public:
    void addHexShape(int, int);
    void addGenericShape(string,string,int,int,int,int);
    void insertShape(Vector3, double, MNTable3D *ntable,int tag,int id);
};

#endif /* SHAPELIST_H */
