/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __GRANULAR_GOUGE_H
#define __GRANULAR_GOUGE_H

#include "AGenerator3D.h"
#include "MNTable3D.h"

void generate_upper_rough_block(MNTable3D*,AGenerator3D*,double,double,double,double,int,double);
void generate_lower_rough_block(MNTable3D*,AGenerator3D*,double,double,double,double,int,double);
void generate_upper_tri_rough_block(MNTable3D*,AGenerator3D*,double,double,double,double,int,double); 
void generate_lower_tri_rough_block(MNTable3D*,AGenerator3D*,double,double,double,double,int,double); 
void generate_granular_gouge(MNTable3D*,AGenerator3D*,const Vector3&,const Vector3&,double,double,int);

#endif // __GRANULAR_GOUGE_H
