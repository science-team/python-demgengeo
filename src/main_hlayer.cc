/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "geometry/Sphere.h"
#include "util/vector3.h"
#include "InsertGenerator2D.h"
#include "MNTable2D.h"
#include "BoxWithLines2D.h"

// --- IO includes ---
#include <iostream>

using std::cout;
using std::cout;
using std::endl;

// -- System includes --
#include <cstdlib>
#include <sys/time.h>
using std::atoi;
using std::atof;

int main(int argc, char** argv)
{
  int ret=0;

  double xsize=atof(argv[1]);
  double ysize=atof(argv[2]);
  double ysize_cl=atof(argv[3]);
  int ntry=atoi(argv[4]);
  double rmin_nb=atof(argv[5]);
  double rmin_b=atof(argv[6]);
  double pbond=atof(argv[7]);

  int outputstyle=atoi(argv[8]);

  // calc dependent parameters
  double y_bdry_1=0.5*(ysize-ysize_cl);
  double y_bdry_2=ysize-y_bdry_1;

  MNTable2D* T=new MNTable2D(Vector3(0.0,0.0,0.0),Vector3(xsize,ysize,0.0),2.5,1);
  InsertGenerator2D *GB=new InsertGenerator2D(rmin_b,1.0,ntry,1000,1e-6);
  InsertGenerator2D *GNB=new InsertGenerator2D(rmin_nb,1.0,ntry,1000,1e-6);

  // seed RNG
  struct timeval tv;
  gettimeofday(&tv,NULL);
  int random_seed=tv.tv_usec;
  srand(random_seed);

  MNTable2D::SetOutputStyle(outputstyle);

  // center layer (bonded)
  BoxWithLines2D CenterBox(Vector3(0.0,y_bdry_1,0.0),Vector3(xsize,y_bdry_2,0.0));

  CenterBox.addLine(Line2D(Vector3(0.0,y_bdry_1,0.0),Vector3(0.0,y_bdry_2,0.0))); // left 
  CenterBox.addLine(Line2D(Vector3(xsize,y_bdry_1,0.0),Vector3(xsize,y_bdry_2,0.0))); // right
  CenterBox.addLine(Line2D(Vector3(0.0,y_bdry_1,0.0),Vector3(xsize,y_bdry_1,0.0))); // bottom
  CenterBox.addLine(Line2D(Vector3(0.0,y_bdry_2,0.0),Vector3(xsize,y_bdry_2,0.0))); // top

  GB->generatePacking(&CenterBox,T,0,1);
  T->generateRandomBonds(0,1e-5,pbond,0,1,0);


  // bottom layer
  BoxWithLines2D BottomBox(Vector3(0.0,0.0,0.0),Vector3(xsize,y_bdry_1+0.5,0.0));

  BottomBox.addLine(Line2D(Vector3(0.0,0.0,0.0),Vector3(0.0,y_bdry_1,0.0))); // left 
  BottomBox.addLine(Line2D(Vector3(xsize,0.0,0.0),Vector3(xsize,y_bdry_1,0.0))); // right
  BottomBox.addLine(Line2D(Vector3(0.0,0.0,0.0),Vector3(xsize,0.0,0.0))); // bottom

  GNB->generatePacking(&BottomBox,T,0,2);

  // top layer
  BoxWithLines2D TopBox(Vector3(0.0,y_bdry_2-0.5,0.0),Vector3(xsize,ysize,0.0));

  TopBox.addLine(Line2D(Vector3(0.0,y_bdry_2,0.0),Vector3(0.0,ysize,0.0))); // left 
  TopBox.addLine(Line2D(Vector3(xsize,y_bdry_2,0.0),Vector3(xsize,ysize,0.0))); // right
  TopBox.addLine(Line2D(Vector3(0.0,ysize,0.0),Vector3(xsize,ysize,0.0))); // top

  GNB->generatePacking(&TopBox,T,0,3);

  cout << *T << endl;

  delete T;
  delete GB;
  delete GNB;
  return ret;
}
