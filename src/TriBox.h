/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __TRIBOX_H
#define __TRIBOX_H

// --- Project includes ---
#include "AVolume3D.h"
#include "geometry/Plane.h"

// --- STL includes ---
#include <vector>
#include <map>

using std::vector;
using std::map;


class TriBox : public AVolume3D
{
 protected:
  vector<Plane> m_planes;
  Vector3 m_pmin;
  Vector3 m_pmax;
  bool m_inverted;

  double m_random(double,double)const;

 public:
  TriBox();
  TriBox(const Vector3&,const Vector3&,bool inverted=false);
  virtual ~TriBox(){};

  void addPlane(const Plane&);
  virtual pair<Vector3,Vector3> getBoundingBox();
  virtual Vector3 getAPoint(int)const;
  virtual bool hasPlane() const{return (m_planes.size()>0);};
  virtual Plane getClosestPlane(const Vector3&);
  virtual const map<double,const AGeometricObject*> getClosestObjects(const Vector3&,int) const;
  virtual bool isIn(const Vector3&) const;
  virtual bool isIn(const Sphere&);
  virtual bool isFullyOutside(const Sphere&);

  friend ostream& operator<< (ostream&, const TriBox&);
};

#endif // __TRIBOX_H
