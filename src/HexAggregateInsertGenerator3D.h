/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __HEXAGGREGATEINSERTGENERATOR3D_H
#define __HEXAGGREGATEINSERTGENERATOR3D_H

// --- Project includes ---
#include "InsertGenerator3D.h"

/*!
  \class HexAggregateInsertGenerator3D

  Packing generator using Place et al. insertion based algorithm to generate
  1st generation particles which are replaced by regular 12-particle aggreagates. 
*/
class HexAggregateInsertGenerator3D : public InsertGenerator3D
{
 protected:

  virtual void seedParticles(AVolume3D* ,MNTable3D* ,int,int);
  void ParticleToAggregate(MNTable3D*, const Sphere&,int); 

 public:
  HexAggregateInsertGenerator3D();
  HexAggregateInsertGenerator3D(double,double,int,int,double,bool);
  virtual ~HexAggregateInsertGenerator3D(){};

  virtual void fillIn(AVolume3D* ,MNTable3D* ,int,int);
};

#endif // __HEXAGGREGATEINSERTGENERATOR2D_H
