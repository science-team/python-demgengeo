/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __MNTABLE_H
#define __MNTABLE_H

// --- STL includes ---
#include <boost/python.hpp>
#include <vector>
#include <map>
#include <set>

using std::vector;
using std::map;
using std::multimap;
using std::set;

// --- IO includes ---
#include <iostream>

using std::ostream;

// --- Project includes ---
#include "MNTCell.h"
#include "geometry/Sphere.h"
#include "geometry/Line2D.h"
#include "geometry/LineSegment2D.h"

/*!
  \class MNTable2D
  \brief Multi-group Neighbour table

  Neighbour table supporting multiple tagged groups of particles
*/
class MNTable2D
{
 public: // types
  typedef pair<int,int> bond;

 protected:
  MNTCell* m_data;
  map<int,set<pair<int,int> > > m_bonds; 
  double m_x0,m_y0; //!< origin
  double m_celldim; //!< cell size
  int m_nx,m_ny; //!< number of cells in x- and y-direction
  unsigned int m_ngroups;
  static int s_output_style; 
  static double s_small_value;
  int m_x_periodic;
  int m_y_periodic;
  int m_write_prec; //!< precision (nr. of significant digits) for file output
  
  virtual int getIndex(const Vector3&) const;
  inline int idx(int i,int j) const {return i*m_ny+j;};
  void WriteAsVtkXml(ostream&) const;

 public:
  MNTable2D();
  MNTable2D(const Vector3&,const Vector3&,double,unsigned int);
  virtual ~MNTable2D();

  virtual bool insert(const Sphere&,unsigned int);
  virtual bool insertChecked(const Sphere&,unsigned int,double tol=s_small_value);
  virtual bool checkInsertable(const Sphere&,unsigned int);
  void GrowNGroups(unsigned int);
  const multimap<double,const Sphere*> getSpheresClosestTo(const Vector3&,unsigned int) const;
  const multimap<double,const Sphere*> getSpheresFromGroupNear(const Vector3&,double,int) const;
  const vector<const Sphere*> getAllSpheresFromGroup(int) const;
  const Sphere* getClosestSphereFromGroup(const Sphere&,int) const;
  Sphere* getClosestSphereFromGroup(const Vector3&,int) const;
  int getTagOfClosestSphereFromGroup(const Sphere&,int) const;
  void tagParticlesAlongLine(const Line2D&,double,int,unsigned int);
  void tagParticlesAlongLineWithMask(const Line2D&,double,int,int, unsigned int);
  void tagParticlesAlongLineSegment(const LineSegment2D&,double,int,int, unsigned int);
  void tagParticlesNear(const Vector3&,double,int,int);
  void tagClosestParticle(const Vector3&,int,int);
  void tagParticlesToClosest(int,int);
  void tagParticlesInVolume(const AVolume&,int,unsigned int);

  boost::python::list getSphereListFromGroup(int) const;
  boost::python::list getBondList(int);

  virtual void generateBonds(int,double,int);
  virtual void generateBondsWithMask(int,double,int,int,int);
  virtual void generateRandomBonds(int,double,double,int,int,int);
  virtual void generateClusterBonds(int,double,int,int);
  virtual void generateBondsTaggedMasked(int,double,int,int,int,int,int);
  void insertBond(int,int,int);
  void breakBondsAlongLineSegment(const LineSegment2D&,double,int, unsigned int);

  void removeTagged(int,int,int);

  // output 
  double getSumVolume(int);
  int getNrParticles(int);
  friend ostream& operator << (ostream&,const MNTable2D&);
  static void SetOutputStyle(int);
  void SetOutputPrecision(int);
  void write(char *filename, int outputStyle);
};
#endif // __MNTABLE_H
