/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "BoxWithLines2DSubVol.h"

// --- system includes ---
#include <cmath>

using std::ceil;

BoxWithLines2DSubVol::BoxWithLines2DSubVol()
{}

/*!
  construct box

  \param pmin minimum point of bounding box
  \param pmax maximum point of bounding box
  \param svdim_x
  \param svdim_y
*/  
BoxWithLines2DSubVol::BoxWithLines2DSubVol(const Vector3& pmin,const Vector3& pmax,double svdim_x,double svdim_y):BoxWithLines2D(pmin,pmax)
{
  m_nsv_x=int(ceil((pmax-pmin).X())/svdim_x);
  m_nsv_y=int(ceil((pmax-pmin).Y())/svdim_y);
  m_svdim_x=(pmax-pmin).X()/double(m_nsv_x);
  m_svdim_y=(pmax-pmin).X()/double(m_nsv_x);
}

/*!
  get a point within a given subvolume

  \param id index of the subvolume
*/
Vector3 BoxWithLines2DSubVol::BoxWithLines2DSubVol::getAPoint(int id) const
{
  int idx=id%m_nsv_x;
  int idy=id/m_nsv_x;

  double minx=double(idx)*m_svdim_x;
  double maxx=double(idx+1)*m_svdim_x;
  double miny=double(idy)*m_svdim_y;
  double maxy=double(idy+1)*m_svdim_y;

   

  double px=m_random(minx,maxx);
  double py=m_random(miny,maxy);

  //std::cout << "id, x, y, p " << id << "  [ " << minx << " - " << maxx << " ]  [ " << miny << " - " << maxy << " ]  [ " << px << " - " << py << " ] " << std::endl;
  return Vector3(px,py,0.0);
}

int BoxWithLines2DSubVol::BoxWithLines2DSubVol::getNumberSubVolumes() const
{
  return m_nsv_x*m_nsv_y;
}
