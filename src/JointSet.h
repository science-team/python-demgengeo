/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __JOINTSET_H
#define __JOINTSET_H

// project includes
#include "util/vector3.h"

/*!
  class to store joints (i.e. surface patches) and check if a bond crosses one of them
*/
class JointSet
{
 public:
  virtual ~JointSet(){};


  virtual int isCrossing(const Vector3&, const Vector3&) const=0;
};

#endif // __JOINTSET_H
