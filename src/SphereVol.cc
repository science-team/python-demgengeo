/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "SphereVol.h"

//--- 
#include <cmath>
#include <cstdlib>

using std::cos;
using std::sin; 

// --- STL includes ---
#include <utility>

using std::make_pair;

SphereVol::SphereVol()
{}

SphereVol::SphereVol(const Vector3& c,double r)
{
  m_sph=SphereIn(c,r);
}

pair<Vector3,Vector3> SphereVol::getBoundingBox()
{
  Vector3 r=Vector3(m_sph.Radius(),m_sph.Radius(),m_sph.Radius());
  return make_pair(m_sph.Center()-r,m_sph.Center()+r);
}

Vector3 SphereVol::getAPoint(int) const
{
  double r=m_sph.Radius()*((double)(rand())/(double)(RAND_MAX));
  double phi=M_PI*((double)(rand())/(double)(RAND_MAX));
  double rho=2*M_PI*((double)(rand())/(double)(RAND_MAX));

  return m_sph.Center()+r*Vector3(sin(phi)*cos(rho),sin(phi)*sin(rho),cos(phi));
}

const map<double,const AGeometricObject*> SphereVol::getClosestObjects(const Vector3& P,int) const
{
  map<double,const AGeometricObject*> res;

  res.insert(make_pair(m_sph.getDist(P),&m_sph));

  return res;  
}

bool SphereVol::isIn(const Vector3& P) const
{
  return (m_sph.getDirDist(P)>0); 
}

bool SphereVol::isIn(const Sphere& S)
{
  return (m_sph.getDirDist(S.Center())>S.Radius()); 
}

/*
  check if sphere is fully outside volume.

  \param S the sphere
*/
bool SphereVol::isFullyOutside(const Sphere& S)
{
  double d=(m_sph.Center()-S.Center()).norm();
  return d>(m_sph.Radius()+S.Radius());
}

ostream& operator << (ostream& ost,const SphereVol& T)
{
   return ost;
}
