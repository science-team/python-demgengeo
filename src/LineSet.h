/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __LINESET_H
#define __LINESET_H

// project includes
#include "JointSet.h"
#include "geometry/LineSegment2D.h"
#include "util/vector3.h"

// STL includes
#include <vector>
#include <utility>

using std::vector;
using std::pair;
using std::make_pair;

/*!
  class to store a collection of line segments and check if a line crosses one of them
*/
class LineSet : public JointSet
{
 private:
  vector<LineSegment2D> m_segments;
  Vector3 m_pmin;
  Vector3 m_pmax;
  bool m_bbx_set;

 public:
  LineSet();
  virtual ~LineSet(){};


  virtual int isCrossing(const Vector3&, const Vector3&) const;
  void addLineSegment(const Vector3&, const Vector3&,int);
  Vector3 getMinPoint() const {return m_pmin;};
  Vector3 getMaxPoint() const {return m_pmax;};
  vector<LineSegment2D>::const_iterator segments_begin() const {return m_segments.begin();};
  vector<LineSegment2D>::const_iterator segments_end() const {return m_segments.end();};
  pair<Vector3,Vector3> getBoundingBox() {return make_pair(m_pmin,m_pmax);};
};

#endif // __LINESET_H
