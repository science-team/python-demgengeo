/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "EllipsoidVol.h"

//--- 
#include <cmath>
#include <cstdlib>

using std::cos;
using std::sin; 

// --- STL includes ---
#include <utility>

using std::make_pair;

EllipsoidVol::EllipsoidVol()
{}

EllipsoidVol::EllipsoidVol(const Vector3& c, double lx, double ly, double lz)
{
  m_posn=c;
  m_lx=lx;
  m_ly=ly;
  m_lz=lz;
}

pair<Vector3,Vector3> EllipsoidVol::getBoundingBox()
{
  Vector3 r=Vector3(0.5*m_lx,0.5*m_ly,0.5*m_lz);
  return make_pair(m_posn-r,m_posn+r);
}

Vector3 EllipsoidVol::getAPoint(int) const
{
/*
  double r=m_sph.Radius()*((double)(rand())/(double)(RAND_MAX));
  double phi=M_PI*((double)(rand())/(double)(RAND_MAX));
  double rho=2*M_PI*((double)(rand())/(double)(RAND_MAX));
*/
  double rx = m_lx*((double)(rand())/(double)(RAND_MAX) - 0.5);
  double ry = m_ly*((double)(rand())/(double)(RAND_MAX) - 0.5);
  double rz = m_lz*((double)(rand())/(double)(RAND_MAX) - 0.5);

  return m_posn + Vector3(rx,ry,rz);
}

const map<double,const AGeometricObject*> EllipsoidVol::getClosestObjects(const Vector3& P,int) const
{
  map<double,const AGeometricObject*> res;

//  res.insert(make_pair(m_sph.getDist(P),&m_sph));

  return res;  
}

bool EllipsoidVol::isIn(const Vector3& P) const
{
  Vector3 dR = P - m_posn;
  double rx = dR.x();
  double ry = dR.y();
  double rz = dR.z();
  double delta = rx*rx/(m_lx*m_lx) + ry*ry/(m_ly*m_ly) + rz*rz/(m_lz*m_lz) ;
  return (delta < 1.0);
}

/*!
  \warning WRONG
*/
bool EllipsoidVol::isIn(const Sphere& S)
{
//  return (m_sph.getDist(S.Center())>S.Radius()); 
  return (isIn(S.Center()));
}

/*
  Check if sphere is fully outside the volume.

  \param S the sphere
  \warning DUMMY IMPLEMENTATION
*/
bool EllipsoidVol::isFullyOutside(const Sphere&)
{
  return true;
}

ostream& operator << (ostream& ost,const EllipsoidVol& T)
{
   return ost;
}
