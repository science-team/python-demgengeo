/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __BOXWITHJOINTSET_H
#define __BOXWITHJOINTSET_H

// --- Project includes ---
#include "BoxWithPlanes3D.h"
#include "TriPatchSet.h"
#include "geometry/Triangle3D.h"

// --- STL includes ---
#include <vector>

using std::vector;

//--- IO includes ---
#include <iostream>

/*!
  \class BoxWithJointSet

  A Box shaped volume with a set of triangular patches to which the particles are fitted.
  Additionally, an arbitrary number of (boundary) planes can be added to which the particles are fitted.
*/
class BoxWithJointSet : public BoxWithPlanes3D
{
 protected:
  vector<Triangle3D> m_joints;

 public:
  BoxWithJointSet();
  BoxWithJointSet(const Vector3&,const Vector3&);
  virtual ~BoxWithJointSet(){};

  virtual const map<double,const AGeometricObject*> getClosestObjects(const Vector3&,int) const;
  void addJoints(const TriPatchSet&);
  virtual bool isIn(const Sphere&);

  friend ostream& operator<< (ostream&, const BoxWithJointSet&);
};

#endif // __BOXWITHJOINTSET_H
