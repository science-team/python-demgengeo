/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __MESH_VOL_JOINT_SET_H
#define __MESH_VOL_JOINT_SET_H

// --- Project includes ---
#include "MeshVolume.h"
#include "TriPatchSet.h"
#include "util/vector3.h"

// --- STL includes ---

/*!
  \class MeshVolWithJointSet

  A class for the generation of random particles inside a volume bounded by a triangle mesh, with the option of including joint sets.
  The correctness of the supplied mesh, i.e. that it actually defines a closed surface,
  is not checked in the class. Instead the caller is responsible to supply a correct mesh.
  No convexity or other topological requirements apply to the volume. 
  Facet orientations is not relevant.
  Works also if the mesh encloses multiple volumes. 
*/
class MeshVolWithJointSet : public MeshVolume
{
 protected:
  vector<Triangle3D> m_joints;

 public:
  MeshVolWithJointSet();
  MeshVolWithJointSet(const TriPatchSet&);
  virtual ~MeshVolWithJointSet(){};

  virtual const map<double,const AGeometricObject*> getClosestObjects(const Vector3&,int) const;
  virtual bool isIn(const Vector3&) const;
  virtual bool isIn(const Sphere&);
  void addJoints(const TriPatchSet&);
};

#endif // __MESH_VOL_JOINT_SET_H
