/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __AGENERATOR2D_H
#define __AGENERATOR2D_H

// --- Project includes ---
#include "AVolume2D.h"
#include "MNTable2D.h"

/*!
  \class AGenerator2D

  Abstract base class for a particle packing generator  
*/
class AGenerator2D
{
 public:
  virtual ~AGenerator2D(){};
  virtual void generatePacking (AVolume2D*,MNTable2D*,int,int) = 0;
};
#endif // __AGENERATOR2D_H
