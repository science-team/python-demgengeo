/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __CIRCLE_VOL_H
#define __CIRCLE_VOL_H

// --- Project includes ---
#include "AVolume2D.h"
#include "geometry/SphereIn.h"

// --- STL includes ---
#include <map>

using std::map;

class CircleVol :  public AVolume2D
{
 protected:
  SphereIn m_sph;

 public:
  CircleVol();
  CircleVol(const Vector3&,double);
  virtual ~CircleVol(){};

  virtual pair<Vector3,Vector3> getBoundingBox();
  virtual Vector3 getAPoint(int) const;  
  virtual Line2D getClosestPlane(const Vector3&);
  virtual const map<double,const Line2D*> getClosestPlanes(const Vector3&,int) const;
  virtual const map<double,const AGeometricObject*> getClosestObjects(const Vector3&,int) const;
  virtual bool isIn(const Vector3&) const;
  virtual bool isIn(const Sphere&);

  friend ostream& operator << (ostream&,const CircleVol&);
};

#endif // __CIRCLE_VOL_H
