#include "LineSet.h"

LineSet::LineSet()
{
  m_bbx_set=false;
}


int LineSet::isCrossing(const Vector3& p1, const Vector3& p2) const
{
	int res=-1;

	vector<LineSegment2D>::const_iterator iter=m_segments.begin();
	while((iter!=m_segments.end()) && (res==-1)){
		if(iter->crosses(p1,p2)) res=iter->getTag();
		iter++;
	}

  return res;
}

void LineSet::addLineSegment(const Vector3& p1, const Vector3& p2,int tag)
{
	LineSegment2D L=LineSegment2D(p1,p2,tag);
	Vector3 tmin=L.getMinPoint();
	Vector3 tmax=L.getMaxPoint();
	m_segments.push_back(L);
  	// update bounding box
	if(m_bbx_set){ // already have bbx -> update
		m_pmin.X()=(m_pmin.X() < tmin.X()) ?  m_pmin.X() : tmin.X();
		m_pmin.Y()=(m_pmin.Y() < tmin.Y()) ?  m_pmin.Y() : tmin.Y();
		m_pmax.X()=(m_pmax.X() > tmax.X()) ?  m_pmax.X() : tmax.X();
		m_pmax.Y()=(m_pmax.Y() > tmax.Y()) ?  m_pmax.Y() : tmax.Y();
	} else { // no bbx yet -> initialize
		m_pmin=tmin;
		m_pmax=tmax;
		m_bbx_set=true;
	}
}
