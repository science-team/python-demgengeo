/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __INSERTGENERATOR2D_H
#define __INSERTGENERATOR2D_H

// --- Project includes ---
#include "AGenerator2D.h"

/*!
  \class InsertGenerator2D

  Packing generator using Place et al. insertion based algorithm. 
*/
class InsertGenerator2D : public AGenerator2D
{
 protected:
  double m_rmin;
  double m_rmax;
  double m_max_tries;
  int m_max_iter;
  double m_prec;
  bool m_old_seeding;

  virtual void seedParticles(AVolume2D* ,MNTable2D* ,int,int);

 public:
  InsertGenerator2D();
  InsertGenerator2D(double,double,int,int,double);
  InsertGenerator2D(double,double,int,int,double,bool);
  virtual ~InsertGenerator2D();

  void setOldSeeding(bool);
  virtual void fillIn(AVolume2D* ,MNTable2D* ,int,int);
  virtual void generatePacking (AVolume2D*,MNTable2D*,int,int);

  friend ostream& operator << (ostream&,const InsertGenerator2D&);
};

#endif // __INSERTGENERATOR2D_H
