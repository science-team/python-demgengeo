/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "ClippedCircleVol.h"

using std::make_pair;

ClippedCircleVol::ClippedCircleVol()
{}
  
/*!
  constructor taking center and radius of the circle as arguments

  \param center the center of the circle
  \param radius the radius of the circle
*/
ClippedCircleVol::ClippedCircleVol(const Vector3& center,double radius):
  CircleVol(center,radius)
{}
  

/*!
  add a line to the volume

  \param l the line
  \param fit if true, line is used for fitting, if false line is only used for in/out detection
*/
void ClippedCircleVol::addLine(const Line2D& l,bool fit)
{
  m_lines.push_back(make_pair(l,fit));
}


/*!
  get a random point inside the volume

  \param n is ignored
*/
Vector3 ClippedCircleVol::getAPoint(int n) const
{
  Vector3 res;
  
  do {
    res=CircleVol::getAPoint(n);
  } while (!isIn(res));

  return res;
}
 
/*!
  get objects closest to a given point

  \param pos the point
  \param nr the max. number of objects returned
*/
const map<double,const AGeometricObject*> ClippedCircleVol::getClosestObjects(const Vector3& pos,int nr) const
{
  map<double,const AGeometricObject*> res=CircleVol::getClosestObjects(pos,nr);

  for(vector<pair<Line2D,bool> >::const_iterator iter=m_lines.begin();
      iter!=m_lines.end();
      iter++){
    if(iter->second){
      double dist=iter->first.getDist(pos);
      res.insert(make_pair(dist,&(iter->first)));
    }
  }
  return res;  
}

/*!
  test if a point is inside the volume

  \param pos the point
*/
bool ClippedCircleVol::isIn(const Vector3& pos) const
{
  bool res=CircleVol::isIn(pos);

  vector<pair<Line2D,bool> >::const_iterator iter=m_lines.begin();
  while(res && iter!=m_lines.end()){
    bool rside=((pos-iter->first.getOrig())*iter->first.getNormal())>0.0;
    res=res && rside;
    iter++;
  }
  return res;
}

/*!
  test if a sphere(circle) is completely inside the volume 

  \param S the sphere
*/
bool ClippedCircleVol::isIn(const Sphere& S)
{
  bool res=CircleVol::isIn(S);

  vector<pair<Line2D,bool> >::const_iterator iter=m_lines.begin();
  while(res && iter!=m_lines.end()){
    bool rside=((S.Center()-iter->first.getOrig())*iter->first.getNormal())>S.Radius();
    res=res && rside;
    iter++;
  }

  return res;
}
