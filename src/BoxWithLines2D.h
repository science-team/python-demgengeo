/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __BOXWITHLINES2D_H
#define __BOXWITHLINES2D_H

// --- Project includes ---
#include "AVolume2D.h"

// --- STL includes ---
#include <vector>
#include <map>

// --- IO includes ---
#include <iostream>

using std::vector;
using std::map;
using std::ostream;


/*!
  \class BoxWithLines2D

  A class for the generation of random particles inside a box. An arbitrary number 
  of lines can be added to which the particles are fitted.  
*/
class BoxWithLines2D : public AVolume2D
{
 protected:
  Vector3 m_pmin;
  Vector3 m_pmax;
  vector<Line2D> m_lines;
  double m_random(double,double)const;

 public:
  BoxWithLines2D();
  BoxWithLines2D(const Vector3&,const Vector3&);
  virtual ~BoxWithLines2D(){};

  void addLine(const Line2D&);
  virtual pair<Vector3,Vector3> getBoundingBox();
  virtual Vector3 getAPoint(int)const;
  virtual bool hasPlane() const{return (m_lines.size()>0);};
  virtual Line2D getClosestPlane(const Vector3&);
  virtual const map<double,const Line2D*> getClosestPlanes(const Vector3&,int) const;
  virtual bool isIn(const Vector3&) const;
  virtual bool isIn(const Sphere&);

  friend ostream& operator<< (ostream&, const BoxWithLines2D&);
};
#endif // __BOXWITHLINES2D_H
