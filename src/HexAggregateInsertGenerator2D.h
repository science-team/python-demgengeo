/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __HEXAGGREGATEINSERTGENERATOR2D_H
#define __HEXAGGREGATEINSERTGENERATOR2D_H

// --- Project includes ---
#include "InsertGenerator2D.h"

/*!
  \class InsertGenerator2D

  Packing generator using Place et al. insertion based algorithm. 
*/
class HexAggregateInsertGenerator2D : public InsertGenerator2D
{
 protected:

  virtual void seedParticles(AVolume2D* ,MNTable2D* ,int,int);

 public:
  HexAggregateInsertGenerator2D();
  HexAggregateInsertGenerator2D(double,double,int,int,double);
  virtual ~HexAggregateInsertGenerator2D(){};

  virtual void fillIn(AVolume2D* ,MNTable2D* ,int,int);
};

#endif // __HEXAGGREGATEINSERTGENERATOR2D_H
