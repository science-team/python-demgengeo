/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "HexAggregateShape.h"

void HexAggregateShape::insert(Vector3 pos, double radius, MNTable3D* table, int tag, int id) {
  if ( this->useRandomOrientation() ) {
    this->setRandomOrientation();
  }
  
  
  double rn=radius/3.0; // new radii
  // center sphere
  Sphere Sc(pos,rn);
  Sc.setTag(tag);
  table->insertChecked(Sc,id);
  int Sc_id=id;
  // outer spheres
  int Sk_id[6];
  for(int k=0;k<6;k++){
    double phi=double(k)*1.04719551; // k*pi/3
    Vector3 offset=Vector3(2.0000*rn*sin(phi),2.0000*rn*cos(phi),0.0);
    Sphere Sk(pos + rotatePoint(offset),rn*0.99999);
    if(table->checkInsertable(Sk,id)){
      Sk.setTag(tag);
      table->insert(Sk,id);
      Sk_id[k]=id;
      table->insertBond(Sc_id,Sk_id[k],0); // bond between center and outer
    } else {
      Sk_id[k]=-1;
    }
  }
  for(int k=0;k<6;k++){
    int k2=(k+1) % 6;
    if((Sk_id[k]!=-1) && (Sk_id[k2]!=-1)) {
      table->insertBond(Sk_id[k],Sk_id[k2],0);
    }
  }
  // upper spheres
  int Sk_up[3];
  double alpha=0.5235987755982988; // pi/6 (30�)
  double beta=1.5707963267948965-atan(0.7071067811865475);
  for(int k=0;k<3;k++){
    double rho=beta;
    double phi=double(1+4*k)*alpha;
    Vector3 offset=Vector3(2.0*rn*sin(phi)*cos(rho),
		     2.0*rn*cos(phi)*cos(rho),
		     2.0*rn*sin(rho));
    Sphere Sk(pos+rotatePoint(offset),rn*0.99999);
    if(table->checkInsertable(Sk,id)){
      Sk.setTag(tag);
      table->insert(Sk,id);
      Sk_up[k]=id;
      table->insertBond(Sc_id,Sk_up[k],0); // bond between center and upper
      if(Sk_id[k*2]!=-1) table->insertBond(Sk_id[k*2],Sk_up[k],0);
      if(Sk_id[(k*2+1)%6]!=-1) table->insertBond(Sk_id[(k*2+1)%6],Sk_up[k],0);
    } else {
      Sk_up[k]=-1;
    }
  }
  // bond within upper
  for(int k=0;k<3;k++){
    int k2=(k+1) % 3;
    if((Sk_up[k]!=-1) && (Sk_up[k2]!=-1)) {
      table->insertBond(Sk_up[k],Sk_up[k2],0);
    }
  }
  // lower spheres
  for(int k=0;k<3;k++){
    double rho=beta;
    double phi=double(1+4*k)*alpha;
    Vector3 offset=Vector3(2.0*rn*sin(phi)*cos(rho),
		     2.0*rn*cos(phi)*cos(rho),
		     -2.0*rn*sin(rho));
    Sphere Sk(pos+rotatePoint(offset),rn*0.99999);
    if(table->checkInsertable(Sk,id)){
      Sk.setTag(tag);
      table->insert(Sk,id);
      Sk_up[k]=id;
      table->insertBond(Sc_id,Sk_up[k],0); // bond between center and upper
      if(Sk_id[k*2]!=-1) table->insertBond(Sk_id[k*2],Sk_up[k],0);
      if(Sk_id[(k*2+1)%6]!=-1) table->insertBond(Sk_id[(k*2+1)%6],Sk_up[k],0);
    } else {
      Sk_up[k]=-1;
    }
  }
  // bond within upper
  for(int k=0;k<3;k++){
    int k2=(k+1) % 3;
    if((Sk_up[k]!=-1) && (Sk_up[k2]!=-1)) {
      table->insertBond(Sk_up[k],Sk_up[k2],0);
    }
  }
}  


int HexAggregateShape::bias() {
  return Shape::bias();
}

void HexAggregateShape::setBias(int i) {
  Shape::setBias(i);
}

