/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __AVOLUME3D_H
#define __AVOLUME3D_H

// --- Project includes ---
#include "AVolume.h"
#include "geometry/Plane.h"

// --- STL includes ---
#include <map>

using std::map;

class AVolume3D : public AVolume
{
 public:
  virtual ~AVolume3D(){};

  virtual const map<double,const AGeometricObject*> getClosestObjects(const Vector3&,int) const = 0;
  virtual bool isFullyOutside(const Sphere&) =0;
};

#endif // __AVOLUME3D_H
