/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __CYLINDERVOL_H
#define __CYLINDERVOL_H

// --- Project includes ---
#include "AVolume3D.h"
#include "geometry/Cylinder.h"
#include "geometry/Plane.h"

// --- STL includes ---
#include <vector>
#include <map>

using std::vector;
using std::map;

class CylinderVol : public AVolume3D
{
 protected:
  Cylinder m_cyl;
  Plane m_bottom,m_top;
  double m_len;
  Vector3 m_xd,m_yd; // "in plane" vectors;

 public:
  CylinderVol();
  CylinderVol(const Vector3&,const Vector3&,double,double);
  virtual ~CylinderVol(){};

  virtual pair<Vector3,Vector3> getBoundingBox();
  virtual Vector3 getAPoint(int) const;  
  virtual const map<double,const AGeometricObject*> getClosestObjects(const Vector3&,int) const;
  virtual bool isIn(const Vector3&) const;
  virtual bool isIn(const Sphere&);
  virtual bool isFullyOutside(const Sphere&);
 
  friend ostream& operator << (ostream&,const CylinderVol&);
};

#endif // __CYLINDERVOL_H
