/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "ShapeList.h"
#include "HexAggregateShape.h"

void ShapeList::addHexShape(int bias, int random) {
  HexAggregateShape shape;
  shape.setBias(bias);
  shape.makeOrientationRandom(random);
  shapeList.push_back(&shape);
}

void ShapeList::insertShape(Vector3 pos, double radius, MNTable3D* ntable, int tag, int id) {
  std::vector<int> biasList;
  unsigned int i;
  int biasTotal=0;
  // at all bias values.
  for(i=0; i < shapeList.size() ; i++) {
    int currBias = shapeList[i]->bias();
    biasTotal = biasTotal + currBias;
    biasList.push_back ( currBias );
  }

  if ( biasTotal == 0 ) {
    return;
  }

  // at a random value to select a shape
  int randomValue = rand() % biasTotal;
  i=0;
  // Find appropriate shape in list.
  while ( randomValue > shapeList[i]->bias() ) {
    randomValue = randomValue - shapeList[i]->bias();
    i++;
    if (i == shapeList.size() ) {
      std::cout << "Error in ShapeList::insertShape :> randomValue too high\n";
      return;
    }
  }
  shapeList[i]->insert(pos, radius, ntable, tag,id);
}


void ShapeList::addGenericShape(string db, string name, int bias, int random, 
    int particleTag, int bondTag) {
  shapeList.push_back(new GenericShape(db,name));
  Shape *shape = shapeList[shapeList.size()-1];
  shape->setBias(bias);
  shape->makeOrientationRandom(random);
  shape->setParticleTag(particleTag);
  shape->setBondTag(bondTag);
}

