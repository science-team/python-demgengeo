/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef SHAPE_H
#define SHAPE_H

#include "util/vector3.h"
#include "MNTable3D.h"
class Shape {
  public:
    Shape();

    /**
     * Tell the shape to be randomly orientated.  This will make every object
     * have a random orientation
     * \param i 1 for using random orientation, 0 to disable
     */
    void makeOrientationRandom(int i);

    /**
     * Returns 1 if the shape is to be randomly orientated
     * \returns 1 if shape is to be randomly orientated
     */
    int useRandomOrientation();

    /**
     * Sets a random orientation.  Note that this will be run before every
     * insert if the appropriate function is called.
     */    
    void setRandomOrientation();

    /** 
     * Set a random pitch to the object
     */    
    void setRandomPitch();

    /** 
     * Set a random yaw to the object
     */    
    void setRandomYaw();

    /**
     * Set a random roll to the object
     */    
    void setRandomRoll();

    /**
     * Rotate a point around the x, y and z axies.  Rotation values are given
     * using setRoll functions etc.
     * \param point the point to rotate
     * \returns the rotated point
     */
    Vector3 rotatePoint(Vector3 point);
    
    virtual void insert(Vector3 pos, double radius, MNTable3D* ntable,int tag,int id ) { std::cout << "No shape" << std::endl; return;} ;

    /** 
     * Show the bias of a given shape (ie how likely it is to occur.
     * Note that the function must return a deterministic value.  
     */
    int bias();
    
    /**
     * Set the bias of the given shape
     * \param bias the bias of the shape
     */
    void setBias(int);

    void setPitch(double);
    double getPitch();
    void setYaw(double);
    double getYaw();
    void setRoll(double);
    double getRoll();

    void setBondTag(int);
    int getBondTag();
    void setParticleTag(int);
    int getParticleTag();
    
    ~Shape();
  protected:
    double pitch;
    double yaw;
    double roll;
    int bias_factor;
    int randomOrientation;
    int bondTag;
    int particleTag;
};

#endif /* SHAPE_H */
