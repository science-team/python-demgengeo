/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __SPHERE_VOL_WITH_JOINT_SET_H
#define  __SPHERE_VOL_WITH_JOINT_SET_H

// --- Project includes ---
#include "SphereVol.h"
#include "geometry/SphereIn.h"
#include "TriPatchSet.h"

// --- STL includes ---
#include <map>

using std::map;

class SphereVolWithJointSet :  public SphereVol
{
 protected:
  vector<Triangle3D> m_joints;

 public:
  SphereVolWithJointSet();
  SphereVolWithJointSet(const Vector3&,double);
  virtual ~SphereVolWithJointSet(){};
 
  void addJoints(const TriPatchSet&);

  virtual const map<double,const AGeometricObject*> getClosestObjects(const Vector3&,int) const;
  virtual bool isIn(const Sphere&);

  friend ostream& operator << (ostream&,const SphereVolWithJointSet&);
};

#endif // __SPHERE_VOL_WITH_JOINT_SET_H
