/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __CYLINDERWITHJOINTSET_H
#define __CYLINDERWITHJOINTSET_H

// --- Project includes ---
#include "CylinderVol.h"
#include "geometry/Plane.h"
#include "TriPatchSet.h"
#include "geometry/Triangle3D.h"

// --- STL includes ---
#include <vector>
#include <map>

using std::vector;
using std::map;

class CylinderWithJointSet : public CylinderVol
{
 protected:
  vector<Triangle3D> m_joints;

 public:
  CylinderWithJointSet();
  CylinderWithJointSet(const Vector3&,const Vector3&,double,double);
  virtual ~CylinderWithJointSet(){};

  virtual const map<double,const AGeometricObject*> getClosestObjects(const Vector3&,int) const;
  void addJoints(const TriPatchSet&);
  virtual bool isIn(const Sphere&);
 
  friend ostream& operator << (ostream&,const CylinderWithJointSet&);
};

#endif // __CYLINDERWITHJOINTSET_H
