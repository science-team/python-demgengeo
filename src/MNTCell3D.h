/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __MNTCELL3D_H
#define __MNTCELL3D_H

// --- STL includes ---
#include <vector>
#include <map>

using std::vector; 
using std::map;

// --- IO includes ---
#include <iostream>

using std::ostream;

// --- Project includes ---
#include <Sphere3d.h>

/*!
  \class MNTCell3D
  \brief class for a cell in a 3D neighbor table (MNTable3D)
*/
class MNTCell3D
{
 private:
  vector<vector<Sphere3D> > m_data;
  static int s_output_style;

 public:
  MNTCell3D(unsigned int ngroups=1);

  void SetNGroups(unsigned int);

  void insert(const Sphere3D&,int);
  int NParts() const;
  vector<Sphere2D> getSpheresNear(const Vector3&,double);
  vector<Sphere2D> getSpheresFromGroupNear(const Vector3&,double,int);
  map<double,Sphere2D> getSpheresClosestTo(const Vector3&,unsigned int); 

  // output 
  double getSumVolume(unsigned int);
  static void SetOutputStyle(int);
  friend ostream& operator << (ostream&,const MNTCell2D&);
  void writePositions(ostream&);
  void writeRadii(ostream&);
};

#endif // MNTCELL_H
