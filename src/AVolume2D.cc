/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "AVolume2D.h"

// dummy implemetation - returns empty list
const map<double,const AGeometricObject*> AVolume2D::getClosestObjects(const Vector3& P,int) const
{
  map<double,const AGeometricObject*> res;

  return res;  
}
