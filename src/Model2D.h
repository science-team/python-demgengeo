/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __MODEL2D_H
#define __MODEL2D_H

// --- Project includes ---
#include "src/MNTable2D.h"
#include "src/AParticleGroup.h"
#include "util/vector3.h"

// --- STL includes ---
#include <list>
#include <string>

using std::list;
using std::string;
/*!
  \class Model2D
  \brief 
*/
class Model2D
{
 private:
  MNTable2D* m_ntable;
  list<AParticleGroup*> m_pgroups;
  Vector3 m_min_pt;
  Vector3 m_max_pt;
  int m_ngroups;
  int m_max_gid;

 public:
  Model2D(const Vector3&,const Vector3&,double);
  ~Model2D();

  int AddParticleGroup(AParticleGroup*);

  void WriteAsGeoFile(const string&);
};


#endif // __MODEL2D_H
