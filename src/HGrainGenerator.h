/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __HGRAINGENERATOR2D_H
#define __HGRAINGENERATOR2D_H

// --- Project includes ---
#include "AVolume2D.h"
#include "MNTable2D.h"
#include "AGenerator2D.h"

/*!
  \class HGrainGenerator2D

  class the generate hexagonal grains on a regular base lattice
*/
class HGrainGenerator2D : public AGenerator2D
{
 protected:
  double m_rad;

 public:
  HGrainGenerator2D();
  HGrainGenerator2D(double);
  virtual ~HGrainGenerator2D();

  virtual void generatePacking (AVolume2D*,MNTable2D*,int,int);

  friend ostream& operator << (ostream&,const HGrainGenerator2D&);
};


#endif // __HGRAINGENERATOR2D_H
