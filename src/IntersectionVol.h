/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
/////////////////////////////////////////////////////////////

#ifndef __INTERSECT_VOLUME_SET_H
#define  __INTERSECT_VOLUME_SET_H

// --- Project includes ---
#include "AVolume3D.h"

// --- STL includes ---
#include <map>

using std::map;

class IntersectionVol :  public AVolume3D
{
 protected:
  AVolume3D* m_vol1;
  AVolume3D* m_vol2;

 public:
  IntersectionVol ();
  IntersectionVol (AVolume3D& , AVolume3D&);
  virtual ~IntersectionVol (){};

  virtual const map<double,const AGeometricObject*> getClosestObjects(const Vector3&,int) const;

  virtual pair<Vector3,Vector3> getBoundingBox();
  virtual Vector3 getAPoint(int) const;
  virtual bool isIn(const Vector3&) const;
  virtual bool isIn(const Sphere&);
  virtual bool isFullyOutside(const Sphere&);
  virtual int getNumberSubVolumes()const{return 1;};

  friend ostream& operator << (ostream&,const IntersectionVol&);
};

#endif // __INTERSECT_VOLUME_SET_H

