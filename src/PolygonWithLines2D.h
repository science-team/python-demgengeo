/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __POLYGONWITHLINES2D_H
#define __POLYGONWITHLINES2D_H

// --- Project includes ---
#include "AVolume2D.h"

// --- STL includes ---
#include <vector>
#include <map>

// --- IO includes ---
#include <iostream>

// --- Boost includes ---
#include <boost/python.hpp>
#include <boost/python/stl_iterator.hpp>

using std::vector;
using std::map;
using std::ostream;

#define MAX_VERTICES 50


/*!
  \class PolygonWithLines2D

  A class for the generation of random particles inside a 2D polygon. An arbitrary number 
  of lines can be added to which the particles are fitted.  
*/
class PolygonWithLines2D : public AVolume2D
{
 protected:
  Vector3 m_pmin;
  Vector3 m_pmax;
  Vector3 m_centre;
  double m_radius;
  int m_nsides;
  Vector3 m_vertices[MAX_VERTICES];
  vector<Line2D> m_lines;
  double m_random(double,double)const;

 public:
  PolygonWithLines2D();
  PolygonWithLines2D(const Vector3&,double,int,bool);
  PolygonWithLines2D(boost::python::list);
  virtual ~PolygonWithLines2D(){};

  void addLine(const Line2D&);
  virtual pair<Vector3,Vector3> getBoundingBox();
  virtual Vector3 getAPoint(int)const;
  virtual bool hasPlane() const{return (m_lines.size()>0);};
  virtual Line2D getClosestPlane(const Vector3&);
  virtual const map<double,const Line2D*> getClosestPlanes(const Vector3&,int) const;
  virtual bool isIn(const Vector3&) const;
  virtual bool isIn(const Sphere&);

  friend ostream& operator<< (ostream&, const PolygonWithLines2D&);
};
#endif // __TRIWITHLINES2D_H
