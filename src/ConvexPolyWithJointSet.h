/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __CONVEX_POLY_WITH_JOINT_SET_H
#define __CONVEX_POLY_WITH_JOINT_SET_H

// --- Project includes ---
#include "BoxWithPlanes3D.h"
#include "ConvexPolyhedron.h"
#include "ConvexPolyWithJointSet.h"
#include "geometry/Plane.h"
#include "TriPatchSet.h"

// --- STL includes ---
#include <vector>
#include <map>

using std::vector;
using std::map;

/*!
  \class ConvexPolyWithJointSet

   A class for the generation of random particles inside a convex polyhedron. 
   The polyhedron defined by a bounding box and an arbitrary number of planes to which the particles are fitted.  The bounding box is largely there to prevent the generation of infinite polyhedra.
   This subclass of ConvexPolyhedron permits addition of triangle patches to represent joints and/or discontinuities.
*/
class ConvexPolyWithJointSet : public ConvexPolyhedron
{
 protected:
   vector<Triangle3D> m_joints;

 public:
  ConvexPolyWithJointSet();
  ConvexPolyWithJointSet(const Vector3&,const Vector3&);
  virtual ~ConvexPolyWithJointSet();

  void addJoints(const TriPatchSet&);

  virtual Vector3 getAPoint(int)const;
  virtual bool isIn(const Vector3&) const;
  virtual bool isIn(const Sphere&);
};
#endif //__CONVEX_POLY_WITH_JOINT_SET_H
