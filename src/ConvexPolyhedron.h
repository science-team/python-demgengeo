/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __CONVEX_POLYHEDRON_H
#define __CONVEX_POLYHEDRON_H

// --- Project includes ---
#include "BoxWithPlanes3D.h"
#include "geometry/Plane.h"

// --- STL includes ---
#include <vector>
#include <map>

using std::vector;
using std::map;

/*!
  \class ConvexPolyhedron

   A class for the generation of random particles inside a convex polyhedron. 
   The polyhedron defined by a bounding box and an arbitrary number of planes to which the particles are fitted.  The bounding box is largely there to prevent the generation of infinite polyhedra.
*/
class ConvexPolyhedron : public BoxWithPlanes3D
{
 protected:

 public:
  ConvexPolyhedron();
  ConvexPolyhedron(const Vector3&,const Vector3&);
  virtual ~ConvexPolyhedron();

  virtual Vector3 getAPoint(int)const;
  virtual bool isIn(const Vector3&) const;
  virtual bool isIn(const Sphere&);
};
#endif //__CONVEX_POLYHEDRON_H
