/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __CIRCMNTABLE2D_H
#define __CIRCMNTABLE2D_H

// --- Project includes ---
#include "MNTable2D.h"

/*!
  \class CircMNTable2D
  \brief circular 2D Multi-group Neighbour table

  Neighbour table supporting multiple tagged groups of particles and circular boundary
  conditions (x only at the moment)
*/
class CircMNTable2D : public MNTable2D
{
 protected:
  Vector3 m_shift_vec_x;

  virtual void set_x_circ();
  virtual int getIndex(const Vector3&) const;
  int getXIndex(const Vector3&) const;
  int getYIndex(const Vector3&) const;
  int getFullIndex(const Vector3&) const;


 public:
  CircMNTable2D();
  CircMNTable2D(const Vector3&,const Vector3&,double,unsigned int);
  virtual ~CircMNTable2D();
  
  virtual bool insert(const Sphere&,unsigned int);
  virtual bool insertChecked(const Sphere&,unsigned int,double tol=s_small_value);
  virtual bool checkInsertable(const Sphere&,unsigned int);
  virtual void generateBonds(int,double,int);  
  virtual void generateBondsWithMask(int,double,int,int,int);

  // output 
  friend ostream& operator << (ostream&,const CircMNTable2D&);
};

#endif // __CIRCMNTABLE2D_H
