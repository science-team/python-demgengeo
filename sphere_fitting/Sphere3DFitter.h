/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __SPHERE3DFITTER_H
#define __SPHERE3DFITTER_H

// --- project includes ---
#include "geometry/Sphere.h"
#include "geometry/AGeometricObject.h"

Sphere FitSphere3D(const AGeometricObject*,const AGeometricObject*,const AGeometricObject*,const AGeometricObject*,const Vector3&,int,double);


#endif // __SPHERE3DFITTER_H
