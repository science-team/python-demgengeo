/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "fit_3d_1sphere_3lines.h"
#include <cmath>

using std::sqrt;
using std::fabs;

fit_3d_1sphere_3lines_fn::fit_3d_1sphere_3lines_fn(const Vector3& sc, double r, 
						   const Vector3& o1, const Vector3& n1,
						   const Vector3& o2, const Vector3& n2,
						   const Vector3& o3, const Vector3& n3)
{
  m_p1=sc;
  m_r1=r;
  m_orig1=o1;
  m_nor1=n1;
  m_orig2=o2;
  m_nor2=n2;
  m_orig3=o3;
  m_nor3=n3;
}

double fit_3d_1sphere_3lines_fn::operator()(const nvector<double,3>& data) const
{
  double x=data[0];
  double y=data[1];
  double z=data[2];

  double ra=sqrt((x-m_p1.x())*(x-m_p1.x())+(y-m_p1.y())*(y-m_p1.y())+(z-m_p1.z())*(z-m_p1.z()))-m_r1;
  double rb=fabs(dot((Vector3(x,y,z)-m_orig1),m_nor1));
  double rc=fabs(dot((Vector3(x,y,z)-m_orig2),m_nor2));
  double rd=fabs(dot((Vector3(x,y,z)-m_orig3),m_nor3));

  double rq=0.25*(ra+rb+rc+rd);

  double dr=sqrt((rq-ra)*(rq-ra)+(rq-rb)*(rq-rb)+(rq-rc)*(rq-rc)+(rq-rd)*(rq-rd));

  return dr; }
