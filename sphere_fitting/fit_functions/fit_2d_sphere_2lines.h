/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __FIT_2D_SPHERE_2LINES_FN_H
#define __FIT_2D_SPHERE_2LINES_FN_H

#include "sphere_fitting/utils/nvector.h"
#include "sphere_fitting/utils/nfunction.h"
#include "util/vector3.h"

class fit_2d_sphere_2lines_fn : public nfunction<double,2>
{
 private:
  Vector3 m_p; // sphere center
  double m_r; // sphere radii
  Vector3 m_orig1,m_nor1,m_orig2,m_nor2; // lines

 public:
  fit_2d_sphere_2lines_fn(const Vector3&, double,const Vector3&, const Vector3& ,const Vector3&, const Vector3&);
  virtual ~fit_2d_sphere_2lines_fn(){};

  virtual double operator()(const nvector<double,2>&) const;
};

#endif // __FIT_2D_SPHERE_2LINES_FN_H
