/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "fit_2d_sphere_line.h"
#include <cmath>

using std::sqrt;
using std::fabs;

/*!
 */
fit_2d_sphere_line_fn::fit_2d_sphere_line_fn(const Vector3& sc1, double r1,const Vector3& sc2, double r2,const Vector3& o, const Vector3& n)
{
  m_p1=sc1;
  m_p2=sc2;
  m_r1=r1;
  m_r2=r2;
  m_orig=o;
  m_nor=n;
}

/*!
 */
double fit_2d_sphere_line_fn::operator()(const nvector<double,2>& data) const
{
  double x=data[0];
  double y=data[1];
  double ra=sqrt((x-m_p1.x())*(x-m_p1.x())+(y-m_p1.y())*(y-m_p1.y()))-m_r1;
  double rb=sqrt((x-m_p2.x())*(x-m_p2.x())+(y-m_p2.y())*(y-m_p2.y()))-m_r2;
  double rc=fabs(dot((Vector3(x,y,0.0)-m_orig),m_nor));
  double rq=(ra+rb+rc)/3.0;
  double dr=sqrt((rq-ra)*(rq-ra)+(rq-rb)*(rq-rb)+(rq-rc)*(rq-rc));

  return dr;
}
