/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __FIT_3D_3SPHERES_1LINE_FN_H
#define __FIT_3D_3SPHERES_1LINE_FN_H

#include "sphere_fitting/utils/nvector.h"
#include "sphere_fitting/utils/nfunction.h"
#include "util/vector3.h"

class fit_3d_3spheres_1line_fn : public nfunction<double,3>
{
 private:
  Vector3 m_p1,m_p2,m_p3; // sphere centers
  double m_r1,m_r2,m_r3; // sphere radii
  Vector3 m_orig,m_nor; // line

 public:
  fit_3d_3spheres_1line_fn(const Vector3&, double,const Vector3&, double,const Vector3&, double,const Vector3&, const Vector3&);
  virtual ~fit_3d_3spheres_1line_fn(){};

  virtual double operator()(const nvector<double,3>&) const;
};

#endif // __FIT_2D_SPHERE_LINE_FN_H
