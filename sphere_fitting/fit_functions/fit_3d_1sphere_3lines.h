/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __FIT_3D_1SPHERE_3LINES_FN_H
#define __FIT_3D_1SPHERE_3LINES_FN_H

#include "sphere_fitting/utils/nvector.h"
#include "sphere_fitting/utils/nfunction.h"
#include "util/vector3.h"

class fit_3d_1sphere_3lines_fn : public nfunction<double,3>
{
 private:
  Vector3 m_p1; // sphere center
  double m_r1; // sphere radius
  Vector3 m_orig1,m_nor1,m_orig2,m_nor2,m_orig3,m_nor3; // planes

 public:
  fit_3d_1sphere_3lines_fn(const Vector3&, double, const Vector3& ,const Vector3&,const Vector3&, const Vector3&,const Vector3&, const Vector3&);
  virtual ~fit_3d_1sphere_3lines_fn(){};

  virtual double operator()(const nvector<double,3>&) const;
};

#endif // __FIT_3D_1SPHERE_3LINES_FN_H
