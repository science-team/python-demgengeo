/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "fit_3d_sphere.h"
#include <cmath>
#include <iostream>

using std::sqrt;

fit_3d_sphere_fn::fit_3d_sphere_fn(const AGeometricObject* GO1,
				   const AGeometricObject* GO2,
				   const AGeometricObject* GO3,
				   const AGeometricObject* GO4)
{
  m_GO1=GO1;
  m_GO2=GO2;
  m_GO3=GO3;
  m_GO4=GO4;
}

double fit_3d_sphere_fn::operator()(const nvector<double,3>& data) const
{
  Vector3 x=Vector3(data[0],data[1],data[2]);

  double ra=m_GO1->getDist(x);
  double rb=m_GO2->getDist(x);
  double rc=m_GO3->getDist(x);
  double rd=m_GO4->getDist(x);

  double rq=(ra+rb+rc+rd)*0.25;
  double dr=sqrt((rq-ra)*(rq-ra)+(rq-rb)*(rq-rb)+(rq-rc)*(rq-rc)+(rq-rd)*(rq-rd));

  return dr;
}
