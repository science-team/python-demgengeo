/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef __FIT_2D_SPHERE_FN_H
#define __FIT_2D_SPHERE_FN_H

// --- project includes ---
#include "sphere_fitting/utils/nvector.h"
#include "sphere_fitting/utils/nfunction.h"
#include "util/vector3.h"
#include "geometry/AGeometricObject.h"

class fit_2d_sphere_fn : public nfunction<double,2>
{
 private:
  const AGeometricObject *m_GO1,*m_GO2,*m_GO3;

 public:
  fit_2d_sphere_fn(const AGeometricObject*,const AGeometricObject*,const AGeometricObject*);
  virtual ~fit_2d_sphere_fn(){};

  virtual double operator()(const nvector<double,2>&) const;
};

#endif // __FIT_2D_SPHERE_FN_H
