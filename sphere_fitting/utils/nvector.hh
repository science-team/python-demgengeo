/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

template<class T,int n>
nvector<T,n>::nvector()
{}

template<class T,int n>
nvector<T,n>::nvector(const T t)
{
  for(int i=0;i<n;i++){
    m_data[i]=t; 
  }
}

template<class T,int n>
nvector<T,n>::nvector(const nvector& v)
{
  for(int i=0;i<n;i++){
    m_data[i]=v.m_data[i]; 
  }
}

template<class T,int n>
nvector<T,n>::~nvector()
{}

template<class T,int n>
nvector<T,n>& nvector<T,n>::operator=(const nvector<T,n>& v)
{
  if(&v!=this){
    for(int i=0;i<n;i++){
      m_data[i]=v.m_data[i]; 
    }
  }
  return *this;
}

template<class T,int n>
nvector<T,n>& nvector<T,n>::operator-=(const nvector<T,n>& v)
{
  for(int i=0;i<n;i++){
    m_data[i]-=v.m_data[i]; 
  }
  return *this;
}

template<class T,int n>
nvector<T,n>& nvector<T,n>::operator+=(const nvector<T,n>& v)
{
  for(int i=0;i<n;i++){
    m_data[i]+=v.m_data[i]; 
  }
  return *this;
}

template<class T,int n>
nvector<T,n> nvector<T,n>::operator+(const nvector<T,n>& v) const
{
  nvector<T,n> res;
  for(int i=0;i<n;i++){
    res.m_data[i]=m_data[i]+v.m_data[i]; 
  }
  return res;
}

template<class T,int n>
nvector<T,n> nvector<T,n>::operator-(const nvector<T,n>& v) const
{
  nvector<T,n> res;
  for(int i=0;i<n;i++){
    res.m_data[i]=m_data[i]-v.m_data[i]; 
  }
  return res;
}

template<class T,int n>
nvector<T,n> operator/(const nvector<T,n>& v, T d)
{
  nvector<T,n> res;
  for(int i=0;i<n;i++){
    res.m_data[i]=v.m_data[i]/d; 
  }
  return res;
}

template<class T,int n>
nvector<T,n> nvector<T,n>::unit(int i)
{
  nvector<T,n> res;
  for(int j=0;j<n;j++){
    res.m_data[j]=(i==j)? T(1) : T(0);
  }
  return res;
}

template<class T,int n> 
ostream& operator<<(ostream& ost,const nvector<T,n>& v)
{
  ost << "<" << v.m_data[0];
  for(int i=1;i<n;i++){
    ost << "," << v.m_data[i];
  }
  ost << ">";
  return ost;
}
