/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

// simplex method
#ifndef __SIMPLEX_H
#define __SIMPLEX_H

#include "nvector.h"
#include "nfunction.h"

#include <iostream>


template<class T,int n>
class simplex_method
{
 private:
  nfunction<T,n>* m_func;
  nvector<T,n> m_vert[n+1];
  T m_val[n+1];

  nvector<T,n> reflect(int);
  void insert(const nvector<T,n>&,T,int);
  void shrink();
  void sort();

 public:
  simplex_method(nfunction<T,n>*);

  nvector<T,n> solve(T,const nvector<T,n> &,int max=-1);
};

#include "simplex.hh"

#endif //__SIMPLEX_H
