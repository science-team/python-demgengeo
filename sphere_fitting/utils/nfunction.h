/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

// function of nvector
#ifndef __NFUNCTION_H
#define __NFUNCTION_H

#include "nvector.h"

template<class T,int n>
class nfunction
{
 public:
  virtual T operator()(const nvector<T,n>&) const=0;
  virtual ~nfunction(){};
};

#endif //__NFUNCTION_H
