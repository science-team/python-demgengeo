/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

// arbitrary size vectors
#ifndef __NVECTOR_H
#define __NVECTOR_H
#include <iostream>


using std::ostream;

template<class T,int n>
  class nvector;

template<class T,int n>
  nvector<T,n> operator/(const nvector<T,n>&, T);

template<class T,int n> 
  ostream& operator<<(ostream& ,const nvector<T,n>&);

template<class T,int n>
class nvector
{
 private:
  T m_data[n];

 public:
  nvector();
  nvector(const T);
  nvector(const nvector&);
  ~nvector();

  inline T operator[](int i) const {return m_data[i];};
  inline T& operator[](int i){return m_data[i];};

  nvector& operator=(const nvector&);
  nvector& operator-=(const nvector&);
  nvector& operator+=(const nvector&);
  nvector operator-(const nvector&) const;
  nvector operator+(const nvector&) const;
  
  friend nvector operator/ <>(const nvector&,T);
  static nvector unit(int);

  friend ostream& operator<< <>(ostream&,const nvector&);
};

#include "nvector.hh"

#endif //__NVECTOR_H
