/////////////////////////////////////////////////////////////
//                                                         //
// Copyright (c) 2007-2017 by The University of Queensland //
// Centre for Geoscience Computing                         //
// http://earth.uq.edu.au/centre-geoscience-computing      //
//                                                         //
// Primary Business: Brisbane, Queensland, Australia       //
// Licensed under the Open Software License version 3.0    //
// http://www.apache.org/licenses/LICENSE-2.0              //
//                                                         //
/////////////////////////////////////////////////////////////

#include "fit_2d_sphere.h"
#include <cmath>
#include <iostream>

using std::sqrt;

/*!
 */
fit_2d_sphere_fn::fit_2d_sphere_fn(const AGeometricObject* GO1,const AGeometricObject* GO2,const AGeometricObject* GO3)
{
  m_GO1=GO1;
  m_GO2=GO2;
  m_GO3=GO3;
}

double fit_2d_sphere_fn::operator()(const nvector<double,2>& data) const
{
  Vector3 x=Vector3(data[0],data[1],0.0);
  double ra=m_GO1->getDist(x);
  double rb=m_GO2->getDist(x);
  double rc=m_GO3->getDist(x);
  double rq=(ra+rb+rc)/3.0;
  double dr=sqrt((rq-ra)*(rq-ra)+(rq-rb)*(rq-rb)+(rq-rc)*(rq-rc));

  return dr;
}
